<?php

// use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Route::get('storage/brand-images/{filename}', function ($filename) {
    $path = storage_path('app/public/brand_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/pos_categories/{filename}', function ($filename) {
    $path = storage_path('app/public/pos_categories/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/pos_products/{filename}', function ($filename) {
    $path = storage_path('app/public/pos_products/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});


Route::get('storage/users-profile/{filename}', function ($filename) {
    $path = storage_path('app/public/users-profile/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::group(['prefix' => 'transaction','middleware'=>'auth'], function () {
    // select2
    Route::get('quotation_orders/type_product/select2', [App\Http\Controllers\transaction\QuotationOrderController::class, 'typeProductSelect2'])->name('quotation_orders.type_product.select2');
    Route::post('/quotation_orders/payment/store', [App\Http\Controllers\transaction\QuotationOrderController::class, 'paymentStore'])->name('quotation_orders.payment.store');
    Route::get('/quotation_orders', [App\Http\Controllers\transaction\QuotationOrderController::class, 'index'])->name('quotation_orders.home');
    Route::get('/quotation_orders/create', [App\Http\Controllers\transaction\QuotationOrderController::class, 'create'])->name('quotation_orders.cretae');
    Route::post('/quotation_orders/store', [App\Http\Controllers\transaction\QuotationOrderController::class, 'store'])->name('quotation_orders.store');
    Route::get('/quotation_orders/edit/{id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'edit'])->name('quotation_orders.edit');
    Route::get('/quotation_orders/print_qo/{id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'printQo'])->name('quotation_orders.print_qo');
    Route::get('/quotation_orders/print_inv/{id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'printInv'])->name('quotation_orders.print_qo');
    Route::post('/quotation_orders/update/{id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'update'])->name('quotation_orders.update');

});

Route::group(['prefix' => 'pos','middleware'=>'auth'], function () {
    // $arrMenu = ['categories','chapters','products','purchases','suppliers','refunds','sales','reports','taxes'];
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSCategoryController::class, 'index'])->name('pos_categories.home');
        Route::get('/create', [App\Http\Controllers\pos\POSCategoryController::class, 'create'])->name('pos_categories.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSCategoryController::class, 'edit'])->name('pos_categories.edit');
    });

    Route::group(['prefix' => 'chapters'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSChapterController::class, 'index'])->name('pos_chapters.home');
        Route::get('/create', [App\Http\Controllers\pos\POSChapterController::class, 'create'])->name('pos_chapters.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSChapterController::class, 'edit'])->name('pos_chapters.edit');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSProductController::class, 'index'])->name('pos_products.home');
        Route::get('/create', [App\Http\Controllers\pos\POSProductController::class, 'create'])->name('pos_products.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSProductController::class, 'edit'])->name('pos_products.edit');
    });

    Route::group(['prefix' => 'purchases'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSPurchaseController::class, 'index'])->name('pos_purchases.home');
        Route::get('/create', [App\Http\Controllers\pos\POSPurchaseController::class, 'create'])->name('pos_purchases.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSPurchaseController::class, 'edit'])->name('pos_purchases.edit');
    });

    Route::group(['prefix' => 'suppliers'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSSupplierController::class, 'index'])->name('pos_suppliers.home');
        Route::get('/create', [App\Http\Controllers\pos\POSSupplierController::class, 'create'])->name('pos_suppliers.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSSupplierController::class, 'edit'])->name('pos_suppliers.edit');
    });

    Route::group(['prefix' => 'refunds'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSRefundController::class, 'index'])->name('pos_refunds.home');
        Route::get('/create', [App\Http\Controllers\pos\POSRefundController::class, 'create'])->name('pos_refunds.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSRefundController::class, 'edit'])->name('pos_refunds.edit');
    });

    Route::group(['prefix' => 'sales'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSSaleController::class, 'index'])->name('pos_sales.home');
        Route::get('/create', [App\Http\Controllers\pos\POSSaleController::class, 'create'])->name('pos_sales.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSSaleController::class, 'edit'])->name('pos_sales.edit');
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSReportController::class, 'index'])->name('pos_reports.home');
        Route::get('/create', [App\Http\Controllers\pos\POSReportController::class, 'create'])->name('pos_reports.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSReportController::class, 'edit'])->name('pos_reports.edit');
    });

    Route::group(['prefix' => 'taxes'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSTaxController::class, 'index'])->name('pos_taxes.home');
        Route::get('/create', [App\Http\Controllers\pos\POSTaxController::class, 'create'])->name('pos_taxes.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSTaxController::class, 'edit'])->name('pos_taxes.edit');
    });

    Route::group(['prefix' => 'barcode_symbologies'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'index'])->name('barcode_symbologies.home');
        Route::get('/create', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'create'])->name('barcode_symbologies.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'edit'])->name('barcode_symbologies.edit');
    });

    Route::group(['prefix' => 'unit_types'], function () {
        Route::get('/', [App\Http\Controllers\pos\POSUnitTypeController::class, 'index'])->name('unit_types.home');
        Route::get('/create', [App\Http\Controllers\pos\POSUnitTypeController::class, 'create'])->name('unit_types.create');
        Route::get('/edit/{id}', [App\Http\Controllers\pos\POSUnitTypeController::class, 'edit'])->name('unit_types.edit');
    });

});



//Get file Import Excel
Route::get('storage/{filename}', function ($filename) {
    $path = storage_path('app/public/file_import/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::post('/loginUsers', [App\Http\Controllers\AuthController::class, 'loginUser'])->name('auth.login');

Route::group(['prefix' => 'master','middleware'=>'auth'], function () {

    // banks
    Route::get('/banks', [App\Http\Controllers\master\BankController::class, 'index'])->name('home')->middleware('permission:bank.index');
    Route::get('/banks/search/datatables', [App\Http\Controllers\master\BankController::class, 'datatables'])->name('home')->middleware('permission:bank.datatables');
    Route::post('/banks/store', [App\Http\Controllers\master\BankController::class, 'store'])->name('store')->middleware('permission:bank.store');
    Route::post('/banks/update/{id}', [App\Http\Controllers\master\BankController::class, 'edit'])->name('store')->middleware('permission:bank.edit');
    Route::post('/banks/delete/{id}', [App\Http\Controllers\master\BankController::class, 'delete'])->name('delete')->middleware('permission:bank.delete');
    Route::get('/banks/edit/{id}', [App\Http\Controllers\master\BankController::class, 'dataUpdate'])->name('edit')->middleware('permission:bank.update');

    // payments
    // Route::get('/payments', [App\Http\Controllers\master\BankController::class, 'index'])->name('home');
    // Route::post('/payments/store', [App\Http\Controllers\master\BankController::class, 'store'])->name('store');
    // Route::post('/payments/update/{id}', [App\Http\Controllers\master\BankController::class, 'edit'])->name('store');
    // Route::post('/payments/delete/{id}', [App\Http\Controllers\master\BankController::class, 'delete'])->name('delete');
    // Route::get('/payments/edit/{id}', [App\Http\Controllers\master\BankController::class, 'dataUpdate'])->name('edit');

    // Payments
    Route::get('/payments', [App\Http\Controllers\master\PaymentController::class, 'index'])->name('home.paymnt')->middleware('permission:payment.index');
    Route::post('/payments/store', [App\Http\Controllers\master\PaymentController::class, 'store'])->name('store.paymnt')->middleware('permission:payment.store');
    Route::post('/payments/update/{id}', [App\Http\Controllers\master\PaymentController::class, 'edit'])->name('store.paymnt')->middleware('permission:payment.edit');
    Route::post('/payments/delete/{id}', [App\Http\Controllers\master\PaymentController::class, 'delete'])->name('delete.paymnt')->middleware('permission:payment.delete');
    Route::get('/payments/edit/{id}', [App\Http\Controllers\master\PaymentController::class, 'dataUpdate'])->name('edit.paymnt')->middleware('permission:payment.update');
    Route::get('/payments/select2', [App\Http\Controllers\master\PaymentController::class, 'paymentSelect2'])->name('select2.paymnt')->middleware('permission:payment.select2');

    // Category Spareparts
    Route::get('/category_spareparts', [App\Http\Controllers\master\CategorySparepartController::class, 'index'])->name('home');
    Route::post('/category_spareparts/store', [App\Http\Controllers\master\CategorySparepartController::class, 'store'])->name('store');
    Route::post('/category_spareparts/update/{id}', [App\Http\Controllers\master\CategorySparepartController::class, 'edit'])->name('store');
    Route::post('/category_spareparts/delete/{id}', [App\Http\Controllers\master\CategorySparepartController::class, 'delete'])->name('delete');
    Route::get('/category_spareparts/edit/{id}', [App\Http\Controllers\master\CategorySparepartController::class, 'dataUpdate'])->name('edit');
    Route::get('/category_spareparts/import_log_datatables', [App\Http\Controllers\master\CategorySparepartController::class, 'importLogdatatables'])->name('category_spareparts.import_log_datatables');
    Route::get('/category_spareparts/imported_by_datatables/{users_id}', [App\Http\Controllers\master\CategorySparepartController::class, 'importedByDatatables'])->name('category_spareparts.imported_by_datatables');

    // Companies
    Route::get('/companies', [App\Http\Controllers\master\CompanyController::class, 'index'])->name('companies.home')->middleware('permission:company.show');
    Route::get('/companies/create', [App\Http\Controllers\master\CompanyController::class, 'create'])->name('companies.create')->middleware('permission:company.create');
    Route::get('/companies/main-edit/{id}', [App\Http\Controllers\master\CompanyController::class, 'mainEdit'])->name('companies.main.edit')->middleware('permission:company.edit');
    Route::post('/companies/store', [App\Http\Controllers\master\CompanyController::class, 'store'])->name('companies.store')->middleware('permission:company.store');
    Route::post('/companies/update-main/{id}', [App\Http\Controllers\master\CompanyController::class, 'updateMain'])->name('companies.update.main')->middleware('permission:company.update');
    Route::post('/companies/destroy/{id}', [App\Http\Controllers\master\CompanyController::class, 'destroy'])->name('companies.destroy')->middleware('permission:company.destroy');

    Route::get('/branch-office', [App\Http\Controllers\master\CompanyController::class, 'indexBranch'])->name('companies.indexBranch')->middleware('permission:branch_office.index');  //branch
    Route::get('/companies/brach', [App\Http\Controllers\master\CompanyController::class, 'brach'])->name('companies.brach')->middleware('permission:branch_office.branch');            //branch
    Route::get('/companies/branch-edit/{id}', [App\Http\Controllers\master\CompanyController::class, 'brachEdit'])->name('companies.brach.edit')->middleware('permission:branch_office.edit');//branch
    Route::post('/companies/store-branch', [App\Http\Controllers\master\CompanyController::class, 'storeBranch'])->name('companies.store.branch')->middleware('permission:branch_office.store');
    Route::post('/companies/update-branch/{id}', [App\Http\Controllers\master\CompanyController::class, 'updateBranch'])->name('companies.update.branch')->middleware('permission:branch_office.update');
    Route::post('/companies/destroy-branch/{id}', [App\Http\Controllers\master\CompanyController::class, 'destroyBranch'])->name('companies.destroy-branch')->middleware('permission:branch_office.destroy');

    // BrandssaveData: function(el, e) {
    Route::get('/brands/edit/{id}', [App\Http\Controllers\master\BrandController::class, 'edit'])->name('brands.edit')->middleware('permission:brand.edit');
    Route::post('/brands/store', [App\Http\Controllers\master\BrandController::class, 'store'])->name('brands.store')->middleware('permission:brand.store');
    Route::post('/brands/update/{id}', [App\Http\Controllers\master\BrandController::class, 'update'])->name('brands.update')->middleware('permission:brand.update');
    Route::post('/brands/destroy/{id}', [App\Http\Controllers\master\BrandController::class, 'destroy'])->name('brands.destroy')->middleware('permission:brand.destroy');   // baru sampe sini

    // spareparts
    Route::get('/spareparts', [App\Http\Controllers\master\SparepartController::class, 'index'])->name('spareparts.home')->middleware('permission:sparepart.home');
    Route::get('/spareparts/create', [App\Http\Controllers\master\SparepartController::class, 'create'])->name('spareparts.create')->middleware('permission:sparepart.create');
    Route::get('/spareparts/datatables', [App\Http\Controllers\master\SparepartController::class, 'datatables'])->name('spareparts.datatables')->middleware('permission:sparepart.datatables');
    Route::get('/spareparts/select2', [App\Http\Controllers\master\SparepartController::class, 'select2'])->name('spareparts.select2')->middleware('permission:sparepart.select2');
    Route::get('/spareparts/update/{id}', [App\Http\Controllers\master\SparepartController::class, 'update'])->name('spareparts.update')->middleware('permission:sparepart.update');
    Route::post('/spareparts/store', [App\Http\Controllers\master\SparepartController::class, 'store'])->name('spareparts.store')->middleware('permission:sparepart.store');
    Route::post('/spareparts/edit/{id}', [App\Http\Controllers\master\SparepartController::class, 'edit'])->name('spareparts.edit')->middleware('permission:sparepart.edit');
    Route::post('/spareparts/destroy/{id}', [App\Http\Controllers\master\SparepartController::class, 'destroy'])->name('spareparts.destroy')->middleware('permission:sparepart.destroy');

    // Customers
    Route::get('/customers', [App\Http\Controllers\master\CustomerController::class, 'index'])->name('customers.home')->middleware('permission:customer.home');
    Route::get('/customers/create', [App\Http\Controllers\master\CustomerController::class, 'create'])->name('customers.cretae')->middleware('permission:customer.cretae');
    Route::get('/customers/update/{id}', [App\Http\Controllers\master\CustomerController::class, 'update'])->name('customers.update')->middleware('permission:customer.update');
    Route::post('/customers/store', [App\Http\Controllers\master\CustomerController::class, 'store'])->name('customers.store')->middleware('permission:customer.store');
    Route::post('/customers/edit/{id}', [App\Http\Controllers\master\CustomerController::class, 'edit'])->name('customers.edit')->middleware('permission:customer.edit');
    Route::post('/customers/destroy/{id}', [App\Http\Controllers\master\CustomerController::class, 'destroy'])->name('customers.destroy')->middleware('permission:customer.destroy');

    // Services
    Route::get('/services', [App\Http\Controllers\master\ServiceController::class, 'index'])->name('services.home');
    Route::get('/services/datatables', [App\Http\Controllers\master\ServiceController::class, 'datatables'])->name('services.datatables');
    Route::get('/services/select2', [App\Http\Controllers\master\ServiceController::class, 'select2'])->name('services.select2');
    Route::get('/services/create', [App\Http\Controllers\master\ServiceController::class, 'create'])->name('services.cretae');
    Route::get('/services/update/{id}', [App\Http\Controllers\master\ServiceController::class, 'update'])->name('services.update');
    Route::post('/services/store', [App\Http\Controllers\master\ServiceController::class, 'store'])->name('services.store');
    Route::post('/services/edit/{id}', [App\Http\Controllers\master\ServiceController::class, 'edit'])->name('services.edit');
    Route::post('/services/destroy/{id}', [App\Http\Controllers\master\ServiceController::class, 'destroy'])->name('services.destroy');
    Route::get('/services/import_log_datatables', [App\Http\Controllers\master\ServiceController::class, 'importLogdatatables'])->name('services.import_log_datatables');
    Route::get('/services/imported_by_datatables/{users_id}', [App\Http\Controllers\master\ServiceController::class, 'importedByDatatables'])->name('services.imported_by_datatables');


    // model part
    Route::get('/model-part', [App\Http\Controllers\master\ModelPartController::class, 'index'])->name('model.part.home');
    Route::get('/model-part/create', [App\Http\Controllers\master\ModelPartController::class, 'create'])->name('model.part.cretae');
    Route::get('/model-part/update/{id}', [App\Http\Controllers\master\ModelPartController::class, 'update'])->name('model.part.update');
    Route::post('/model-part/store', [App\Http\Controllers\master\ModelPartController::class, 'store'])->name('model.part.store');
    Route::post('/model-part/edit/{id}', [App\Http\Controllers\master\ModelPartController::class, 'edit'])->name('model.part.edit');
    Route::post('/model-part/destroy/{id}', [App\Http\Controllers\master\ModelPartController::class, 'destroy'])->name('model.part.destroy');
    Route::get('/model-part/import_log_datatables', [App\Http\Controllers\master\ModelPartController::class, 'importLogdatatables'])->name('model.part.import_log_datatables');
    Route::get('/model-part/imported_by_datatables/{users_id}', [App\Http\Controllers\master\ModelPartController::class, 'importedByDatatables'])->name('model.part.imported_by_datatables');
});



Route::group(['prefix' => 'utility','middleware'=>'auth'], function () {
    Route::get('/list-user', [App\Http\Controllers\utility\ListUsersController::class, 'index'])->middleware('permission:list_user.show-list');
    Route::get('/list-user/create', [App\Http\Controllers\utility\ListUsersController::class, 'create'])->middleware('permission:list_user.created');
    Route::get('/list-user/update/{id}', [App\Http\Controllers\utility\ListUsersController::class, 'update'])->middleware('permission:list_user.updated');
    Route::post('/list-user/store', [App\Http\Controllers\utility\ListUsersController::class, 'store'])->middleware('permission:list_user.stored');
    Route::post('/list-user/edit/{id}', [App\Http\Controllers\utility\ListUsersController::class, 'edit'])->middleware('permission:list_user.edited');
    Route::post('/list-user/destroy/{id}', [App\Http\Controllers\utility\ListUsersController::class, 'destroy'])->middleware('permission:list_user.destroyed');
    Route::get('/list-user/datatables', [App\Http\Controllers\utility\ListUsersController::class, 'datatables'])->name('datatables');
    Route::get('/list-user/select2/company', [App\Http\Controllers\utility\ListUsersController::class, 'select2Company'])->name('select.company');
    Route::get('/list-user/select2/branch/{id}', [App\Http\Controllers\utility\ListUsersController::class, 'select2Branch'])->name('select.branch');
});

Route::group(['prefix' => 'system-setting','middleware'=>'auth'], function () {
    Route::get('/my-profile', [App\Http\Controllers\system_setting\MyProfileController::class, 'index'])->name('home')->middleware('permission:profiles.home');
    Route::post('/my-profile/upload-image/{id}', [App\Http\Controllers\system_setting\MyProfileController::class, 'updateImage'])->name('updateImage')->middleware('permission:profiles.updateImage');
    Route::post('/my-profile/update/{id}', [App\Http\Controllers\system_setting\MyProfileController::class, 'update'])->name('profile.update')->middleware('permission:profiles.update');
    Route::post('/my-profile/forget-password', [App\Http\Controllers\system_setting\MyProfileController::class, 'changePassword'])->name('forgot.password')->middleware('permission:profiles.password');
});

Route::group(['prefix' => 'role'], function () {
    Route::get('show', [App\Http\Controllers\RoleController::class, 'index'])->name('index.role');
    Route::get('datatables', [App\Http\Controllers\RoleController::class, 'datatables'])->name('index.role.datatables');
    Route::get('create', [App\Http\Controllers\RoleController::class, 'create']);
    Route::get('update/{id}', [App\Http\Controllers\RoleController::class, 'edit']);
});

Route::group(['prefix' => 'permission'], function () {
    Route::get('show', [App\Http\Controllers\PermissionController::class, 'index'])->name('index.permission');
    Route::get('datatables', [App\Http\Controllers\PermissionController::class, 'datatables'])->name('index.permission.datatables');
    Route::get('create', [App\Http\Controllers\PermissionController::class, 'create']);
    Route::post('store', [App\Http\Controllers\PermissionController::class, 'store'])->name('permission.store');
    Route::get('update/{id}', [App\Http\Controllers\PermissionController::class, 'edit']);
});

Route::post('/roles', [App\Http\Controllers\RoleController::class, 'store'])->name('admin.role.store');
Route::put('/roles/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('admin.role.update');

