<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [App\Http\Controllers\AuthController::class, 'loginApi'])->name('auth.login');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'auth'], function () {
    Route::group(['prefix' => 'master-datatables'], function () {

        // services
        Route::get('/services/search/datatables', [App\Http\Controllers\master\ServiceController::class, 'serviceSearch'])->name('services.search');
        Route::get('/services/datatables', [App\Http\Controllers\master\ServiceController::class, 'datatables'])->name('services.datatables');

        // bank
        Route::get('/bank/search/datatables', [App\Http\Controllers\master\BankController::class, 'bankSearch'])->name('bank.search');
        Route::get('/payment/search/datatables', [App\Http\Controllers\master\PaymentController::class, 'paymentSearch'])->name('payment.search');

        // brand
        Route::get('/brand/search/datatables', [App\Http\Controllers\master\BrandController::class, 'brandSearch'])->name('brand.search');

        // model part
        Route::get('/model-part/search/datatables', [App\Http\Controllers\master\ModelPartController::class, 'datatables'])->name('model.part.datatables');
        Route::get('/model-part/import_log_datatables', [App\Http\Controllers\master\ModelPartController::class, 'importLogdatatables'])->name('model.part.import_log_datatables');
        Route::get('/model-part/imported_by_datatables/{users_id}', [App\Http\Controllers\master\ModelPartController::class, 'importedByDatatables'])->name('model.part.imported_by_datatables');

        // services
        Route::get('/services/import_log_datatables', [App\Http\Controllers\master\ServiceController::class, 'importLogdatatables'])->name('services.import_log_datatables');
        Route::get('/services/imported_by_datatables/{users_id}', [App\Http\Controllers\master\ServiceController::class, 'importedByDatatables'])->name('services.imported_by_datatables');

        // category_spareparts
        Route::get('/category_spareparts/import_log_datatables', [App\Http\Controllers\master\CategorySparepartController::class, 'importLogdatatables'])->name('category_spareparts.import_log_datatables');
        Route::get('/category_spareparts/imported_by_datatables/{users_id}', [App\Http\Controllers\master\CategorySparepartController::class, 'importedByDatatables'])->name('category_spareparts.imported_by_datatables');


    });

    Route::group(['prefix' => 'master'], function () {

        // Branch
        Route::group(['prefix' => 'branches'], function () {
            Route::get('/select2', [App\Http\Controllers\master\CompanyController::class, 'select2Branch'])->name('branches.select2');
        });

    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::group(['prefix' => 'quotation_orders'], function () {
            Route::get('/payment_details/datatables/{quotation_orders_id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'paymentDetailDatatables'])->name('quotation_orders.payment_details.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\transaction\QuotationOrderController::class, 'datatablesSearch'])->name('quotation_orders.datatablesSearch');
            Route::get('/datatables', [App\Http\Controllers\transaction\QuotationOrderController::class, 'dataTables'])->name('quotation_orders.datatables');
            Route::delete('/destroy_details_data/{detail_id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'destroyDetail'])->name('quotation_orders.destroy_details_data');
            Route::delete('/delete/{id}', [App\Http\Controllers\transaction\QuotationOrderController::class, 'delete'])->name('quotation_orders.delete');
        });

    });
    Route::group(['prefix' => 'pos'], function () {
        // $arrMenu = ['categories','chapters','products','purchases','suppliers','refunds','sales','reports','taxes'];
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSCategoryController::class, 'datatables'])->name('pos_categories.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSCategoryController::class, 'datatablesSearch'])->name('pos_categories.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSCategoryController::class, 'select2'])->name('pos_categories.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSCategoryController::class, 'store'])->name('pos_categories.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSCategoryController::class, 'update'])->name('quotation_orders.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSCategoryController::class, 'update'])->name('quotation_orders.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSCategoryController::class, 'delete'])->name('pos_categories.delete');
        });
        Route::group(['prefix' => 'chapters'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSChapterController::class, 'datatables'])->name('pos_chapters.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSChapterController::class, 'datatablesSearch'])->name('pos_chapters.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSChapterController::class, 'select2'])->name('pos_chapters.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSChapterController::class, 'store'])->name('pos_chapters.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSChapterController::class, 'update'])->name('pos_chapters.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSChapterController::class, 'update'])->name('pos_chapters.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSChapterController::class, 'delete'])->name('pos_chapters.delete');
        });
        Route::group(['prefix' => 'products'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSProductController::class, 'datatables'])->name('pos_products.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSProductController::class, 'datatablesSearch'])->name('pos_products.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSProductController::class, 'select2'])->name('pos_products.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSProductController::class, 'store'])->name('pos_products.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSProductController::class, 'update'])->name('pos_products.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSProductController::class, 'update'])->name('pos_products.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSProductController::class, 'delete'])->name('pos_products.delete');
        });
        Route::group(['prefix' => 'purchases'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSPurchaseController::class, 'datatables'])->name('pos_purchases.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSPurchaseController::class, 'datatablesSearch'])->name('pos_purchases.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSPurchaseController::class, 'select2'])->name('pos_purchases.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSPurchaseController::class, 'store'])->name('pos_purchases.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSPurchaseController::class, 'update'])->name('pos_purchases.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSPurchaseController::class, 'update'])->name('pos_purchases.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSPurchaseController::class, 'delete'])->name('pos_purchases.delete');
        });
        Route::group(['prefix' => 'suppliers'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSSupplierController::class, 'datatables'])->name('pos_suppliers.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSSupplierController::class, 'datatablesSearch'])->name('pos_suppliers.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSSupplierController::class, 'select2'])->name('pos_suppliers.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSSupplierController::class, 'store'])->name('pos_suppliers.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSSupplierController::class, 'update'])->name('pos_suppliers.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSSupplierController::class, 'update'])->name('pos_suppliers.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSSupplierController::class, 'delete'])->name('pos_suppliers.delete');
        });
        Route::group(['prefix' => 'refunds'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSRefundController::class, 'datatables'])->name('pos_refunds.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSRefundController::class, 'datatablesSearch'])->name('pos_refunds.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSRefundController::class, 'select2'])->name('pos_refunds.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSRefundController::class, 'store'])->name('pos_refunds.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSRefundController::class, 'update'])->name('pos_refunds.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSRefundController::class, 'update'])->name('pos_refunds.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSRefundController::class, 'delete'])->name('pos_refunds.delete');
        });
        Route::group(['prefix' => 'sales'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSSaleController::class, 'datatables'])->name('pos_sales.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSSaleController::class, 'datatablesSearch'])->name('pos_sales.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSSaleController::class, 'select2'])->name('pos_sales.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSSaleController::class, 'store'])->name('pos_sales.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSSaleController::class, 'update'])->name('pos_sales.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSSaleController::class, 'update'])->name('pos_sales.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSSaleController::class, 'delete'])->name('pos_sales.delete');
        });
        Route::group(['prefix' => 'reports'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSReportController::class, 'datatables'])->name('pos_reports.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSReportController::class, 'datatablesSearch'])->name('pos_reports.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSReportController::class, 'select2'])->name('pos_reports.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSReportController::class, 'store'])->name('pos_reports.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSReportController::class, 'update'])->name('pos_reports.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSReportController::class, 'update'])->name('pos_reports.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSReportController::class, 'delete'])->name('pos_reports.delete');
        });
        Route::group(['prefix' => 'taxes'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSTaxController::class, 'datatables'])->name('pos_taxes.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSTaxController::class, 'datatablesSearch'])->name('pos_taxes.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSTaxController::class, 'select2'])->name('pos_taxes.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSTaxController::class, 'store'])->name('pos_taxes.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSTaxController::class, 'update'])->name('pos_taxes.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSTaxController::class, 'update'])->name('pos_taxes.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSTaxController::class, 'delete'])->name('pos_taxes.delete');
        });
        Route::group(['prefix' => 'barcode_symbologies'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'datatables'])->name('barcode_symbologies.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'datatablesSearch'])->name('barcode_symbologies.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'select2'])->name('barcode_symbologies.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'store'])->name('barcode_symbologies.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'update'])->name('barcode_symbologies.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'update'])->name('barcode_symbologies.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSBarcodeSymbologyController::class, 'delete'])->name('barcode_symbologies.delete');
        });
        Route::group(['prefix' => 'unit_types'], function () {
            Route::get('/datatables', [App\Http\Controllers\pos\POSUnitTypeController::class, 'datatables'])->name('unit_types.datatables');
            Route::get('/datatables/search', [App\Http\Controllers\pos\POSUnitTypeController::class, 'datatablesSearch'])->name('unit_types.datatablesSearch');
            Route::get('/select2', [App\Http\Controllers\pos\POSUnitTypeController::class, 'select2'])->name('unit_types.select2');
            Route::post('/store', [App\Http\Controllers\pos\POSUnitTypeController::class, 'store'])->name('unit_types.store');
            Route::post('/update/{id}', [App\Http\Controllers\pos\POSUnitTypeController::class, 'update'])->name('unit_types.update');
            Route::put('/update/{id}', [App\Http\Controllers\pos\POSUnitTypeController::class, 'update'])->name('unit_types.update');
            Route::delete('/{id}', [App\Http\Controllers\pos\POSUnitTypeController::class, 'delete'])->name('unit_types.delete');
        });

    });


    //IMPORT EXCEL
    Route::post('/import-excel', [App\Http\Controllers\master\ImportExcelController::class, 'importExcel'])->name('function.import');
    Route::group(['prefix' => 'master'], function () {
        Route::group(['prefix' => 'import'], function () {
            Route::post('/category_spareparts', [App\Http\Controllers\master\ImportExcelController::class, 'saveDataCategorySpareparts'])->name('category_spareparts.import');
            Route::post('/services', [App\Http\Controllers\master\ImportExcelController::class, 'saveDataServices'])->name('services.import');
            Route::post('/model_parts', [App\Http\Controllers\master\ImportExcelController::class, 'saveDataModelParts'])->name('model_parts.import');
        });
    });

    // get provinsi dan kota
    Route::get('/get_city/{id}', [App\Http\Controllers\getCityAndProvController::class, 'getCity'])->name('get.city');
    Route::get('/get_province', [App\Http\Controllers\getCityAndProvController::class, 'getProvince'])->name('get.province');
});
