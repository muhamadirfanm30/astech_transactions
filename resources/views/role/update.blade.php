@extends('layouts.master')
@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="card-title">
                                    List Role 
                                </h3>
                            </div>
                            <div class="col-md-6 text-right">
                                {{-- <a href="{{url('role/create')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i>&nbsp;
                                    Add Role
                                </a> --}}
                            </div>
                        </div>
                    
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.role.update', ['id' => $role->id]) }}" method="post" id="multi">
                            @csrf
                            @method('PUT')
                            <label>Name</label>
                            <div class="form-group">
                                <input name="name" placeholder="enter name" type="text" value="{{ old('name', $role->name) }}" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                                <label>Permission</label>
                            <br />
                                <input id="" type="checkbox" class="permission-checkbox-all"> Check All
                            <hr />
                            <div class="row">
                                @foreach ($permissions as $nameGrup => $group)
                                    <div class="col-md-3" style="margin-bottom: 10px;">
                                        <div class="card">
                                            <div class="card-body">
                                                <input id="" type="checkbox" class="permission-checkbox-parent" data-id="{{ $nameGrup }}">
                                                <label>{{ $no++ }}. {{ str_replace('_', ' ', ucfirst($nameGrup)) }}</label>
                                                @foreach ($group as $parent)
                                                <?php 
                                                    $checked = $role->permissions->where('id', $parent->id)->first() == null ? '' : 'checked';
                                                ?>
                                                <div class="checkbox">
                                                    <input id="checkbox-{{ $parent->id }}" type="checkbox" name="permissions[]" value="{{ $parent->id }}" class="permission-checkbox p-c-{{ $nameGrup }}" {{ $checked }}>
                                                    <label for="checkbox-{{ $parent->id }}">{{ $parent->name }}</label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="position-relative">
                                <div class="col-sm-10 offset-sm-2">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </section>


@endsection
@include('role.js')
@section('script')
<script src="https://kendo.cdn.telerik.com/2020.1.219/js/kendo.all.min.js"></script>
<script>
    $('.permission-checkbox-all').change(function() {
        if (this.checked) {
            $('.permission-checkbox').prop('checked', true);
        } else {
            $('.permission-checkbox').prop('checked', false);
        }
    })

    $('.permission-checkbox-parent').change(function() {
        id = $(this).attr('data-id');
        if (this.checked) {
            $('.p-c-' + id).prop('checked', true);
        } else {
            $('.p-c-' + id).prop('checked', false);
        }
    })
</script>
@endsection