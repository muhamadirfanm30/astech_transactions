@extends('layouts.master')
@section('content')

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title">
                                        List Role 
                                    </h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('role/create')}}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i>&nbsp;
                                        Add Role
                                    </a>
                                </div>
                            </div>
                        
                        </div>
                        <div class="card-body">
                            <table id="example" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name </th>
                                        <th>Guard Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>


@endsection
@section('script')
<script>
    globalCRUD.datatables({
        url: '/role/datatables',
        selector: '#example',
        columnsField: ['DT_RowIndex', 'name', 'guard_name'],
        actionLink: {
            update: function(row) {
                return "/role/update/" + row.id;
            },
            delete: function(row) {
                return "/role/" + row.id;
            }
        }
    })
</script>
@endsection
