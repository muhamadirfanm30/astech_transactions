<head>
    <title>Main Office List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="{{ url('/master/companies') }}" ><strong>Main Office</strong></a>
                        <a class="nav-item nav-link" href="{{ url('/master/branch-office') }}">Branch Office</a>
                    </div>
                </div>
            </nav>
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List companies
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('/master/companies/create')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add main office
                            </a>
                        </div>
                    </div>

                </div>
                <div class="card-body">

                    <table id="categori_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code </th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @foreach ($showData as $val)
                            <tbody>
                                <td>{{$val->code}}</td>
                                <td>{{$val->name}}</td>
                                <td>{{$val->city}}</td>
                                <td>{{$val->email}}</td>
                                <td>
                                    <form action="{{ url('master/companies/destroy/'.$val->id) }}" method="POST" id="deleteBrand-{{$val->id}}">
                                        @csrf
                                    </form>
                                    <a href="{{ url('master/companies/main-edit/'.$val->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $val->id }})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#categori_table').DataTable({});
    } );

    function deleteData(id){


       swal({
           title: "Alert",
           text: "Are you sure you want to delete the Branch data?",
           icon: "warning",
           buttons: true,
           dangerMode: true,
       })
       .then((willDelete) => {
           if (willDelete) {
               document.getElementById('deleteBrand-'+id).submit();
           } else {
               swal("Your imaginary file is safe!");
           }
       });
   }
</script>
@endsection
