<head>
    <title>Main Office Update | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Create companies
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('/master/companies')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-back"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>

                </div>
                <form action="{{ url('/master/companies/update-main/'.$mainOffices->id) }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">companies Code *</label>
                                        <input type="text" name="code" class="form-control" placeholder="companies Code" value="{{$mainOffices->code}}">
                                        @error('code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">companies Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="companies Name" value="{{$mainOffices->name}}">
                                        @error('name')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address 1 *</label>
                                <textarea name="address_1" id="" cols="30" rows="2" class="form-control" style="width:100%">{{$mainOffices->address_1}}</textarea>
                                @error('address_1')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Address 2</label>
                                <textarea name="address_2" id="" cols="30" rows="2" class="form-control" style="width:100%">{{$mainOffices->address_2}}</textarea>

                            </div>
                            <div class="form-group">
                                <label for="">Address 3</label>
                                <textarea name="address_3" id="" cols="30" rows="2" class="form-control" style="width:100%">{{$mainOffices->address_3}}</textarea>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">City *</label>
                                        <input type="text" name="city" class="form-control" placeholder="City" value="{{$mainOffices->city}}">
                                        @error('city')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fax *</label>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax" value="{{$mainOffices->fax}}">
                                        @error('fax')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Phone *</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone" value="{{$mainOffices->phone}}">
                                        @error('phone')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="">Email *</label>
                                        <input type="text" name="email" class="form-control" placeholder="Email" value="{{$mainOffices->email}}">
                                        @error('email')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label for="">Tax Address *</label>
                                <textarea name="tax_address" id="" cols="30" rows="2" class="form-control" style="width:100%">{{$mainOffices->tax_address}}</textarea>
                                @error('tax_address')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Bank Name *</label>
                                        <input type="text" name="bank_name" class="form-control" placeholder="Bank Name" value="{{$mainOffices->bank_name}}">
                                        @error('bank_name')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Bank Account *</label>
                                        <input type="text" name="bank_account" class="form-control" placeholder="Bank Account" value="{{$mainOffices->bank_account}}">
                                        @error('bank_account')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <button type="submit" class="btn btn-success">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#categori_table').DataTable({});
    } );
</script>
@endsection
