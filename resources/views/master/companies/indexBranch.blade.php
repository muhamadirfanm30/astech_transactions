<head>
    <title>Branch Office List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="{{ url('/master/companies') }}" >Main Office</a>
                        <a class="nav-item nav-link active" href="{{ url('/master/branch-office') }}"><strong>Branch Office</strong></a>
                    </div>
                </div>
            </nav>
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Branch Office
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('/master/companies/brach')}}" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add branch office
                            </a>
                        </div>
                    </div>

                </div>
                <div class="card-body">

                    <table id="categori_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code </th>
                                <th>Name</th>
                                <th>Main Office</th>
                                <th>Province</th>
                                <th>City</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @foreach ($showBranch as $val)
                            <tbody>
                                <td>{{$val->code}}</td>
                                <td>{{$val->name}}</td>
                                <td>{{!empty($val->company) ? $val->company->name : '-'}}</td>
                                <td>{{!empty($val->prov) ? $val->prov->name : '-'}}</td>
                                <td>{{!empty($val->city) ? $val->city->name : '-'}}</td>
                                <td>
                                    <form action="{{ url('master/companies/destroy-branch/'.$val->id) }}" method="POST" id="deleteBrand-{{$val->id}}">
                                        @csrf
                                    </form>
                                    <a href="{{ url('master/companies/branch-edit/'.$val->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $val->id }})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#categori_table').DataTable({});
    } );

    function deleteData(id){


       swal({
           title: "Alert",
           text: "Are you sure you want to delete the Branch data?",
           icon: "warning",
           buttons: true,
           dangerMode: true,
       })
       .then((willDelete) => {
           if (willDelete) {
               document.getElementById('deleteBrand-'+id).submit();
           } else {
               swal("Your imaginary file is safe!");
           }
       });
   }
</script>
@endsection
