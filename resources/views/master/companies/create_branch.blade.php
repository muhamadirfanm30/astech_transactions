<head>
    <title>Branch Office Update | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session()->get('success') }}</strong>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session()->get('error') }}</strong>
                </div>
            @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Create Branch Office Data
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            {{-- <a href="{{url('/master/company')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-back"></i>&nbsp;
                                Back
                            </a> --}}
                        </div>
                    </div>

                </div>
                <form action="{{ url('/master/companies/store-branch') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Choose Main Office *</label>
                            <select class="form-control" name="ms_branches_id" id="exampleFormControlSelect1">
                                <option value="">Choose Main Office</option>
                                @foreach ($getMainOffice as $item)
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('ms_branches_id')
                                <span style="color:red"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6" id="provHide">
                                    <div class="form-group">
                                        <label>Provinsi</label>
                                        <select class="form-control" name="provinces_id" id="province_id" style="width:100%">
                                            <option>Pilih Provinsi</option>
                                        </select>
                                        @error('provinces_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    {{-- <input type="hidden" name="get_prov" id="get_prov" /> --}}
                                </div>
                                <div class="col-sm-6" id="cityHide">
                                    <div class="form-group">
                                        <label>Kota / Kabupaten</label>
                                        <select class="form-control" name="cities_id" id="kota_id" style="width:100%">
                                            <option>Pilih Provinsi Terlebih dahulu</option>
                                        </select>
                                        @error('cities_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    {{-- <input type="hidden" name="get_kota" id="get_kota" /> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Code *</label>
                                        <input type="text" name="code" class="form-control" placeholder="Company Code" value="{{Request::old('code')}}">
                                        @error('code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="Company Name" value="{{Request::old('name')}}">
                                        @error('name')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address  *</label>
                                <textarea name="address" id="" cols="30" rows="2" class="form-control" style="width:100%">{{Request::old('address')}}</textarea>
                                @error('address')
                                    <span style="color:red"><small>{{ $message }}</small></span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">phone *</label>
                                        <input type="text" name="phone" class="form-control" placeholder="phone" value="{{Request::old('phone')}}">
                                        @error('phone')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Fax *</label>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax" value="{{Request::old('fax')}}">
                                        @error('fax')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Head Of Branch *</label>
                                        <input type="text" name="head_of_branch" class="form-control" placeholder="head of branch" value="{{Request::old('head_of_branch')}}">
                                        @error('head_of_branch')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">head of departement *</label>
                                        <input type="text" name="head_of_departement" class="form-control" placeholder="head of departement" value="{{Request::old('head_of_departement')}}">
                                        @error('head_of_departement')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Choose Branch Area</label>
                                        <select class="form-control" name="branch_area" id="branch_area" style="width:100%">
                                            <option>Choose Branch Area</option>
                                            <option value="AREA-01">AREA-01</option>
                                            <option value="AREA-02">AREA-02</option>
                                        </select>
                                        @error('branch_area')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Choose type</label>
                                        <select class="form-control" name="type" id="type" style="width:100%">
                                            <option>Choose type</option>
                                            <option value="BRANCH">BRANCH</option>
                                            <option value="PARTNER">PARTNER</option>
                                        </select>
                                        @error('type')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Choose Bank</label>
                                        <select class="form-control" name="ms_banks_id" id="ms_banks_id" style="width:100%">
                                            <option>Choose Bank</option>
                                            @foreach ($getBank as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_banks_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Rekening No *</label>
                                        <input type="number" name="rekening_no" class="form-control" placeholder="Rekening No" value="{{Request::old('rekening_no')}}">
                                        @error('rekening_no')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Credit Limit *</label>
                                        <input type="number" name="credit_limit" class="form-control" placeholder="Credit Limit" value="{{Request::old('credit_limit')}}">
                                        @error('credit_limit')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Item Limit *</label>
                                        <input type="number" name="item_limit" class="form-control" placeholder="Item Limit" value="{{Request::old('item_limit')}}">
                                        @error('item_limit')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Branch Principal *</label>
                                        <input type="text" name="branch_principal" class="form-control" placeholder="Branch Principal" value="{{Request::old('branch_principal')}}">
                                        @error('branch_principal')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Service Percent *</label>
                                        <input type="text" name="service_percent" class="form-control" placeholder="Service Percent" value="{{Request::old('service_percent')}}">
                                        @error('service_percent')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Sales Percent *</label>
                                        <input type="text" name="sales_percent" class="form-control" placeholder="Service Percent" value="{{Request::old('sales_percent')}}">
                                        @error('sales_percent')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Down Payment *</label>
                                        <input type="text" name="down_payment" class="form-control" placeholder="Down Payment" value="{{Request::old('down_payment')}}">
                                        @error('down_payment')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Check Unit Price *</label>
                                        <input type="text" name="check_unit_price" class="form-control" placeholder="Check Unit Price" value="{{Request::old('check_unit_price')}}">
                                        @error('check_unit_price')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <button type="submit" class="btn btn-success">Save Branch Office</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#categori_table').DataTable({});
    } );

    function ajaxRequest(type, url, callbackSuccess = null) {
        $.ajax({
            url: url,
            type: type,
            success: function(response) {
                if (callbackSuccess != null) {
                    callbackSuccess(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    $(document).ready(function(){
        ajaxRequest('GET', '/v2/api/get_province', function(response) {
            console.log(response[0].name)
            var select = '<option value="">Pilih Provinsi</option>';
            $.each(response, function(item,item2) {
                console.log(item2)
                select += `<option value="${item2.id}" data-name="${item2.name}">${item2.name}</option>` ;
            });
            Helper.loadingStop()
            $('#province_id').html(select)
        })
                

        // on chnege provinsi
        $('#province_id').change(function() {
            province_id = $(this).val();
            console.log(province_id)
            if(province_id == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Provinsi Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                Helper.loadingStart()
                ajaxRequest('GET', '/v2/api/get_city/' + province_id, function(response) {
                    console.log(response)
                    var city = '<option value="">Pilih Kota / Kabupaten</option>';
                    $.each(response, function(item,item2) {
                        city += `<option value="${item2.id}">${item2.name}</option>`;
                    });
                    Helper.loadingStop()
                    $('#kota_id').html(city)
                })
                // empty select
                $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
            }
        })

        // on chnege city
        // $('#kota_id').change(function() {
        //     city_id = $(this).val();
        //     if(city_id == ""){
        //         swal({
        //             title: "Ups!",
        //             text: "Silahkan Pilih Kota / Kabupaten Yang Tersedia! ",
        //             type: 'error'
        //         });
        //     }else{
        //         var get_kota_id =  $("#kota_id option:selected").text()
        //         $('#get_kota').val(get_kota_id);
        //         Helper.loadingStart()
        //         // load subdistrict
        //         ajaxRequest('GET', '/api/rajaongkir/subdistrict?city=' + city_id, function(response) {
        //             var distrik = '<option value="">Pilih Kecamatan</option>';
        //             $.each(response.data.rajaongkir.results, function(item,item2) {
        //                 distrik += `<option value="${item2.subdistrict_id}">${item2.subdistrict_name}</option>`;
        //             })
        //             Helper.loadingStop()
        //             $('#distrik_id').html(distrik)
        //         })
        //     }
        // })
    })
</script>
@endsection
