<head>
    <title>Sparepart List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Sparepart 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('/master/spareparts/create')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Sparepart
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    
                    <table id="categori_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code Part </th>
                                <th>Information</th>
                                <th>Price </th>
                                <th>Dealer 1</th>
                                <th>Dealer 2</th>
                                <th>selling price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @foreach ($showSparepart as $val)
                            <tbody>
                                <td>{{$val->sparepart_code}}</td>
                                <td>{{$val->description}}</td>
                                <td>Rp.{{number_format($val->cogs_price)}}</td>
                                <td>Rp.{{number_format($val->supplier_price_1)}}</td>
                                <td>Rp.{{number_format($val->supplier_price_2)}}</td>
                                <td>Rp.{{number_format($val->selling_price)}}</td>
                                <td>
                                    <form action="{{ url('master/spareparts/destroy/'.$val->id) }}" method="POST" id="deletePart-{{$val->id}}">
                                        @csrf
                                    </form>
                                    <a href="{{ url('master/spareparts/update/'.$val->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $val->id }})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#categori_table').DataTable({});
    } );

    function deleteData(id){
       

       swal({
           title: "Alert",
           text: "Are you sure you want to delete the Branch data?",
           icon: "warning",
           buttons: true,
           dangerMode: true,
       })
       .then((willDelete) => {
           if (willDelete) {
               document.getElementById('deletePart-'+id).submit();
           } else {
               swal("Your imaginary file is safe!");
           }
       });
   }
</script>
@endsection
