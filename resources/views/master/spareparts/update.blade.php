<head>
    <title>Sparepart Update  | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Customer 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('master/spareparts')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    <form action="{{ url('/master/spareparts/edit/'.$dataEdit->id) }}" method="post">
                        @csrf
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">ID Product</label>
                                        <input type="text" class="form-control" name="id_product" value="{{ $dataEdit->id_product }}" placeholder="ID Service" readonly>
                                        @error('id_product')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Category</label>
                                        <select class="form-control" name="ms_categories_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Status</option>
                                            @foreach ($showCategory as $cat)
                                                <option value="{{ $cat->id }}" {{ $cat->id == $dataEdit->ms_categories_id ? 'selected' : '' }}>{{ $cat->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_categories_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company</label>
                                        <select class="form-control" name="ms_companies_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Company</option>
                                            @foreach ($showCompany as $comp)
                                                <option value="{{ $comp->id }}" {{ $comp->id == $dataEdit->ms_companies_id ? 'selected' : '' }}>{{ $comp->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_companies_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Brand</label>
                                        <select class="form-control" name="ms_brands_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Brand</option>
                                            @foreach ($showBrand as $brand)
                                                <option value="{{ $brand->id }}" {{ $brand->id == $dataEdit->ms_brands_id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_brands_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Part Code</label>
                                        <input type="text" class="form-control" name="sparepart_code" value="{{ $dataEdit->sparepart_code }}" placeholder="Part Code">
                                        @error('sparepart_code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Division Category</label>
                                        <input type="text" class="form-control" name="division_category" value="{{ $dataEdit->division_category }}" placeholder="Division Category">
                                        @error('division_category')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">UOM</label>
                                        <select class="form-control" name="uom" id="exampleFormControlSelect1">
                                            <option value="">Choose Service Type</option>
                                            <option value="BAG" {{ "BAG" == $dataEdit->uom ? 'selected' : '' }}>BAG</option>
                                            <option value="BAL" {{ "BAL" == $dataEdit->uom ? 'selected' : '' }}>BAL</option>
                                            <option value="BDT" {{ "BDT" == $dataEdit->uom ? 'selected' : '' }}>BDT</option>
                                            <option value="BKS" {{ "BKS" == $dataEdit->uom ? 'selected' : '' }}>BKS</option>
                                            <option value="BLT" {{ "BLT" == $dataEdit->uom ? 'selected' : '' }}>BLT</option>
                                        </select>
                                        @error('uom')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea name="description" id="" cols="30" rows="3" class="form-control">{{ $dataEdit->description }}</textarea>
                                        @error('description')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">IMEI 1</label>
                                        <input type="text" class="form-control" name="imei_1" value="{{ $dataEdit->imei_1 }}" placeholder="IMEI 1">
                                        @error('imei_1')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">IMEI 2</label>
                                        <input type="text" class="form-control" name="imei_2" value="{{ $dataEdit->imei_2 }}" placeholder="IMEI 2">
                                        @error('imei_2')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Suplier Price 1</label>
                                        <input type="text" class="form-control" name="supplier_price_1" value="{{ $dataEdit->supplier_price_1 }}" placeholder="Suplier Price 1">
                                        @error('supplier_price_1')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Suplier Price 2</label>
                                        <input type="text" class="form-control" name="supplier_price_2" value="{{ $dataEdit->supplier_price_2 }}" placeholder="Suplier Price 2">
                                        @error('supplier_price_2')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">COGS Price</label>
                                        <input type="text" class="form-control" name="cogs_price" value="{{ $dataEdit->cogs_price }}" placeholder="COGS price">
                                        @error('cogs_price')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Selling Price</label>
                                        <input type="text" class="form-control" name="selling_price" onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)" value="{{ $dataEdit->selling_price }}" placeholder="Selling Price">
                                        @error('selling_price')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Up % Price</label>
                                        <input type="text" class="form-control" name="up_price_percent" value="{{ $dataEdit->up_price_percent }}" placeholder="0" readonly>
                                        @error('up_price_percent')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Customer Price</label>
                                        <input type="text" class="form-control" name="customer_price" id="to_slug" value="{{ $dataEdit->customer_price }}" placeholder="0" readonly>
                                        @error('customer_price')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('master/spareparts')}}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-bookmark"></i> &nbsp; Save Data</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    function convertToSlug(str) {
        elem = 'to_slug';
        Helper.toSlug(str, elem);
    }
</script>
@endsection
