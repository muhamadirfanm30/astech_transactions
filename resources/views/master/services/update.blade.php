<head>
    <title>Services Update | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Customer 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('master/services')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    <form action="{{ url('/master/services/edit/'.$seriveEdit->id) }}" method="post">
                        @csrf
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">ID Service</label>
                                        <input type="text" class="form-control" name="id_service" value="{{ $seriveEdit->id_service }}" placeholder="ID Service" readonly>
                                        @error('id_service')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                                            <option value="">Choose Status</option>
                                            <option value="By.Jasa" {{ "By.Jasa" == $seriveEdit->status ? 'selected' : '' }}>By Jasa</option>
                                            <option value="By.Cek" {{ "By.Cek" == $seriveEdit->status ? 'selected' : '' }}>By Cek</option>
                                        </select>
                                        @error('status')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Company</label>
                                        <select class="form-control" name="ms_companies_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Company</option>
                                            @foreach ($showCompany as $comp)
                                                <option value="{{ $comp->id }}"  {{ $comp->id == $seriveEdit->ms_companies_id ? 'selected' : '' }}>{{ $comp->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_companies_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Brand</label>
                                        <select class="form-control" name="ms_brands_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Brand</option>
                                            @foreach ($showBrand as $brand)
                                                <option value="{{ $brand->id }}" {{ $brand->id == $seriveEdit->ms_brands_id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_brands_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Service Code</label>
                                        <input type="text" class="form-control" name="service_code" value="{{ $seriveEdit->service_code }}" placeholder="Service Code">
                                        @error('service_code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea name="description" id="" cols="30" rows="3" class="form-control">{{ $seriveEdit->description }}</textarea>
                                        @error('description')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">SVC Type</label>
                                                <select class="form-control" name="svc_type" id="exampleFormControlSelect1">
                                                    <option value="">Choose Service Type</option>
                                                    <option value="CI" {{ "CI" == $seriveEdit->svc_type ? 'selected' : '' }}>CI</option>
                                                    <option value="IH" {{ "IH" == $seriveEdit->svc_type ? 'selected' : '' }}>IH</option>
                                                    <option value="PS" {{ "PS" == $seriveEdit->svc_type ? 'selected' : '' }}>PS</option>
                                                    <option value="RH" {{ "RH" == $seriveEdit->svc_type ? 'selected' : '' }}>RH</option>
                                                    <option value="OTH" {{ "OTH" == $seriveEdit->svc_type ? 'selected' : '' }}>OTH</option>
                                                </select>
                                                @error('svc_type')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Deffect</label>
                                                <select class="form-control" name="defect_type" id="exampleFormControlSelect1">
                                                    <option value="">Choose Deffect Type</option>
                                                    <option value="MJ" {{ "MJ" == $seriveEdit->defect_type ? 'selected' : '' }}>MJ</option>
                                                    <option value="MN" {{ "MN" == $seriveEdit->defect_type ? 'selected' : '' }}>MN</option>
                                                    <option value="SP" {{ "SP" == $seriveEdit->defect_type ? 'selected' : '' }}>SP</option>
                                                    <option value="AE" {{ "AE" == $seriveEdit->defect_type ? 'selected' : '' }}>AE</option>
                                                    <option value="FL" {{ "FL" == $seriveEdit->defect_type ? 'selected' : '' }}>FL</option>
                                                    <option value="OTH" {{ "OTH" == $seriveEdit->defect_type ? 'selected' : '' }}>OTH</option>
                                                </select>
                                                @error('defect_type')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">COGS Price</label>
                                                <input type="number" class="form-control" name="cogs_price" value="{{ $seriveEdit->cogs_price }}" placeholder="0">
                                                @error('cogs_price')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Selling Price</label>
                                                <input type="number" class="form-control" name="selling_price" value="{{ $seriveEdit->selling_price }}" placeholder="0">
                                                @error('selling_price')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('master/services')}}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-bookmark"></i> &nbsp; Save Data</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  
</script>
@endsection
