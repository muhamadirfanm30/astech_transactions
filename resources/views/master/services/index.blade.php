<head>
    <title>Services List | Cashier System</title>
</head>
<style>
    .upload-description-header{
        color:red;
        font-weight:800;
    }
    #upload{
        width: 500px;
        height: 250px;
        margin:auto;
        margin-bottom:25px;
        margin-top:25px;
        padding: 25px;
        border:2px dashed #028AF4;
    }

    .upload-description-content{
        list-style-type: decimal;
    }

    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height:450px;
        overflow-y: auto;
    }

</style>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('success') }}</strong>
            </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session()->get('error') }}</strong>
                </div>
            @endif
            <div class="card card-primary card-outline card-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs justify-content-end" id="custom-tabs-two-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill"
                                href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home"
                                aria-selected="true">Data Table</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-two-upload-tab" data-toggle="pill"
                                href="#custom-tabs-two-upload" role="tab" aria-controls="custom-tabs-two-upload"
                                aria-selected="false">Import Data</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-two-tabContent">
                        <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 class="card-title">
                                                Services
                                            </h3>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <a href="{{url('master/services/create')}}" class="btn btn-primary btn-sm">
                                                <i class="fa fa-plus"></i>&nbsp;
                                                Add New Service
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table id="services_table" class="display table table-hover table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Service Code</th>
                                                <th>Information </th>
                                                <th>SVC Type</th>
                                                <th>Defect</th>
                                                <th>Starting Price</th>
                                                <th>Salling Price</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-two-upload" role="tabpanel"
                        aria-labelledby="custom-tabs-two-upload-tab">
                            <div class="card mb-3">
                                <div class="portlet-body">
                                    <div class="card-header"><h5>Import Services</h5></div>
                                    <div id="upload">
                                        <h4 class="upload-description-header">Note to upload Excel: </h4>
                                        <ul class="upload-description-content">
                                            <li>Download file template <a href="{{ asset('/storage/import_services_template.xlsx') }}">here</a></li>
                                            <li>Accepted file type only in .xlsx extension</li>
                                            <li>File must not exceed 10 MB</li>
                                            <li>Please <b>DO NOT</b> change the format of excel template</li>
                                        </ul>
                                        {{-- <form method="post" id="file-upload" enctype="multipart/form-data"> --}}

                                            <input type="file" id="file" accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/>
                                            <input type="hidden" id="type_import" value="services">
                                            <div class="padding-bottom-30">
                                                <div class="float-right">
                                                    <div class="loader"></div>
                                                    <button class="btn btn-primary" id="import"> Upload </button>
                                                </div>
                                            </div>
                                        {{-- </form> --}}
                                    </div>
                                </div>
                                <br>
                                <div class="d-block text-center card-footer">
                                    <div class="progress" style="display:none">
                                        <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only">0%</span>
                                        </div>
                                    </div>
                                    <div class="note">
                                        <div style="max-height:175px;overflow:auto;">
                                            <span></span><br/>
                                            <p style="color:red;" id="note-content"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Import Log
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <table style="width: 100%;" id="table-import-log" class="table table-hover table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Name User</th>
                                                <th>Amount Imported</th>
                                                <th>Data Imported</th>
                                                <th>Imported At</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- BEGIN MODAL PREVIEW UPLOAD --}}
    <div class="modal fade" data-backdrop="false" id="modal_preview" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg mw-100 w-75" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Upload Excel Import Services
                    </h4>
                </div>
                <div class="modal-body">
                    {{-- START TABLE PREVIEW --}}
                    <div class="col-lg-12 preview hidden">
                        <h2>Data Preview Services</h2>
                        <div>
                            <table id="preview" class="display" >
                                <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Company Name</th>
                                    <th>Brand Name</th>
                                    <th>Service Code</th>
                                    <th>Description</th>
                                    <th>Svc Type</th>
                                    <th>Defect Type</th>
                                    <th>Cogs Price</th>
                                    <th>Selling Price</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    {{-- END TABLE PREVIEW --}}
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn" id="close_import_modal">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-info" id="save" onClick="saveDataServices();">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL CONFIRM DELETE -->

    <!-- Modal DATA IMPORTED -->
    <div class="modal fade data_imported_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Data Imported By <span class="badge badge-info" id="name_user"></span></h5>
            </div>
            <div class="modal-body">
                <table id="table-data-imported-by" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Status</th>
                            <th>Service Code</th>
                            <th>Description</th>
                            <th>Cogs Price</th>
                            <th>Selling Price</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close_data_imported_modal"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</section>
@endsection
@section('script')
@include('layouts.template._importScript')
<script>

    $(document).on('click', '#close_import_modal', function() {
        $('#modal_preview').modal('hide');
    });
    $(document).ready(function() {
        var tableOrder = $('#services_table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            ordering: 'true',
            order: [1, 'desc'],
            responsive: false,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                // {
                //     text: '<i class="fa fa-refresh"></i>',
                //     action: function(e, dt, node, config) {
                //         dt.ajax.reload();
                //     }
                // }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/master-datatables/services/search/datatables'),
                type: 'get',
            },
            columns: [
                {
                    data: "service_code",
                    name: "service_code",
                    render: function(data, type, full) {
                        return full.service_code;
                    }
                },
                {
                    data: "description",
                    name: "description",
                    render: function(data, type, full) {
                        return full.description;
                    }
                },
                {
                    data: "svc_type",
                    name: "svc_type",
                    render: function(data, type, full) {
                        return full.svc_type;
                    }
                },
                {
                    data: "defect_type",
                    name: "defect_type",
                    render: function(data, type, full) {
                        return full.defect_type;
                    }
                },
                {
                    data: "cogs_price",
                    name: "cogs_price",
                    render: function(data, type, full) {
                        return full.cogs_price;
                    }
                },
                {
                    data: "selling_price",
                    name: "selling_price",
                    render: function(data, type, full) {
                        return full.selling_price;
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, full) {
                        return full.status;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return "<a href='services/update/" + full.id + "' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a> <button  class='btn btn-danger btn-sm' onclick='deleteData" + full.id + "'><i class='fa fa-trash'></i></button>";
                    }
                }
            ]
        });
    });

    // $("#form-search").sumbit(function(e) {
    //     input = Helper.serializeForm($(this));
    //     playload = '?';
    //     _.each(input, function(val, key) {
    //         playload += key + '=' + val + '&'
    //     });
    //     playload = playload.substring(0, playload.length - 1);
    //     console.log('asd', playload)

    //     url = Helper.apiUrl('/master-datatables/services/search/datatables' + playload);
    //     tableOrder.ajax.url(url).reload();
    //     e.preventDefault();
    // })

    function deleteData(id){
        swal({
            title: "Alert",
            text: "Are you sure you want to delete the Customer data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                document.getElementById('deleteServ-'+id).submit();
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    }

    //Import logs
    globalCRUD.datatables({
        url: '/master-datatables/services/import_log_datatables',
        selector: '#table-import-log',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "user.name",
            name: "user.name",
            render: function(row,data,full) {
                return '<h5><span class="badge badge-info">'+row+'</span></h5>';
            }
        },
        {
            data: "amount_imported",
            name: "amount_imported",
        },
        {
            data: "id",
            name: "id",
            render: function(row,data,full) {
                return '<button type="button" class="btn btn-warning open_data_imported_modal" data-users-id="'+full.users_id+'" data-name_user="'+full.user.name+'"><i class="fa fa-database disabled"></i></button>';
            }
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm A");
            }
        }
        ]
    });

    $(document).on('click', '.open_data_imported_modal', function() {
        $('.data_imported_modal').modal('show');
        //Imported By Datatables
        var users_id = $(this).attr('data-users-id');
        $('#name_user').html($(this).attr('data-name_user'));
        globalCRUD.datatables({
            url: '/master-datatables/services/imported_by_datatables/'+users_id+'',
            selector: '#table-data-imported-by',
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            {
                data: "status",
                name: "status",
            },
            {
                data: "service_code",
                name: "service_code",
            },
            {
                data: "description",
                name: "description",
            },
            {
                data: "cogs_price",
                name: "cogs_price",
            },
            {
                data: "selling_price",
                name: "selling_price",
            }
            ]
        });

    });
    $(document).on('click', '#close_data_imported_modal', function() {
        $('.data_imported_modal').modal('hide');
    });
</script>
@endsection
