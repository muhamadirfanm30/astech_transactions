<head>
    <title>Model Parts Update | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Update Model Part
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('master/model-part')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <form action="{{ url('master/model-part/edit/'.$dataEdit->id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Companies Name</label>
                                            <select class="form-control" name="ms_companies_id" id="exampleFormControlSelect1">
                                                @foreach ($getComp as $comp)
                                                    <option value="{{ $comp->id }}" {{ $dataEdit->ms_companies_id == $comp->id ? 'selected' : '' }}>{{ $comp->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Model Code</label>
                                            <input type="text" name="model_code" class="form-control" value="{{ $dataEdit->model_code }}" placeholder="Enter Model Code">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">brand Name</label>
                                            <select class="form-control" name="ms_brands_id" id="exampleFormControlSelect1">
                                                @foreach ($getBrand as $brand)
                                                    <option value="{{ $brand->id }}" {{ $dataEdit->ms_brands_id == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Category</label>
                                            <select class="form-control" name="ms_categories_id" id="exampleFormControlSelect1">
                                                @foreach ($getCategory as $cat)
                                                    <option value="{{ $cat->id }}" {{ $dataEdit->ms_categories_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Model Name</label>
                                            <input type="text" name="model_name" class="form-control" value="{{ $dataEdit->model_name }}" placeholder="Enter Model Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Service Code</label>
                                            <input type="text" name="service_code" class="form-control" value="{{ $dataEdit->service_code }}" placeholder="Enter Service Code">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="desc" id="" cols="30" rows="3" class="form-control">{{ $dataEdit->desc }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save Model Part</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>

</script>
@endsection
