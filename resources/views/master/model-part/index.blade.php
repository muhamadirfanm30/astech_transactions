 <head>
     <title>Model Parts List | Cashier System</title>
 </head>
 <style>
    .upload-description-header{
        color:red;
        font-weight:800;
    }
    #upload{
        width: 500px;
        height: 250px;
        margin:auto;
        margin-bottom:25px;
        margin-top:25px;
        padding: 25px;
        border:2px dashed #028AF4;
    }

    .upload-description-content{
        list-style-type: decimal;
    }

    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height:450px;
        overflow-y: auto;
    }

</style>
 @extends('layouts.master')
 @section('content')
     <section class="content">
         <div class="row">
             <div class="col-md-12">
                 @if (session()->has('success'))
                     <div class="alert alert-success alert-block">
                         <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>{{ session()->get('success') }}</strong>
                     </div>
                 @endif
                 @if (session()->has('error'))
                     <div class="alert alert-danger alert-block">
                         <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>{{ session()->get('error') }}</strong>
                     </div>
                 @endif
                 <div class="card card-primary card-outline card-tabs">
                     <div class="card-header p-0 pt-1 border-bottom-0">
                         <ul class="nav nav-tabs justify-content-end" id="custom-tabs-two-tab" role="tablist">
                             <li class="nav-item">
                                 <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill"
                                     href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home"
                                     aria-selected="true">Data Table</a>
                             </li>
                             <li class="nav-item">
                                 <a class="nav-link" id="custom-tabs-two-upload-tab" data-toggle="pill"
                                     href="#custom-tabs-two-upload" role="tab" aria-controls="custom-tabs-two-upload"
                                     aria-selected="false">Import Data</a>
                             </li>
                         </ul>
                     </div>
                     <div class="card-body">
                         <div class="tab-content" id="custom-tabs-two-tabContent">
                             <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel"
                                 aria-labelledby="custom-tabs-two-home-tab">
                                 <div class="card">
                                     <div class="card-header">
                                         <div class="row">
                                             <div class="col-md-6">
                                                 <h3 class="card-title">
                                                     List Model Part
                                                 </h3>
                                             </div>
                                             <div class="col-md-6 text-right">
                                                 <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                     data-target="#exampleModal">
                                                     <i class="fa fa-plus"></i>&nbsp;
                                                     Add Data
                                                 </button>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="card-body">
                                         <table id="model-part" class="display" style="width:100%">
                                             <thead>
                                                 <tr>
                                                     <th>Companies Name </th>
                                                     <th>code</th>
                                                     <th>Model Name</th>
                                                     <th>Service Code</th>
                                                     <th>Action</th>
                                                 </tr>
                                             </thead>
                                             @foreach ($showDataModelPart as $part)
                                                 <form action="{{ url('master/brands/destroy/' . $part->id) }}"
                                                     method="POST" id="deleteBrand-{{ $part->id }}">
                                                     @csrf
                                                 </form>
                                             @endforeach
                                         </table>
                                     </div>
                                 </div>
                                 <div class="card-footer"></div>

                             </div>
                             <div class="tab-pane fade" id="custom-tabs-two-upload" role="tabpanel"
                                aria-labelledby="custom-tabs-two-upload-tab">
                                <div class="card mb-3">
                                    <div class="portlet-body">
                                        <div class="card-header"><h5>Import Services</h5></div>
                                        <div id="upload">
                                            <h4 class="upload-description-header">Note to upload Excel: </h4>
                                            <ul class="upload-description-content">
                                                <li>Download file template <a href="{{ asset('/storage/import_model_parts_template.xlsx') }}">here</a></li>
                                                <li>Accepted file type only in .xlsx extension</li>
                                                <li>File must not exceed 10 MB</li>
                                                <li>Please <b>DO NOT</b> change the format of excel template</li>
                                            </ul>
                                            {{-- <form method="post" id="file-upload" enctype="multipart/form-data"> --}}

                                                <input type="file" id="file" accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/>
                                                <input type="hidden" id="type_import" value="model_parts">
                                                <div class="padding-bottom-30">
                                                    <div class="float-right">
                                                        <div class="loader"></div>
                                                        <button class="btn btn-primary" id="import"> Upload </button>
                                                    </div>
                                                </div>
                                            {{-- </form> --}}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="d-block text-center card-footer">
                                        <div class="progress" style="display:none">
                                            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                <span class="sr-only">0%</span>
                                            </div>
                                        </div>
                                        <div class="note">
                                            <div style="max-height:175px;overflow:auto;">
                                                <span></span><br/>
                                                <p style="color:red;" id="note-content"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            Import Log
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        <table style="width: 100%;" id="table-import-log" class="table table-hover table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name User</th>
                                                    <th>Amount Imported</th>
                                                    <th>Data Imported</th>
                                                    <th>Imported At</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>

                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>

         {{-- modal create bank --}}
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog modal-lg" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Add Model Part</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <form action="{{ url('master/model-part/store') }}" method="post">
                         @csrf
                         <div class="modal-body">
                             <div class="row">
                                 <div class="col-md-12">
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for="exampleFormControlSelect1">Companies Name</label>
                                                 <select class="form-control" name="ms_companies_id"
                                                     id="exampleFormControlSelect1">
                                                     @foreach ($getComp as $comp)
                                                         <option value="{{ $comp->id }}">{{ $comp->name }}
                                                         </option>
                                                     @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                 <label for="">Model Code</label>
                                                 <input type="text" name="model_code" class="form-control"
                                                     placeholder="Enter Model Code">
                                             </div>
                                         </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for="exampleFormControlSelect1">brand Name</label>
                                                 <select class="form-control" name="ms_brands_id"
                                                     id="exampleFormControlSelect1">
                                                     @foreach ($getBrand as $brand)
                                                         <option value="{{ $brand->id }}">{{ $brand->name }}
                                                         </option>
                                                     @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for="exampleFormControlSelect1">Category</label>
                                                 <select class="form-control" name="ms_categories_id" id="exampleFormControlSelect1">
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for="">Model Name</label>
                                                 <input type="text" name="model_name" class="form-control"
                                                     placeholder="Enter Model Name">
                                             </div>
                                         </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for="">Service Code</label>
                                                 <input type="text" name="service_code" class="form-control"
                                                     placeholder="Enter Service Code">
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <div class="form-group">
                                                 <label for="">Description</label>
                                                 <textarea name="desc" id="" cols="30" rows="3"
                                                     class="form-control"></textarea>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="modal-footer">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                             <button type="submit" class="btn btn-primary">Save changes</button>
                         </div>
                     </form>
                 </div>
             </div>
        </div>
        {{-- BEGIN MODAL PREVIEW UPLOAD --}}
        <div class="modal fade" data-backdrop="false" id="modal_preview" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg mw-100 w-75" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Upload Excel Import Model Parts
                        </h4>
                    </div>
                    <div class="modal-body">
                        {{-- START TABLE PREVIEW --}}
                        <div class="col-lg-12 preview hidden">
                            <h2>Data Preview Model Parts</h2>
                            <div>
                                <table id="preview" class="display">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Model Code</th>
                                            <th>Brand Name</th>
                                            <th>Categories Name</th>
                                            <th>Model Name</th>
                                            <th>Service Code</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        {{-- END TABLE PREVIEW --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger" id="close_import_modal">Cancel</button>
                        <button type="button" data-dismiss="modal" class="btn btn-info" id="save"
                            onClick="saveDataModelParts();">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL CONFIRM DELETE -->

        <!-- Modal DATA IMPORTED -->
        <div class="modal fade data_imported_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Imported By <span class="badge badge-info" id="name_user"></span></h5>
                </div>
                <div class="modal-body">
                    <table id="table-data-imported-by" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Model Code</th>
                                <th>Model Name</th>
                                <th>Service Code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_data_imported_modal"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
     </section>
@endsection
@section('script')
@include('layouts.template._importScript')
     <script>
        $(document).on('click', '#close_import_modal', function() {
            $('#modal_preview').modal('hide');
        });
         $(document).ready(function() {
             var tableOrder = $('#model-part').DataTable({
                 processing: true,
                 serverSide: true,
                 destroy: true,
                 scrollX: true,
                 ordering: 'true',
                 // order: [2, 'desc'],
                 responsive: false,
                 language: {
                     buttons: {
                         colvis: '<i class="fa fa-list-ul"></i>'
                     },
                     search: '',
                     searchPlaceholder: "Search...",
                     processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                 },
                 oLanguage: {
                     sLengthMenu: "_MENU_",
                 },
                 buttons: [{
                         extend: 'colvis'
                     },
                     // {
                     //     text: '<i class="fa fa-refresh"></i>',
                     //     action: function(e, dt, node, config) {
                     //         dt.ajax.reload();
                     //     }
                     // }
                 ],
                 dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                 ajax: {
                     url: Helper.apiUrl('/master-datatables/model-part/search/datatables'),

                     type: 'get',
                 },
                 columns: [{
                         data: "id",
                         name: "id",
                         render: function(data, type, full) {
                             return full.getcompany.name;
                         }
                     },
                     {
                         data: "model_code",
                         name: "model_code",
                         render: function(data, type, full) {
                             return full.model_code;
                         }
                     },
                     {
                         data: "model_name",
                         name: "model_name",
                         render: function(data, type, full) {
                             return full.model_name;
                         }
                     },
                     {
                         data: "service_code",
                         name: "service_code",
                         render: function(data, type, full) {
                             return full.service_code;
                         }
                     },
                     {
                         orderable: false,
                         searchable: false,
                         render: function(data, type, full) {
                             return '<a href="model-part/update/' + full.id +
                                 '" class="btn btn-warning btn-sm" style="color: white"><i class="fa fa-edit"></i></a> <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData(' +
                                 full.id + ')"><i class="fa fa-trash"></i></button>';
                             // return '<a href="/master/brands/edit/' + full.id + '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a><button type="submit" class="btn btn-danger btn-sm" onclick="deleteData('+ full.id +')"><i class="fa fa-trash"></i></button>';
                         }
                     }
                 ]
             });
         });

         function deleteData(id) {


             swal({
                     title: "Alert",
                     text: "Are you sure you want to delete the Model Part data?",
                     icon: "warning",
                     buttons: true,
                     dangerMode: true,
                 })
                 .then((willDelete) => {
                     if (willDelete) {
                         document.getElementById('deleteBrand-' + id).submit();
                     } else {
                         swal("Your imaginary file is safe!");
                     }
                 });
         }

         //Import logs
    globalCRUD.datatables({
        url: '/master-datatables/model-part/import_log_datatables',
        selector: '#table-import-log',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "user.name",
            name: "user.name",
            render: function(row,data,full) {
                return '<h5><span class="badge badge-info">'+row+'</span></h5>';
            }
        },
        {
            data: "amount_imported",
            name: "amount_imported",
        },
        {
            data: "id",
            name: "id",
            render: function(row,data,full) {
                return '<button type="button" class="btn btn-warning open_data_imported_modal" data-users-id="'+full.users_id+'" data-name_user="'+full.user.name+'"><i class="fa fa-database disabled"></i></button>';
            }
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm A");
            }
        }
        ]
    });

    $(document).on('click', '.open_data_imported_modal', function() {
        $('.data_imported_modal').modal('show');
        //Imported By Datatables
        var users_id = $(this).attr('data-users-id');
        $('#name_user').html($(this).attr('data-name_user'));
        globalCRUD.datatables({
            url: '/master-datatables/model-part/imported_by_datatables/'+users_id+'',
            selector: '#table-data-imported-by',
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            {
                data: "model_code",
                name: "model_code",
            },
            {
                data: "model_name",
                name: "model_name",
            },
            {
                data: "service_code",
                name: "service_code",
            },
            {
                data: "desc",
                name: "desc",
            },
            ]
        });

    });
    $(document).on('click', '#close_data_imported_modal', function() {
        $('.data_imported_modal').modal('hide');
    });
     </script>
@endsection
