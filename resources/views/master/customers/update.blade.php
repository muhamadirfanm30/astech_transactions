@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Update Customer 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('master/customers')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    <form action="{{ url('master/customers/edit/'.$custEdit->id) }}" method="post">
                        @csrf
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label for="">Status</label>
                                                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                                                            {{-- <option value="">Choose Main Office</option> --}}
                                                            <option value="1">Customer</option>
                                                        </select>
                                                        @error('status')
                                                            <span style="color:red"><small>{{ $message }}</small></span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="color: white">_</label>
                                                        <input type="text" name="code" value="{{ $custEdit->code }}" placeholder="Code" class="form-control" readonly>
                                                        @error('code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Contact Person</label>
                                        <input type="text" class="form-control" name="contact_person" value="{{ $custEdit->contact_person }}" placeholder="contact person">
                                        @error('contact_person')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Customer Name</label>
                                        <input type="text" class="form-control" name="name" value="{{ $custEdit->name }}" placeholder="Customer Name">
                                        @error('name')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Phone Number C.P</label>
                                        <input type="text" class="form-control" name="phone" value="{{ $custEdit->phone }}" placeholder="Phone Number">
                                        @error('phone')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Alias</label>
                                        <input type="text" class="form-control" name="alias" value="{{ $custEdit->alias }}" placeholder="Customer Alias">
                                        @error('alias')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Telephone C.P</label>
                                        <input type="text" class="form-control" name="telepon_cp" value="{{ $custEdit->telepon_cp }}" placeholder="Telephone C.P">
                                        @error('telepon_cp')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Alamat 1</label>
                                        <textarea name="address_1" id="" cols="30" rows="3" class="form-control">{{ $custEdit->address_1 }}</textarea>
                                        @error('address_1')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Alamat 2</label>
                                        <textarea name="address_2" id="" cols="30" rows="3" class="form-control">{{ $custEdit->address_2 }}</textarea>
                                       
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">City</label>
                                        <input type="text" class="form-control" name="city" value="{{ $custEdit->city }}" placeholder="City">
                                        @error('city')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Credit Limit</label>
                                        <input type="text" class="form-control" name="limit_piutang" value="{{ $custEdit->limit_piutang }}" placeholder="Credit Limit">
                                        @error('limit_piutang')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Rekening Number</label>
                                        <input type="text" class="form-control" name="rekening_no" value="{{ $custEdit->rekening_no }}" placeholder="Rekening Number">
                                        @error('rekening_no')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Number KTP</label>
                                        <input type="text" class="form-control" name="no_ktp" value="{{ $custEdit->no_ktp }}" placeholder="Number KTP">
                                        @error('no_ktp')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">FAX</label>
                                        <input type="text" class="form-control" name="fax" value="{{ $custEdit->fax }}" placeholder="Customer FAX">
                                        @error('fax')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Rekening Name</label>
                                        <input type="text" class="form-control" name="rekening_name" value="{{ $custEdit->rekening_name }}" placeholder="Rekening Name">
                                        @error('rekening_name')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{ $custEdit->email }}" placeholder="Email">
                                        @error('email')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Npwp</label>
                                        <input type="text" class="form-control" name="npwp" value="{{ $custEdit->npwp }}" placeholder="Customer Npwp">
                                        @error('npwp')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Bank Name</label>
                                        <select class="form-control" name="ms_banks_id" id="exampleFormControlSelect1">
                                            <option value="">Choose Bank Name</option>
                                            @foreach ($getPayment as $bank)
                                                <option value="{{ $bank->id }}" {{ $bank->id == $custEdit->ms_banks_id ? 'selected' : '' }}>{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('ms_banks_id')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('master/customers')}}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-bookmark"></i> &nbsp; Save Data</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  
</script>
@endsection
