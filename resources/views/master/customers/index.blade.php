<head>
    <title>Customers List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Customer 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('master/customers/create')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Customer
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    <table id="customers_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name </th>
                                <th>Phone Number</th>
                                <th>Aliases Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @foreach ($getCustomer as $cust)
                            <tbody>
                                <td>{{$cust->code}}</td>
                                <td>{{$cust->name}}</td>
                                <td>{{$cust->phone}}</td>
                                <td>{{$cust->alias}}</td>
                                <td>
                                    <form action="{{ url('master/customers/destroy/'.$cust->id) }}" method="POST" id="deleteCust-{{$cust->id}}">
                                        @csrf
                                    </form>
                                    <a href="{{ url('/master/customers/update/'.$cust->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $cust->id }})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
        var table = $('#customers_table').DataTable({});
    } );

    function deleteData(id){
        swal({
            title: "Alert",
            text: "Are you sure you want to delete the Customer data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                document.getElementById('deleteCust-'+id).submit();
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    }
</script>
@endsection
