<head>
    <title>Brands List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Brand
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Brand
                            </button>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="brand_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Company Name </th>
                                <th>Brand</th>
                                <th><center>Logo</center></th>
                                <th>Create Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @foreach ($brandShow as $merk)
                            {{-- <tbody>
                                <td>{{$merk->code}}</td>
                                <td>{{$merk->getcompany->name}}</td>
                                <td>{{$merk->name}}</td>
                                <td><center><img src="{{url('storage/brand-images/'.$merk->logo)}}" alt="" width="70px"></center></td>
                                <td>{{date('d M Y', strtotime($merk->created_at));}}</td>
                                <td>

                                    <a href="{{ url('/master/brands/edit/'.$merk->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $merk->id }})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tbody> --}}
                            <form action="{{ url('master/model-part/destroy/'.$merk->id) }}" method="POST" id="deleteBrand-{{$merk->id}}">
                                @csrf
                            </form>
                        @endforeach

                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>

    {{-- model create brand  --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('/master/brands/store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Company Name *</label>
                            <select class="form-control" name="ms_companies_id" id="exampleFormControlSelect1">
                                <option value="">Company Name</option>
                                @foreach ($getCompany as $item)
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('ms_companies_id')
                                <span style="color:red"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="row" style="margin-bottom: 15px">
                                    <div class="col-md-6">
                                        <label for="">Brand Code *</label>
                                        <input type="text" name="code" class="form-control" placeholder="Brand Code" value="{{Request::old('code')}}" readonly>
                                        @error('code')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Brand Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="Brand Name" value="{{Request::old('name')}}">
                                        @error('name')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <label>Upload Image</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Browse… <input type="file" name="logo" id="imgInp">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" readonly>
                                </div><hr>
                                <center>
                                    <img id='img-upload'/>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
            }
            .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
            }

            #img-upload{
                width: 50%;
            }
    </style>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>

    $(document).ready(function() {
        var tableOrder = $('#brand_table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            ordering: 'true',
            // order: [2, 'desc'],
            responsive: false,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/api/master-datatables/brand/search/datatables'),

                type: 'get',
            },
            columns: [
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        return full.code;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return full.getcompany.name;
                    }
                },
                {
                    data: "name",
                    name: "name",
                    render: function(data, type, full) {
                        return full.name;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return '<img width="70px" src="/v2/storage/brand-images/'+full.logo+'"/>';
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full) {
                        return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return '<a href="brands/edit/' + full.id + '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData('+ full.id +')"><i class="fa fa-trash"></i></button>';
                    }
                }
            ]
        });
    });

    // img preview js
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});

    function deleteData(id){


        swal({
            title: "Alert",
            text: "Are you sure you want to delete the Brand data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                document.getElementById('deleteBrand-'+id).submit();
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    }
</script>
@endsection
