<head>
    <title>Brands Update | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Brand
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            {{-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Brand
                            </button> --}}
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <form action="{{ url('/master/brands/update/'.$dataEdit->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Company Name *</label>
                                <select class="form-control" name="ms_companies_id" id="exampleFormControlSelect1">
                                    <option value="">Company Name</option>
                                    @foreach ($getCompany as $item)
                                        <option value="{{$item->id}}"  {{ $item->id == $dataEdit->ms_companies_id ? 'selected' : '' }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @error('ms_companies_id')
                                    <span style="color:red"><small>{{ $message }}</small></span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="row" style="margin-bottom: 15px">
                                        <div class="col-md-6">
                                            <label for="">Brand Code *</label>
                                            <input type="text" name="code" class="form-control" placeholder="Brand Code" value="{{$dataEdit->code}}" readonly>
                                            @error('code')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Brand Name *</label>
                                            <input type="text" name="name" class="form-control" placeholder="Brand Name" value="{{$dataEdit->name}}">
                                            @error('name')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <label>Upload Image</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Browse… <input type="file" name="logo" id="imgInp">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div><hr>
                                    <center>
                                        @if (!empty($dataEdit->logo))
                                            <img src="{{ url('storage/brand-images/'.$dataEdit->logo) }}" id="img-upload" alt="brand-img">
                                        @else
                                            <img id='img-upload'/>
                                        @endif
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>


    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
            }
            .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
            }

            #img-upload{
                width: 30%;
            }
    </style>
</section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    // img preview js
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>
@endsection
