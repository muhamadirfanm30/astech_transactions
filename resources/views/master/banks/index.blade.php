<head>
    <title>Bank List | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Banks
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Data
                            </button>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="banks_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code </th>
                                <th>Metod</th>
                                {{-- <th>Create By</th> --}}
                                <th>Create Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        {{-- @foreach ($showBank as $bank)
                            <tbody>
                                <th>{{$bank->code}}</th>
                                <th>{{$bank->name}}</th>
                                <th>{{date('d M Y', strtotime($bank->created_at));}}</th>
                                <th>
                                    <button type="button" data-id ="{{$bank->id}}" class="btn btn-warning btn-sm" id="edit_bank" data-toggle="tooltip" data-html="true" style="color:white"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-danger btn-sm" data-id ="{{$bank->id}}" id="btn_delete"><i class="fa fa-trash"></i></button>
                                </th>
                            </tbody>
                        @endforeach --}}
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>

    {{-- modal create bank --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Bank</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('master/banks/store') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <label for="">Bank Metod</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Bank Name">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal update --}}
    <div class="modal fade bd-example-modal-lg" id="update_bank" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Bank</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="get_id">
                    <div id="content_bank">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="">Code</label>
                                <input type="text" name="code" id="get_code" class="form-control" placeholder="Code" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" name="name" id="get_name" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary waves-effect" id="saved" type="submit">
                                Update
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')

<script>


    $(document).ready(function() {
        var tableOrder = $('#banks_table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            ordering: 'true',
            // order: [2, 'desc'],
            responsive: false,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/api/master-datatables/bank/search/datatables'),
                
                type: 'get',
            },
            columns: [
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        return full.code;
                    }
                },
                {
                    data: "name",
                    name: "name",
                    render: function(data, type, full) {
                        return full.name;
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full) {
                        return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return '<button type="button" data-id ="'+ full.id +'" class="btn btn-warning btn-sm" id="edit_bank" data-toggle="tooltip" data-html="true" style="color:white"><i class="fa fa-edit"></i></button><button class="btn btn-danger btn-sm" data-id ="'+ full.id +'" id="btn_delete"><i class="fa fa-trash"></i></button>';
                    }
                }
            ]
        });
    }); 

    var $modal = $('#update_bank').modal({
        show: false
    });

    var ProductAdditionalObj = {
        // isi field input
        isiDataFormModal: function(id) {
            $.ajax({
                url: '/master/banks/edit/' + id,
                type: 'get',
                success: function(resp) {
                    console.log(resp.code)
                    $("#get_id").html('');
                    $("#get_code").html('');
                    $("#get_name").html('');
                    var id = resp.id;
                    var code = resp.code;
                    var name = resp.name;
                    $("#get_id").val(id);
                    $("#get_code").val(code);
                    $("#get_name").val(name);
                },
                error: function(xhr, status, error) {
                    console.log('error');
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            Helper.successNotif(resp.msg);
            $modal.modal('hide');
        },
    }

    $(document)
        .on('click', '#edit_bank', function() {
            id = $(this).attr('data-id');
            // $('#getIds').val(id)
            // $('.additional_parts').show();
            console.log(ProductAdditionalObj.isiDataFormModal(id));
            $modal.modal('show');
        })

    $(document).on('click', '#saved', function(){
        Helper.loadingStart();
        $.ajax({
            url: '/master/banks/update/' + id,
            type: 'post',
            data:{
                "_token": "{{ csrf_token() }}",
                'name': $("#get_name").val()
            },
            success: function(resp) {
                console.log(resp)

                // Helper.loadingStop();
                // swal("success", "Data Has Been Saved", "success");
                // window.location.href = '/master/banks';
            },
            error: function(resp, xhr, status, error) {
                Helper.errorNotif('Error : '+resp.responseJSON.msg);
                Helper.loadingStop();
            },
        })

    });

    $(document).on('click', '#btn_delete', function(){
        swal({
            title: "Are you sure?",
            text: "want to delete this data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = $(this).attr('data-id');
                Helper.loadingStart();
                $.ajax({
                    url: '/master/banks/delete/' + id,
                    type: 'post',
                    data:{
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(resp) {
                        Helper.loadingStop();
                        swal("success", "Data Has Been Saved", "success");
                        window.location.href = '/master/banks';
                    },
                    error: function(resp, xhr, status, error) {
                        Helper.errorNotif('Error : '+resp.responseJSON.msg);
                        Helper.loadingStop();
                    },
                })
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    });
</script>
@endsection
