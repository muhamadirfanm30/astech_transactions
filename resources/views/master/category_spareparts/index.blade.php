<head>
    <title>Category Spareparts List | Cashier System</title>
</head>
<style>
    .upload-description-header{
        color:red;
        font-weight:800;
    }
    #upload{
        width: 500px;
        height: 250px;
        margin:auto;
        margin-bottom:25px;
        margin-top:25px;
        padding: 25px;
        border:2px dashed #028AF4;
    }

    .upload-description-content{
        list-style-type: decimal;
    }

    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height:450px;
        overflow-y: auto;
    }

</style>
@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">
                        <ul class="nav nav-tabs justify-content-end" id="custom-tabs-two-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill"
                                    href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home"
                                    aria-selected="true">Data Table</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-upload-tab" data-toggle="pill"
                                    href="#custom-tabs-two-upload" role="tab" aria-controls="custom-tabs-two-upload"
                                    aria-selected="false">Import Data</a>
                            </li>

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-two-tabContent">
                            <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel"
                                aria-labelledby="custom-tabs-two-home-tab">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h3 class="card-title">
                                                    Categories Sparepart
                                                </h3>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#exampleModal">
                                                    <i class="fa fa-plus"></i>&nbsp;
                                                    Add Data
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-body">
                                        <table id="categori_table" class="display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Create Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            @foreach ($categori as $kategori)
                                                <tbody>
                                                    <th>{{ $kategori->name }}</th>
                                                    <th>{{ date('d M Y', strtotime($kategori->created_at)) }}</th>
                                                    <th>
                                                        <button type="button" data-id="{{ $kategori->id }}" class="btn btn-warning btn-sm"
                                                            id="edit_categori" data-toggle="tooltip" data-html="true" style="color:white"><i
                                                                class="fa fa-edit"></i></button>
                                                        <button class="btn btn-danger btn-sm" data-id="{{ $kategori->id }}" id="btn_delete"><i
                                                                class="fa fa-trash"></i></button>
                                                    </th>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer"></div>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-two-upload" role="tabpanel"
                                aria-labelledby="custom-tabs-two-upload-tab">
                                <div class="card mb-3">
                                    <div class="portlet-body">
                                        <div class="card-header"><h5>Import Categories Spareparts</h5></div>
                                        <div id="upload">
                                            <h4 class="upload-description-header">Note to upload Excel: </h4>
                                            <ul class="upload-description-content">
                                                <li>Download file template <a href="{{ asset('/storage/import_category_spareparts_template.xlsx') }}">here</a></li>
                                                <li>Accepted file type only in .xlsx extension</li>
                                                <li>File must not exceed 10 MB</li>
                                                <li>Please <b>DO NOT</b> change the format of excel template</li>
                                            </ul>
                                            {{-- <form method="post" id="file-upload" enctype="multipart/form-data"> --}}

                                                <input type="file" id="file" accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/>
                                                <input type="hidden" id="type_import" value="category_spareparts">
                                                <div class="padding-bottom-30">
                                                    <div class="float-right">
                                                        <div class="loader"></div>
                                                        <button class="btn btn-primary" id="import"> Upload </button>
                                                    </div>
                                                </div>
                                            {{-- </form> --}}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="d-block text-center card-footer">
                                        <div class="progress" style="display:none">
                                            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                <span class="sr-only">0%</span>
                                            </div>
                                        </div>
                                        <div class="note">
                                            <div style="max-height:175px;overflow:auto;">
                                                <span></span><br/>
                                                <p style="color:red;" id="note-content"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            Import Log
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        <table style="width: 100%;" id="table-import-log" class="table table-hover table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name User</th>
                                                    <th>Amount Imported</th>
                                                    <th>Data Imported</th>
                                                    <th>Imported At</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>



        {{-- modal create payment --}}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Categories Sparepart</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ url('master/category_spareparts/store') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control"
                                placeholder="Enter Categories Sparepart Name">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- modal update --}}
        <div class="modal fade bd-example-modal-lg" id="update_categori" data-backdrop="false" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Categories Sparepart</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="get_id">
                        <div id="content_category">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="name" id="get_name" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary waves-effect" id="saved" type="submit">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- BEGIN MODAL PREVIEW UPLOAD --}}
        <div class="modal fade" data-backdrop="false" id="modal_preview" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg mw-100 w-75" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Upload Excel Import Category Spareparts
                        </h4>
                    </div>
                    <div class="modal-body">
                        {{-- START TABLE PREVIEW --}}
                        <div class="col-lg-12 preview hidden">
                            <h2>Data Preview</h2>
                            <div>
                                <table id="preview" class="display">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        {{-- END TABLE PREVIEW --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary" id="close_import_modal">Cancel</button>
                        <button type="button" data-dismiss="modal" class="btn btn-info" id="save" onClick="saveDataCategorySpareparts();">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL CONFIRM DELETE -->


        <!-- Modal DATA IMPORTED -->
        <div class="modal fade data_imported_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Imported By <span class="badge badge-info" id="name_user"></span></h5>
                </div>
                <div class="modal-body">
                    <table id="table-data-imported-by" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_data_imported_modal"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    @include('layouts.template._importScript')
    <script>
        $(document).on('click', '#close_import_modal', function() {
            $('#modal_preview').modal('hide');
        });

        $(document).ready(function() {
            var table = $('#categori_table').DataTable({});
        });
        var $modal = $('#update_categori').modal({
            show: false
        });

        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    url: '/master/category_spareparts/edit/' + id,
                    type: 'get',
                    success: function(resp) {
                        console.log(resp.code)
                        $("#get_id").html('');
                        $("#get_code").html('');
                        $("#get_name").html('');
                        var id = resp.id;
                        var code = resp.code;
                        var name = resp.name;
                        $("#get_id").val(id);
                        $("#get_code").val(code);
                        $("#get_name").val(name);
                    },
                    error: function(xhr, status, error) {
                        console.log('error');
                    },
                })

            },
            // hadle ketika response sukses
            successHandle: function(resp) {
                Helper.successNotif(resp.msg);
                $modal.modal('hide');
            },
        }

        $(document)
            .on('click', '#edit_categori', function() {
                id = $(this).attr('data-id');
                // $('#getIds').val(id)
                // $('.additional_parts').show();
                console.log(ProductAdditionalObj.isiDataFormModal(id));
                $modal.modal('show');
            })

        $(document).on('click', '#saved', function() {
            Helper.loadingStart();
            $.ajax({
                url: '/master/category_spareparts/update/' + id,
                type: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'name': $("#get_name").val()
                },
                success: function(resp) {
                    Helper.loadingStop();
                    swal("success", "Data Has Been Saved", "success");
                    window.location.href = '/master/category_spareparts';
                },
                error: function(resp, xhr, status, error) {
                    Helper.errorNotif('Error : ' + resp.responseJSON.msg);
                    Helper.loadingStop();
                },
            })

        });

        $(document).on('click', '#btn_delete', function() {
            swal({
                    title: "Are you sure?",
                    text: "want to delete this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var id = $(this).attr('data-id');
                        Helper.loadingStart();
                        $.ajax({
                            url: '/master/category_spareparts/delete/' + id,
                            type: 'post',
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(resp) {
                                Helper.loadingStop();
                                swal("success", "Data Has Been Saved", "success");
                                window.location.href = '/master/category_spareparts';
                            },
                            error: function(resp, xhr, status, error) {
                                Helper.errorNotif('Error : ' + resp.responseJSON.msg);
                                Helper.loadingStop();
                            },
                        })
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        });

    //Import logs
    globalCRUD.datatables({
        url: '/master-datatables/category_spareparts/import_log_datatables',
        selector: '#table-import-log',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "user.name",
            name: "user.name",
            render: function(row,data,full) {
                return '<h5><span class="badge badge-info">'+row+'</span></h5>';
            }
        },
        {
            data: "amount_imported",
            name: "amount_imported",
        },
        {
            data: "id",
            name: "id",
            render: function(row,data,full) {
                return '<button type="button" class="btn btn-warning open_data_imported_modal" data-users-id="'+full.users_id+'" data-name_user="'+full.user.name+'"><i class="fa fa-database disabled"></i></button>';
            }
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm A");
            }
        }
        ]
    });

    $(document).on('click', '.open_data_imported_modal', function() {
        $('.data_imported_modal').modal('show');
        //Imported By Datatables
        var users_id = $(this).attr('data-users-id');
        $('#name_user').html($(this).attr('data-name_user'));
        globalCRUD.datatables({
            url: '/master-datatables/category_spareparts/imported_by_datatables/'+users_id+'',
            selector: '#table-data-imported-by',
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            {
                data: "name",
                name: "name",
            }
            ]
        });

    });
    $(document).on('click', '#close_data_imported_modal', function() {
        $('.data_imported_modal').modal('hide');
    });


    </script>
@endsection
