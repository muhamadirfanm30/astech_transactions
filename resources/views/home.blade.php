@extends('layouts.master')

@section('content')
<style>
    .count-title {
        font-size: 40px;
        font-weight: normal;
        margin-top: 10px;
        margin-bottom: 0;
        color: white;
    }
</style>
<section class="content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3 class="timer count-title count-number" data-to="100" data-speed="700">0</h3>
                            <p>Registered Users</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3 class="timer count-title count-number" data-to="200" data-speed="800">0</h3>
                            <p>Open Transaction</p>
                        </div>
                        <div class="icon">
                            <i class="far fa-chart-bar"></i>
                        </div>
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3 class="timer count-title count-number" data-to="150" data-speed="900">0</h3>
            
                            <p style="color: white">Dp Transaction</p>
                        </div>
                        <div class="icon">
                            <i class="far fa-envelope"></i>
                        </div>
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3 class="timer count-title count-number" data-to="10" data-speed="1000">0</h3>
                            <p>Inv.Transaction</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-signal"></i>
                        </div>
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>
              </div><br>

              <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                Income Daily : {{ date('d M Y') }}
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="income_table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>BRANCH</th>
                                <th>CASH</th>
                                <th>DEBIT[BCA/MDR]</th>
                                <th>CREDIT[BCA/MDR]</th>
                                <th>TRANSFER</th>
                                <th>ONLINE[OVO/DANA]</th>
                                <th>FLAG</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
            {{-- <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div> --}}
        </div>
    </div>
</section>
<div class="container">
    
</div>
<script>
    $(document).ready(function() {
        var tableOrder = $('#income_table').DataTable();
    });
</script>
@endsection
