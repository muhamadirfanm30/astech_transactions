@extends('layouts.master')
@section('content')
<input type="hidden" id="hidden-user-id" value="{{ Auth::user()->id }}">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    {{-- <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle"
                                            src="../../dist/img/user4-128x128.jpg"
                                            alt="User profile picture">
                                    </div> --}}
                                    <div class="profile-userpic text-center">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                <label for="imageUpload"></label>
                                            </div>
                                            <div class="avatar-preview">
                                                {{-- <div id="imagePreview">
                                                    <img src="{{ url('/storage/users-profile/'. Auth::user()->attachment) }}" alt="a">
                                                </div> --}}
                                                <div id="imagePreview" style="background-image:  url({{ url('/storage/users-profile/'. Auth::user()->attachment) }}) ;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="" alt="">
                                    <h3 class="profile-username text-center" style="margin-bottom: -4px">{{ !empty(Auth::user()->name) ? Auth::user()->name : '-' }}</h3>
                                    <small><center>{{ !empty(Auth::user()->email) ? Auth::user()->email : '-' }}</center></small>
                                    <p class="text-muted text-center"><strong>{{ !empty(Auth::user()->get_role) ? Auth::user()->get_role->name : '-' }}</strong></p>
                                    {{-- <ul class="list-group list-group-unbordered mb-3">
                                        <li class="list-group-item">
                                            <b>Followers</b> <a class="float-right">0</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Following</b> <a class="float-right">0</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Friends</b> <a class="float-right">0</a>
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="#" class="btn btn-primary btn-block btn-sm"><i class="fas fa-user-plus"></i>&nbsp;<b>Follow</b></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="#" class="btn btn-success btn-block btn-sm"><i class="far fa-envelope"></i>&nbsp;<b>Message</b></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Profile</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Change password</a></li>
                                </ul>
                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        {{-- <div class="active tab-pane" id="activity">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6 text-right">
                                                            <div class="form-group">
                                                                <label for="" style="margin-bottom:-2px">Cell Phone</label>
                                                                <p>1231231231</p>

                                                                <label for="" style="margin-bottom:-2px">Mail To</label>
                                                                <p>muhamadirfanm30@gmail.com</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" style="margin-bottom:-2px">FAX</label>
                                                            <p>+1112225555</p>

                                                            <label for="" style="margin-bottom:-2px">Address</label>
                                                            <p>+3rd Floor WISMA SSK BUILDING, Daan Mogot Rd No.Km. 11, RT.5/RW.4, Kedaung Kali Angke, Cengkareng, West Jakarta City, Jakarta 11710</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="active tab-pane" id="timeline">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12" style="margin-bottom: 13px">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="">Username</label>
                                                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="{{!empty(Auth::user()->username) ? Auth::user()->username : ''}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 13px">
                                                        <label for="asd">Full Name</label>
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="{{!empty(Auth::user()->name) ? Auth::user()->name : ''}}">
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 13px">
                                                        <label for="asd">Brand</label>
                                                        <select class="form-control" name="ms_brand_id" id="brand_id">
                                                            @if (!empty($brand))
                                                                @foreach ($brand as $brn)
                                                                    <option value="{{ $brn->id }}" {{ Auth::user()->ms_brand_id == $brn->id ? 'selected' : '' }}>{{$brn->name}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value="">Data Not Found</option>
                                                            @endif
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom: 13px">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label for="">Company</label>
                                                                <select class="form-control" name="ms_companies_id" id="company_id">
                                                                    @if (!empty($company))
                                                                        @foreach ($company as $com)
                                                                            <option value="{{ $com->id }}" {{ Auth::user()->ms_companies_id == $com->id ? 'selected' : '' }}>{{$com->name}}</option>
                                                                        @endforeach
                                                                    @else
                                                                        <option value="">Data Not Found</option>
                                                                    @endif
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="">Branch Company</label>
                                                                <select class="form-control" name="ms_branch_id" id="branch_id">
                                                                    @if (!empty($branchOffice))
                                                                        @foreach ($branchOffice as $bo)
                                                                            <option value="{{ $bo->id }}" {{ Auth::user()->ms_branch_id == $bo->id ? 'selected' : '' }}>{{$bo->name}}</option>
                                                                        @endforeach
                                                                    @else
                                                                        <option value="">Data Not Found</option>
                                                                    @endif
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="">N.I.P</label>
                                                                <select class="form-control" name="nip" id="nip">
                                                                    <option>Udin Sarudin</option>
                                                                    <option>Fahmi Nur Aulia</option>
                                                                    <option>Arshad</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12"  style="margin-bottom: 13px">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label for="">Email</label>
                                                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{!empty(Auth::user()->email) ? Auth::user()->email : ''}}">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label for="">Phone Number</label>
                                                                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" value="{{!empty(Auth::user()->phone_number) ? Auth::user()->phone_number : ''}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12"  style="margin-bottom: 13px">
                                                        <label for="">Address</label>
                                                        <textarea name="address" id="address" cols="30" rows="3" class="form-control">{{!empty(Auth::user()->address) ? Auth::user()->address : ''}}</textarea>
                                                    </div>
                                                    <div class="col-md-12 text-right">
                                                        <button class="btn btn-primary" id="save_user" data-id="{{ Auth::user()->id }}">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                            <form action="{{ route('forgot.password') }}" method="post">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12" style="margin-bottom: 13px">
                                                        <label for="">Old Password</label>
                                                        <input type="password" name="current_password" class="form-control" placeholder="Old Password">
                                                        @if ($errors->has('current_password'))
                                                            <small class="help-block">{{ $errors->first('current_password') }}</small>
                                                        @endif
                                                    </div>
    
                                                    <div class="col-md-12" style="margin-bottom: 13px">
                                                        <label for="">New Password</label>
                                                        <input type="password" name="new_password" class="form-control" placeholder="New Password">
                                                        @if ($errors->has('new_password'))
                                                            <small class="help-block">{{ $errors->first('new_password') }}</small>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom: 13px">
                                                        <label for="">Confirm Password</label>
                                                        <input type="password" name="new_confirm_password" class="form-control" placeholder="Confirm Password">
                                                        @if ($errors->has('new_confirm_password'))
                                                            <small class="help-block">{{ $errors->first('new_confirm_password') }}</small>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-12 text-right">
                                                        <button class="btn btn-primary" type="submit">Forgot Password</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
<style>
    .profile {
    margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
    background: #fff;
    }

    .profile-userpic img {
    float: none;
    margin: 0 auto;
    width: 50%;
    -webkit-border-radius: 50% !important;
    -moz-border-radius: 50% !important;
    border-radius: 50% !important;
    }

    .profile-usertitle {
    text-align: center;
    margin-top: 10px;
    }

    .profile-usertitle-name {
    color: #5a7391;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 7px;
    }

    .profile-usertitle-job {
    text-transform: uppercase;
    color: #555;
    font-size: 12px;
    font-weight: 600;
    margin-bottom: 15px;
    }

    .profile-userbuttons {
    margin: 20px 0px;
    }

    .profile-userbuttons .btn {
    text-transform: uppercase;
    font-size: 11px;
    font-weight: 600;
    padding: 6px 15px;
    margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
    margin-right: 0px;
    }

    .profile-usermenu {
    margin-top: 30px;
    }

    /* Profile Content */
    .profile-content {
    padding: 20px;
    background: #fff;
    min-height: 460px;
    }

    .avatar-upload {
    position: relative;
    max-width: 175px;
    margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input {
        display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    /* content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto; */

        font-family: "Font Awesome 5 Free";
        content: "\270F";
        display: inline-block;
        padding-right: 3px;
        transform: rotate(30deg);
        position: absolute;
        top: 8px;
        left: 7px;
        right: 0;
    }
    .avatar-upload .avatar-preview {
    width: 170px;
    height: 170px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
</style>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    function readURL(input) {
	    var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
	}
    
    $("#imageUpload").change(function() {
        input = this;

        if (input.files && input.files[0]) {
            Helper.loadingStart();
            user_id = $("#hidden-user-id").val();
            var formData = new FormData();
            formData.append("image", input.files[0]);
            formData.append("user_id", user_id);

            console.log(formData);
            Axios
                .post(Helper.apiUrl('/system-setting/my-profile/upload-image/' + user_id), formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    }
                })
                .then(function(response) {
                    Helper.successNotif(response.data.msg);
                    readURL(input);
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        }
    });

    $(document).on('click', '#save_user', function(){
        Helper.loadingStart();
        id = $(this).attr('data-id')
        $.ajax({
            url: Helper.url('/system-setting/my-profile/update/'+id),
            type: 'post',
            data:{
                "_token": "{{ csrf_token() }}",
                'name': $("#name").val(),
                'email': $("#email").val(),
                'pass': $("#pass").val(),
                'username': $("#username").val(),
                'company_id': $("#company_id").val(),
                'brand_id': $("#brand_id").val(),
                'branch_id': $("#branch_id").val(),
                'nip': $("#nip").val(),
                'email': $("#email").val(),
                'phone_number': $("#phone_number").val(),
                'address': $("#address").val(),
            },
            success: function(resp) {
                Helper.loadingStop();
                swal("success", "Data Has Been Saved", "success");
                window.location.href = Helper.url('/system-setting/my-profile');
            },
            error: function(resp, xhr, status, error) {
                Helper.errorNotif('Error : '+resp.responseJSON.msg);
                Helper.loadingStop();
            },
        })

    });
</script>
@endsection
