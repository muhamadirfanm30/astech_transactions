<head>
    <title>{{ $title }} | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-sm add-suppliers">ADD {{ $title }}</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="table-suppliers" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Phone</th>
                                <th>VAT</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="form-suppliers">
        <div class="modal fade" id="modal-suppliers" data-backdrop="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add {{ $title }}</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <label>Name</label>
                        <div class="form-group">
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        </div>
                        <label>Email</label>
                        <div class="form-group">
                            <input name="email" placeholder="email" id="email" type="email" class="form-control" required>
                        </div>
                        <label>Company</label>
                        <div class="form-group">
                            <input name="company" placeholder="company" id="company" type="text" class="form-control" required>
                        </div>
                        <label>Phone</label>
                        <div class="form-group">
                            <input name="phone" placeholder="phone" id="phone" type="text" class="form-control" required>
                        </div>
                        <label>VAT (Tax)</label>
                        <div class="form-group">
                            <input name="vat" placeholder="vat" id="vat" type="text" class="form-control" required>
                        </div>
                        <label>Address</label>
                        <div class="form-group">
                            <textarea name="address" placeholder="address" id="address" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal" id="close_modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

@endsection

@section('script')
<script>
    $(document).on('click', '#close_modal', function() {
        $('#modal-suppliers').modal('hide');
    });

    globalCRUD.datatables({
        url: '/pos/suppliers/datatables',
        selector: '#table-suppliers',
        columnsField: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            'name',
            'email',
            'company',
            'phone',
            'vat'
        ],
        modalSelector: "#modal-suppliers",
        modalButtonSelector: ".add-suppliers",
        modalFormSelector: "#form-suppliers",
        actionLink: {
            store: function() {
                return "/pos/suppliers/store";
            },
            update: function(row) {
                return "/pos/suppliers/update/" + row.id;
            },
            delete: function(row) {
                return "/pos/suppliers/" + row.id;
            },
        },
    });

</script>
@endsection
