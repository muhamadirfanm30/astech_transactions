
<head>
    <title>{{ $title }} Create | Cashier System</title>
    <style>
        .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
    </style>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('pos/categories')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                </div>
                <form id="form-data" style="display: contents;" autocomplete="off">
                    <div class="card-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Name*</label>
                                <input name="name" placeholder="Name Categories" type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Detail*</label>
                                <textarea name="detail" placeholder="Detail" class="form-control" required></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Image</label>
                                <div class="col-sm-12">
                                    <input name="image" placeholder="Image" type="file" class="form-control" id="imgInp">
                                </div>
                                <div class="col-sm-3">
                                    <a download data-fancybox href="#">
                                        <img id='img-upload'/>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="position-relative row form-check">
                            <div class="col-sm-10 offset-sm-2">
                                <button class="btn btn-success" type="submit" style="float:right"><i class="fa fa-sign-in" ></i>Submit</button>
                            </div>
                        </div><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
@include('pos.categories._js')
<script>

    $("#form-data").submit(function(e) {
        ClassApp.saveData($(this),e);
    });

</script>

@endsection
