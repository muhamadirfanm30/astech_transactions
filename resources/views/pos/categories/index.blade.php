<head>
    <title>{{ $title }} | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ url('pos/categories/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add {{ $title }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="table-categories" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Detail</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script>
        globalCRUD.datatables({
            url: '/pos/categories/datatables',
            selector: '#table-categories',
            columnsField: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex"
                },
                'code',
                'name',
                'detail',
                {
                    data: "image",
                    name: "image",
                    render: function(data, type, full) {
                        if(data != null) {
                            return '<a download data-fancybox href="'+Helper.url('/storage/pos_categories/'+data+'')+'">'+data+'</a>'
                        }
                        return '-';
                    }
                }],
            actionLink: {
                update: function(row) {
                    return "/pos/categories/edit/" + row.id;
                },
                delete: function(row) {
                    return "/pos/categories/" + row.id;
                },
            }
        })
    </script>
@endsection
