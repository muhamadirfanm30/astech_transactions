
<head>
    <title>{{ $title }} Update | Cashier System</title>
    <style>
        .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
    </style>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('pos/categories')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                </div>
                <form id="form-data" style="display: contents;" autocomplete="off">
                    <div class="card-body">
                        <input type="hidden" name="id" id="id" value="{{ $maindata->id }}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Name*</label>
                                <input name="name" placeholder="Name Categories" type="text" class="form-control" required value="{{ $maindata->name }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Code</label>
                                <input type="text" class="form-control" value="{{ $maindata->code }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Detail*</label>
                                <textarea name="detail" placeholder="Detail Categories" class="form-control" required>{{ $maindata->detail }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Image</label>
                                <div class="col-sm-12">
                                    <input name="image" placeholder="Image" type="file" class="form-control" id="imgInp">
                                </div>
                                <div class="col-sm-3">
                                    @if($maindata->image == null )
                                        <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                                    @else
                                    <a id="img-fancy" download data-fancybox href="{{asset('/storage/pos_categories/' . $maindata->image)}}">
                                        <img id='img-upload' src="{{asset('/storage/pos_categories/' . $maindata->image)}}" alt="your image" style="width:125px;height:125px">
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="position-relative row form-check">
                            <div class="col-sm-10 offset-sm-2">
                                <button class="btn btn-success" type="submit" style="float:right"><i class="fa fa-sign-in" ></i>Submit</button>
                            </div>
                        </div><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
@include('pos.categories._js')
<script>

    $("#form-data").submit(function(e) {
        var id = $('#id').val();
        ClassApp.updateData($(this),e, id);
    });

</script>

@endsection
