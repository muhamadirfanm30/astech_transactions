<script>

    var ClassApp = {
        saveData: function(el, e) {
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            var files = $('#imgInp')[0].files[0];
            if(files){
                fd.append('image', files);
            }
            // console.log(form, fd);
            // Display the key/value pairs
            for (var pair of fd.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }

            $.ajax({
                url:Helper.apiUrl('/pos/categories/store'),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        Helper.successNotif("POS Categories Has Been Saved");
                        window.location.href = Helper.redirectUrl('/pos/categories');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            Helper.errorNotif(pesan[0]);
                        })
                    }

                },
            });

            e.preventDefault();
        },
        updateData: function(el, e, id) {
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            var files = $('#imgInp')[0].files[0];
            if(files){
                fd.append('image', files);
            }
            // console.log(form, fd);
            // Display the key/value pairs
            for (var pair of fd.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }

            $.ajax({
                url:Helper.apiUrl('/pos/categories/update/'+id),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        Helper.successNotif("POS Categories Has Been Saved");
                        window.location.href = Helper.redirectUrl('/pos/categories');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            Helper.errorNotif(pesan[0]);
                        })
                    }

                },
            });

            e.preventDefault();
        }
    }

    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>
