<head>
    <title>{{ $title }} | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-sm add-taxes">ADD {{ $title }}</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="table-taxes" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Rate</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="form-taxes">
        <div class="modal fade" id="modal-taxes" data-backdrop="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add {{ $title }}</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <label>Name</label>
                        <div class="form-group">
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        </div>
                        <label>Code</label>
                        <div class="form-group">
                            <input name="code" placeholder="code" id="code" type="text" class="form-control" required>
                        </div>
                        <label>Rate</label>
                        <div class="form-group">
                            <input name="rate" placeholder="rate" id="rate" type="text" class="form-control number_only" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal" id="close_modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

@endsection

@section('script')
<script>
    $(document).on('click', '#close_modal', function() {
        $('#modal-taxes').modal('hide');
    });

    globalCRUD.datatables({
        url: '/pos/taxes/datatables',
        selector: '#table-taxes',
        columnsField: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            'name',
            'code',
            'rate'
        ],
        modalSelector: "#modal-taxes",
        modalButtonSelector: ".add-taxes",
        modalFormSelector: "#form-taxes",
        actionLink: {
            store: function() {
                return "/pos/taxes/store";
            },
            update: function(row) {
                return "/pos/taxes/update/" + row.id;
            },
            delete: function(row) {
                return "/pos/taxes/" + row.id;
            },
        },
    });

    var max = 50;
    $('#rate').keyup(function(e) {
        if ($(this).val() > max) {
        e.preventDefault();
        $(this).val(max);
        } else {
            $(this).val();
        }
    });



</script>
@endsection
