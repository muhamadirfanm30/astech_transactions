<head>
    <title>{{ $title }} | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary btn-sm add-barcode_symbologies">ADD {{ $title }}</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="table-barcode_symbologies" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="form-barcode_symbologies">
        <div class="modal fade" id="modal-barcode_symbologies" data-backdrop="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add {{ $title }}</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <label>Name</label>
                        <div class="form-group">
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        </div>
                        <label>Code</label>
                        <div class="form-group">
                            <input name="code" placeholder="code" id="code" type="text" class="form-control" required>
                        </div>
                        <label>Description</label>
                        <div class="form-group">
                            <textarea name="desc" placeholder="desc" id="desc" class="form-control" required></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal" id="close_modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

@endsection

@section('script')
<script>
    $(document).on('click', '#close_modal', function() {
        $('#modal-barcode_symbologies').modal('hide');
    });

    globalCRUD.datatables({
        url: '/pos/barcode_symbologies/datatables',
        selector: '#table-barcode_symbologies',
        columnsField: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            'name',
            'code',
            'desc'
        ],
        modalSelector: "#modal-barcode_symbologies",
        modalButtonSelector: ".add-barcode_symbologies",
        modalFormSelector: "#form-barcode_symbologies",
        actionLink: {
            store: function() {
                return "/pos/barcode_symbologies/store";
            },
            update: function(row) {
                return "/pos/barcode_symbologies/update/" + row.id;
            },
            delete: function(row) {
                return "/pos/barcode_symbologies/" + row.id;
            },
        },
    });

</script>
@endsection
