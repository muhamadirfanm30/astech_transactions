
<head>
    <title>{{ $title }} Create | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('pos/products')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;
                                Back
                            </a>
                        </div>
                    </div>
                </div>
                <form id="form-data" style="display: contents;" autocomplete="off">
                    <div class="card-body">
                        <input type="hidden" name="id" id="id" value="{{ $maindata->id }}">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="">Product Name*</label>
                                    <input name="name" placeholder="Fill Unique Product name" type="text" class="form-control" value="{{ $maindata->name }}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">SKU*</label>
                                    <input name="sku" placeholder="Fill SKU example (00008BCD)" type="text" class="form-control" value="{{ $maindata->sku }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Barcode Symbology Type*</label>
                                    <select name="pos_barcode_symbologies_id" id="pos_barcode_symbologies_id" class="form-control" required>
                                        <option value="{{ $maindata->pos_barcode_symbologies_id }}" selected>{{ $maindata->barcode_symbology->name }}</option>
                                    </select>
                                    <input name="barcode_symbology_code" id="barcode_symbology_code" type="hidden" value="{{ $maindata->barcode_symbology->code }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Barcode*</label>
                                    <input name="barcode"  id="barcode" placeholder="Product Barcode" type="text" class="form-control" value="{{ $maindata->barcode }}" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Unit*</label>
                                    <select name="pos_unit_types_id" id="pos_unit_types_id" class="form-control" required>
                                        <option value="{{ $maindata->pos_unit_types_id }}" selected>{{ $maindata->unit_type->name }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="">Product Supplier*</label>
                                    <select name="pos_suppliers_id" id="pos_suppliers_id" class="form-control" required>
                                        <option value="{{ $maindata->pos_suppliers_id }}" selected>{{ $maindata->supplier->name }} / {{ $maindata->supplier->phone }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Category*</label>
                                    <select name="pos_categories_id" id="pos_categories_id" type="text" class="form-control" required>
                                        <option value="{{ $maindata->pos_categories_id }}" selected>{{ $maindata->category->name }}</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Cogs Price*</label>
                                    <input name="cogs_price" placeholder="Cogs Price" type="text" class="form-control currency" value="{{ $maindata->cogs_price }}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Selling Price*</label>
                                    <input name="selling_price" placeholder="Selling Price" type="text" class="form-control currency" value="{{ $maindata->selling_price }}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Discount Rate(%) *</label>
                                    <input name="discount_rate" placeholder="Discount Price" type="text" class="form-control number_only" value="{{ $maindata->discount_rate }}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">VAT (Tax)*</label>
                                    <select name="pos_taxes_id" id="pos_taxes_id" class="form-control" required>
                                        <option value="{{ $maindata->pos_taxes_id }}" selected>{{ $maindata->tax->name }} / {{ $maindata->tax->rate }} %</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Status*</label>
                                    <select name="status" id="status" type="text" class="form-control" required>
                                        <option value=1 <?php if($maindata->status == 1) { echo 'selected'; } ?> >Available</option>
                                        <option value=0 <?php if($maindata->status == 0) { echo 'selected'; } ?> >Not Available</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Manufacturing Date</label>
                                    <input name="manufacturing_date" placeholder="Manufacturing Date (optional)" type="text" class="form-control date_format" value="{{ $maindata->manufacturing_date->format('d-m-Y') }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Expiry Date</label>
                                    <input name="expiry_date" placeholder="Expiry Date (optional)" type="text" class="form-control date_format" value="{{ $maindata->expiry_date->format('d-m-Y') }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Product Details*</label>
                                    <textarea name="product_details" id="product_details" placeholder="Product Details" class="form-control required" >{{ $maindata->product_details }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Image</label>
                                    <div class="col-sm-12">
                                        <input name="image" placeholder="Image" type="file" class="form-control" id="imgInp">
                                    </div>
                                    <div class="col-sm-3">
                                        @if($maindata->image == null )
                                            <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                                        @else
                                        <a id="img-fancy" download data-fancybox href="{{asset('/storage/pos_products/' . $maindata->image)}}">
                                            <img id='img-upload' src="{{asset('/storage/pos_products/' . $maindata->image)}}" alt="your image" style="width:125px;height:125px">
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="position-relative row form-group float-right">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Barcode Image</label>
                                    <div class="col-sm-6">
                                        <div id="barcode_image"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="position-relative row form-check">
                            <div class="col-sm-10 offset-sm-2">
                                <button class="btn btn-success" type="submit" style="float:right"><i class="fa fa-sign-in" ></i>Submit</button>
                            </div>
                        </div><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
@include('pos.products._js')
<script>
    Helper.wysiwygEditor('#product_details');
    $("#barcode_image").barcode(
        ''+$('#barcode').val()+'',// Value barcode (dependent on the type of barcode)
        $('#barcode_symbology_code').val() // type (string)
    );
    $(document).on('keyup', '#barcode', function() {
        console.log($(this).val());
        $("#barcode_image").barcode(
            ''+$(this).val()+'',
            $('#barcode_symbology_code').val()
        );
    });
    $(document).on('change', '#pos_barcode_symbologies_id', function() {
        $("#barcode_image").barcode(
            ''+$('#barcode').val()+'',
            $('#barcode_symbology_code').val()
        );
    });
    ClassApp.initializeSelect2Supplier();
    ClassApp.initializeSelect2Vat();

    $("#form-data").submit(function(e) {
        var id = $('#id').val();
        ClassApp.updateData($(this),e,id);
    });

</script>

@endsection
