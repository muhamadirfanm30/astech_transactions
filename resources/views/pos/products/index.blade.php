<head>
    <title>{{ $title }} | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                {{ $title }}
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ url('pos/products/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add {{ $title }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="table-products" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Cogs Price</th>
                                <th>Selling Price</th>
                                <th>Qty</th>
                                <th>Alert Qty</th>
                                <th>Barcode</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script>
        globalCRUD.datatables({
            url: '/pos/products/datatables',
            selector: '#table-products',
            columnsField: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex"
                },
                'name',
                'code',
                {
                    render: function(row,type,full) { //total
                        return full.cogs_price != null ? Helper.toCurrency(full.cogs_price) : '-';
                    }
                },
                {
                    render: function(row,type,full) { //total
                        return full.selling_price != null ? Helper.toCurrency(full.selling_price) : '-';
                    }
                },
                'qty',
                'alert_qty',
                {
                    render: function(row,type,full) {
                        $(".barcode_image").barcode(
                            full.barcode,// Value barcode (dependent on the type of barcode)
                            full.barcode_symbology.code // type (string)
                        );
                        return '<div class="barcode_image"></div>';
                    }
                },
            ],
            actionLink: {
                update: function(row) {
                    return "/pos/products/edit/" + row.id;
                },
                delete: function(row) {
                    return "/pos/products/" + row.id;
                },
            }
        })
    </script>
@endsection
