{{-- Image Upload --}}
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload{
            width: 100%;
        }
    </style>

   {{-- Select2 with description --}}
    <style>
        .rounded-profile {
            border-radius: 100% !important;
            overflow: hidden;
            width: 100px;
            height: 100px;
            border: 8px solid rgba(255, 255, 255, 0.7);
        }

        wrap {
            width: 500px;
            margin: 2em auto;
        }

        .clearfix:before,
        .clearfix:after {
            content: " ";
            display: table;
        }

        .clearfix:after {
            clear: both;
        }

        .select2-result-repository {
            padding-top: 4px;
            padding-bottom: 3px;
        }

        .select2-result-repository__avatar {
            float: left;
            width: 60px;
            height: auto;
            margin-right: 10px;
        }

        .select2-result-repository__avatar img {
            width: 100%;
            height: auto;
            border-radius: 50%;
        }

        .select2-result-repository__meta {
            /* margin-left: 70px; */
            margin-left: 0px;
        }

        .select2-result-repository__title {
            color: black;
            font-weight: bold;
            word-wrap: break-word;
            line-height: 1.1;
            margin-bottom: 4px;
        }

        .select2-result-repository__price,
        .select2-result-repository__stargazers {
            margin-right: 1em;
        }

        .select2-result-repository__price,
        .select2-result-repository__stargazers,
        .select2-result-repository__watchers {
            display: inline-block;
            color: rgb(68, 68, 68);
            font-size: 13px;
        }

        .select2-result-repository__description {
            font-size: 13px;
            color: #777;
            margin-top: 4px;
        }

        .select2-results__option--highlighted .select2-result-repository__title {
            color: white;
        }

        .select2-results__option--highlighted .select2-result-repository__price,
        .select2-results__option--highlighted .select2-result-repository__stargazers,
        .select2-results__option--highlighted .select2-result-repository__description,
        .select2-results__option--highlighted .select2-result-repository__watchers {
            color: #c6dcef;
        }
    </style>
<script>
    $(function() {
        Helper.dateFormat('.date_format');
        Helper.currency('.currency');
        Helper.onlyNumberInput('.number_only');
    });

    globalCRUD.select2('#pos_barcode_symbologies_id', '/pos/barcode_symbologies/select2', function(item) {
        return {
            id: item.id,
            text:item.name,
            code:item.code
        }
    });
    globalCRUD.select2('#pos_unit_types_id', '/pos/unit_types/select2');
    globalCRUD.select2('#ms_branches_id', '/master/branches/select2');
    globalCRUD.select2('#pos_categories_id', '/pos/categories/select2');

    $('#pos_barcode_symbologies_id').on('select2:select', function (e) {
        $('#barcode_symbology_code').val(e.params.data.code);
    });

    var ClassApp = {
        initializeSelect2Supplier: function() {
            $("#pos_suppliers_id").select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/pos/suppliers/select2'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data, ClassApp.renderSelect2Supplier(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });
        },

        renderSelect2Supplier: function(data) {
            return function(data) {
                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>Phone : <b>" + data.phone + "</b></div>" +
                        "<div class='select2-result-repository__description'>Address : <b>" + data.address + "</b></div>" +
                        "</div></div>",
                    name: data.name + ' / ' + data.phone,
                    data: data
                };
            };
        },

        initializeSelect2Vat: function() {
            $("#pos_taxes_id").select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/pos/taxes/select2'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data, ClassApp.renderSelect2Vat(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });
        },

        renderSelect2Vat: function(data) {
            return function(data) {
                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>Rate : <b>" + data.rate + " %</b></div>" +
                        "</div></div>",
                    name: data.name + ' / ' + data.rate + ' %',
                    data: data
                };
            };
        },
        saveData: function(el, e) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            var files = $('#imgInp')[0].files[0];
            if(files){
                fd.append('image', files);
            }
            // console.log(form, fd);
            // Display the key/value pairs
            for (var pair of fd.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }

            $.ajax({
                url:Helper.apiUrl('/pos/products/store'),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        Helper.successNotif("POS Product Has Been Saved");
                        window.location.href = Helper.redirectUrl('/pos/products');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            Helper.errorNotif(pesan[0]);
                        })
                    }

                },
            });

            e.preventDefault();
        },
        updateData: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            var files = $('#imgInp')[0].files[0];
            if(files){
                fd.append('image', files);
            }
            // console.log(form, fd);
            // Display the key/value pairs
            for (var pair of fd.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }

            $.ajax({
                url:Helper.apiUrl('/pos/products/update/'+id),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        Helper.successNotif("POS Products Has Been Saved");
                        window.location.href = Helper.redirectUrl('/pos/products');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            Helper.errorNotif(pesan[0]);
                        })
                    }

                },
            });

            e.preventDefault();
        }
    }

    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>
