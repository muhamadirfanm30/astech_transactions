<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      {{-- <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span><center><strong>M-Care Cashier</strong> 2.0</center></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            {{-- <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div> --}}
            <div class="image">
                <div class="profile-userpic text-center">
                    <div class="avatar-upload">
                        <div class="avatar-previews">
                            {{-- @if(!empty(Auth::user()->avatar_user)) --}}
                                <div id="imagePreview" style="background-image: url( {{ url('/storage/users-profile/'. Auth::user()->attachment) }} )"></div>
                            {{-- @else
                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="info">
                <a href="{{ url('system-setting/my-profile') }}" class="d-block">Hi, {{Auth::user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ url('home') }}" class="nav-link {{Request::segment(1) == 'home' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @php
                    $menuMasterData = '';
                    $arrMenu = ['brands',
                                'category_spareparts',
                                'companies',
                                'banks',
                                'spareparts',
                                'customers',
                                'services',
                                'payments',
                                'branch-office',
                                'model-part'
                            ];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuMasterData = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$menuMasterData}}">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-copy"></i>Master Data Admin</a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('master/payments') }}" class="nav-link {{Request::segment(2) == 'payments' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'payments' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Payment</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/brands') }}" class="nav-link {{Request::segment(2) == 'brands' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'brands' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Brand</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/model-part') }}" class="nav-link {{Request::segment(2) == 'model-part' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'model-part' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Model Part</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/spareparts') }}" class="nav-link {{Request::segment(2) == 'spareparts' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'spareparts' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Spareparts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/services') }}" class="nav-link {{Request::segment(2) == 'services' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'services' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Services</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/customers') }}" class="nav-link {{Request::segment(2) == 'customers' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'customers' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Customers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/banks') }}" class="nav-link {{Request::segment(2) == 'banks' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'banks' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Banks</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/category_spareparts') }}" class="nav-link {{Request::segment(2) == 'category_spareparts' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'category_spareparts' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Category Spareparts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('master/companies') }}" class="nav-link {{Request::segment(2) == 'companies' ? 'active' : ''}} {{Request::segment(2) == 'branch-office' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'companies' ? 'far fa-check-circle' : 'far fa-circle'}} {{Request::segment(2) == 'branch-office' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Companies</p>
                            </a>
                        </li>
                    </ul>
                </li>

                @php
                    $menuTransaksi = '';
                    $arrMenu = ['categories','chapters','products','purchases','suppliers','refunds','sales','reports','taxes','barcode_symbologies','unit_types'];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuTransaksi = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$menuTransaksi}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Point Of Sale (POS)
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('pos/categories') }}" class="nav-link {{Request::segment(2) == 'categories' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'categories' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{Request::segment(2) == 'chapters' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'chapters' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Chapters (soon)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pos/products') }}" class="nav-link {{Request::segment(2) == 'products' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'products' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Products</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{Request::segment(2) == 'purchases' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'purchases' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Purchases (soon)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pos/suppliers') }}" class="nav-link {{Request::segment(2) == 'suppliers' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'suppliers' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Suppliers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{Request::segment(2) == 'refunds' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'refunds' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Refunds (soon)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{Request::segment(2) == 'sales' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'sales' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Sales (soon)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{Request::segment(2) == 'reports' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'reports' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Reports (soon)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pos/taxes') }}" class="nav-link {{Request::segment(2) == 'taxes' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'taxes' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Taxes</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pos/barcode_symbologies') }}" class="nav-link {{Request::segment(2) == 'barcode_symbologies' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'barcode_symbologies' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Barcode Symbologies</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pos/unit_types') }}" class="nav-link {{Request::segment(2) == 'unit_types' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'unit_types' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Unit Types</p>
                            </a>
                        </li>
                    </ul>
                </li>

                @php
                    $menuTransaksi = '';
                    $trans = ['quotation_orders'];
                    if(in_array(Request::segment(2),$trans)){
                        $menuTransaksi = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$menuTransaksi}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Transactions
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('transaction/quotation_orders') }}" class="nav-link {{Request::segment(2) == 'quotation_orders' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'quotation_orders' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Quotation orders</p>
                            </a>
                        </li>
                    </ul>
                </li>

                @php
                    $menuUtility = '';
                    $trans = ['list-user'];
                    if(in_array(Request::segment(2),$trans)){
                        $menuUtility = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$menuUtility}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            Utility System
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('utility/list-user') }}" class="nav-link {{Request::segment(2) == 'list-user' ? 'active' : ''}}">
                            <i class="{{Request::segment(2) == 'list-user' ? 'far fa-check-circle' : 'far fa-circle'}} nav-icon"></i>
                                <p> Users System</p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<style>
    /* Profile container */
    .profile {
      margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
      background: #fff;
    }

    .profile-userpic img {
      float: none;
      margin: 0 auto;
      width: 10%;
      -webkit-border-radius: 10% !important;
      -moz-border-radius: 10% !important;
      border-radius: 10% !important;
    }

    .profile-usertitle {
      text-align: center;
      margin-top: 20px;
    }

    .profile-usertitle-name {
      color: #5a7391;
      font-size: 16px;
      font-weight: 600;
      margin-bottom: 7px;
    }

    .profile-usertitle-job {
      text-transform: uppercase;
      color: #555;
      font-size: 12px;
      font-weight: 600;
      margin-bottom: 15px;
    }

    .profile-userbuttons {
      text-align: center;
      margin-top: 10px;
    }

    .profile-userbuttons .btn {
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 600;
      padding: 6px 15px;
      margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
      margin-right: 0px;
    }

    .profile-usermenu {
      margin-top: 30px;
    }

    /* Profile Content */
    .profile-content {
      padding: 20px;
      background: #fff;
      min-height: 460px;
    }

    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f040";
      font-family: 'FontAwesome';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-previews {
      width: 35px;
      height: 35px;
      position: relative;
      border-radius: 100%;
      border: 3px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-previews > div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
</style>
