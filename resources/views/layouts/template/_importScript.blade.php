<script>

    $(document).ready(function() {
        $('#import').attr('disabled', true);
        $('.msg').hide();
		$('.progress').show();
        $('#file').on('change',function(){
            var size = 10; //in MB
            var maxSize = 1024 * (1024*size);
            if(this.files[0].size > maxSize){
                $('#import').attr('disabled', true);
                Helper.errorNotif('Error. File size may not exceed '+ size +' MB');

            }
            else{
                $('#import').attr('disabled', false);
            }
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'xlsx':
                    $('#import').attr('disabled', false);
                    break;
                default:
                $('#import').attr('disabled', true);
                Helper.errorNotif('Extension must xlsx !.');

            }

        });
    });

     $("#import").click(function() {
        Helper.loadingStart();
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        var type_import = $('#type_import').val();
        if(files){
            fd.append('file', files);
            fd.append('type_import', type_import);
        }
        $.ajax({
            xhr : function() {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener('progress', function(e){
					if(e.lengthComputable){
						var percent = Math.round((e.loaded / e.total) * 100);
						$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
					}
				});
				return xhr;
			},
            url:Helper.apiUrl('/import-excel'),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data)
                Helper.hideSidebar();
                data = JSON.parse(data);
                myData = data.preview;
                if(data.error == 0) {
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','none');
                    if(type_import == 'category_spareparts') {
                        $('#preview').DataTable({
                            destroy: true,
                            bAutoWidth: false,
                            "aaData": data.preview,
                            "aoColumns": [
                                { "mDataProp": "name", width: '200px'}
                            ]
                        });
                    } else if(type_import == 'services') {
                        var table = $('#preview').DataTable({
                            destroy: true,
                            bAutoWidth: false,
                            "aaData": data.preview,
                            "aoColumns": [
                                { "mDataProp": "status"},
                                { "mDataProp": "company_name"},
                                { "mDataProp": "brand_name"},
                                { "mDataProp": "service_code"},
                                { "mDataProp": "description"},
                                { "mDataProp": "svc_type"},
                                { "mDataProp": "defect_type"},
                                { "mDataProp": "cogs_price"},
                                { "mDataProp": "selling_price"}

                            ]
                        });
                    } else if(type_import == 'model_parts') {
                        var table = $('#preview').DataTable({
                            destroy: true,
                            bAutoWidth: false,
                            "aaData": data.preview,
                            "aoColumns": [
                                { "mDataProp": "company_name"},
                                { "mDataProp": "model_code"},
                                { "mDataProp": "brand_name"},
                                { "mDataProp": "category_name"},
                                { "mDataProp": "model_name"},
                                { "mDataProp": "service_code"},
                                { "mDataProp": "desc"},

                            ]
                        });
                    }

                    $('#modal_preview').modal('show');
                }
                else if(data.error == 1){
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','none');
                    $('#import').attr('disabled', true);
                    Helper.errorNotif(data.msg);
                }
                else if(data.error == 2) {
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','block');
                    $('#import').attr('disabled', true);
                    $('.note span').append('<p style="font-style: italic;font-weight:800; color:#FF6347;">'+data.data.length+" error has found</p>");
                    for(i=0;i<data.data.length;i++){
                        $('.note #note-content').append((i+1)+". "+data.data[i]+"<br/>");
                    }
                }

            },
            complete:function(){
                Helper.loadingStop();
                $('.preview').removeClass('hidden');
            },
            error:function(data){
                Helper.loadingStop();
                $('.loader').css('display','none');
                $('#import').attr('disabled', false);
                Helper.errorNotif('Error : '+data.responseJSON.msg);

            }
        });
    });


    var myData;

    function saveDataCategorySpareparts() {
        console.log(myData);
        Helper.loadingStart();
        $('#import').attr('disabled', true);
        $.ajax({
            url:Helper.apiUrl('/master/import/category_spareparts'),
            type: "POST",
            data: {data:myData},
            success: function (data) {
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.successNotif('Category Spareparts Import has been Success !');
                location.reload();
            },error:function(){
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.errorNotif(data.msg);
            }
        });
    }

    function saveDataServices() {
        Helper.loadingStart();
        $('#import').attr('disabled', true);
        $.ajax({
            url:Helper.apiUrl('/master/import/services'),
            type: "POST",
            data: {data:myData},
            success: function (data) {
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.successNotif('Services Import has been Success !');
                location.reload();
            },error:function(){
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.errorNotif(data.msg);
            }
        });
    }

    function saveDataModelParts() {
        // // console.log(JSON.stringify(myData));
        // var aa = JSON.stringify(myData);

        Helper.loadingStart();
        $('#import').attr('disabled', true);
        $.ajax({
            url:Helper.apiUrl('/master/import/model_parts'),
            type: "POST",
            data: {data:myData},
            success: function (data) {
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.successNotif('Model Parts Import has been Success !');
                location.reload();
            },error:function(){
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.errorNotif(data.msg);
            }
        });
    }

</script>
