
<!DOCTYPE html>
<html lang="en">
    @include('layouts.template._header')

    <body class="hold-transition sidebar-mini">

        <div class="wrapper">
            @include('layouts.template._navbar')
            <!-- Main Sidebar Container -->
            @include('layouts.template._sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="content"><br>
                    @yield('content')
                </div>
            </div>
            {{-- @include('layouts.template._footer') --}}

            <!-- /.content-wrapper -->
            <!-- Control Sidebar -->

            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <h5>Title</h5>
                    <p>Sidebar content</p>
                </div>
            </aside>
        </div>
        @include('layouts.template._script')
        @yield('script')
    </body>
</html>
