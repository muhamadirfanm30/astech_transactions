@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="card-title">
                                    Create Quotation Orders
                                </h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ url('/transaction/quotation_orders') }}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-back"></i>&nbsp;
                                    Back
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="col-md-12">
                <div class="card">
                    <div class="card-body card-outline card-info">
                        <div class="row text-right">
                            <div class="col-md-6 text-right">
                                <h2><b>TOTAL : </b></h2>
                            </div>
                            <div class="col-md-6 text-left">
                                <b>
                                    <h2 class="text-success grand_total"></h2>
                                </b>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <form id="form-data" style="display: contents;" autocomplete="off">
                @csrf
                <section class="col-md-6">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Info</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Date*</label>
                                        <div class="input-group">
                                            <input name="date" type="text" class="form-control date_format" required readonly>
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Type Transaction*</label>
                                        <select name="type_transaction" class="form-control required">
                                            <option value="service">Service</option>
                                            <option value="sparepart">Sparepart</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Brand *</label>
                                        <select name="ms_brands_id" id="ms_brands_id" class="form-control" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Type Product *</label>
                                        <select name="type_product" id="type_product" class="form-control" required>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">RO / BILL *</label>
                                        <input type="text" name="bill_number" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">SO NUMBER *</label>
                                        <input type="text" name="so_number" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="">IMEI/SN</label>
                                        <input type="text" name="imei_number" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="">BILL TO</label>
                                        <select name="bill_to" class="form-control required">
                                            <option value="END USER">END USER</option>
                                            <option value="PRINCIPAL">PRINCIPAL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Keterangan *</label>
                                        <textarea name="description" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="col-md-6">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Customer</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Customer Name *</label>
                                        <input type="text" name="customer_name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Handphone *</label>
                                        <input type="text" name="handphone" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email *</label>
                                        <input type="text" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Alamat *</label>
                                        <textarea name="address" class="form-control" rows="4" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="col-md-12" id="section_sparepart">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Sparepart</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="btn-actions-pane-left text-right">
                                <button type="button" class="btn btn-success btn_add_sparepart"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode</th>
                                        <th>Desc Sparepart</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Discount</th>
                                        <th>Sub Total</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="dynamic_sparepart">
                                    <tr>
                                        <td><span class="counter">1</span></td>
                                        <input type="hidden" value=0 id="count_sparepart_details_data" />
                                        <td>
                                            <input name="sparepart_status[]" type="hidden"
                                                class="form-control sparepart_status-xxx" data-uniq="xxx" value="PART">
                                            <input name="ms_spareparts_id[]" type="hidden"
                                                class="form-control ms_spareparts_id-xxx" data-uniq="xxx">
                                            <input name="sparepart_code[]" type="text" placeholder="Kode Sparepart"
                                                class="form-control sparepart_code-xxx" data-uniq="xxx" required
                                                style="width:100%">
                                        </td>
                                        <td width="30%">
                                            <input name="sparepart_description[]" type="text"
                                                placeholder="Description Sparepart"
                                                class="form-control sparepart_description-xxx" data-uniq="xxx"
                                                style="width:100%" required>
                                        </td>
                                        <td>
                                            <input name="sparepart_qty[]" type="text" placeholder="Quantity"
                                                class="form-control sparepart_qty-xxx number_only" data-uniq="xxx" required>
                                        </td>
                                        <td>
                                            <input name="sparepart_price[]" type="text" placeholder="Price"
                                                class="form-control sparepart_price-xxx number_only currency" data-uniq="xxx" required>
                                        </td>
                                        <td>
                                            <input name="sparepart_discount[]" type="text" placeholder="Discount"
                                                class="form-control sparepart_discount-xxx number_only currency" data-uniq="xxx"
                                                >
                                        </td>
                                        <td>
                                            <input name="sub_total_sparepart[]" type="text"
                                                class="form-control sub_total_sparepart-xxx number_only currency" data-uniq="xxx"
                                                readonly>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL SPAREPART</b></td>
                                        <td><input name="total_qty_sparepart" type="text" class="form-control"
                                                id="total_qty_sparepart" readonly></td>
                                        <td></td>
                                        <td></td>
                                        <td><input name="total_sparepart" type="text" class="form-control" id="total_sparepart"
                                                readonly></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <section class="col-md-12" id="section_service">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Service</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="btn-actions-pane-left text-right">
                                <button type="button" class="btn btn-success btn_add_service"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode</th>
                                        <th>Desc Service</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Discount</th>
                                        <th>Sub Total</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="dynamic_service">
                                    <tr>
                                        <td><span class="counter">1</span></td>
                                        <input type="hidden" value=0 id="count_service_details_data" />
                                        <td>
                                            <input name="service_status[]" type="hidden" class="form-control service_status-xxx"
                                                data-uniq="xxx"  value="LABOR">
                                            <input name="ms_services_id[]" type="hidden" class="form-control ms_services_id-xxx"
                                                data-uniq="xxx">
                                            <input name="service_code[]" type="text" placeholder="Kode Service"
                                                class="form-control service_code-xxx" data-uniq="xxx"
                                                style="width:100%">
                                        </td>
                                        <td width="30%">
                                            <input name="service_description[]" type="text" placeholder="Description Sparepart"
                                                class="form-control service_description-xxx" data-uniq="xxx" style="width:100%"
                                                >
                                        </td>
                                        <td>
                                            <input name="service_qty[]" type="text" placeholder="Quantity"
                                                class="form-control service_qty-xxx number_only" data-uniq="xxx" >
                                        </td>
                                        <td>
                                            <input name="service_price[]" type="text" placeholder="Price"
                                                class="form-control service_price-xxx number_only currency" data-uniq="xxx" >
                                        </td>
                                        <td>
                                            <input name="service_discount[]" type="text" placeholder="Discount"
                                                class="form-control service_discount-xxx number_only currency" data-uniq="xxx" >
                                        </td>
                                        <td>
                                            <input name="sub_total_service[]" type="text"
                                                class="form-control sub_total_service-xxx number_only currency" data-uniq="xxx" readonly>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL SERVICE</b></td>
                                        <td><input name="total_qty_service" type="text" class="form-control"
                                                id="total_qty_service" readonly></td>
                                        <td></td>
                                        <td></td>
                                        <td><input name="total_service" type="text" class="form-control" id="total_service"
                                                readonly></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <section class="col-md-12" id="section_price_calculation">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Price Calculation</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group row">
                                            <label class="col-sm-1 control-label text-right text-danger">DP</label>
                                            <div class="col-sm-1">
                                                <input type="text" id="payment_dp_default" class="form-control text-center" value="50" readonly="">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" id="payment_dp_calculated" name="payment_dp_calculated"
                                                    class="form-control text-right currency" readonly="">
                                            </div>
                                            <label class="col-sm-1 control-label no-padding-right text-danger">STATUS</label>
                                            <input type="hidden" id="ms_payments_id" name="ms_payments_id"
                                                    class="form-control text-right" value=1>
                                            <div class="col-sm-2">
                                                <input type="text" id="payment_status_1" name="payment_status_1"
                                                    class="form-control text-center" readonly="" value="OP">
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" id="payment_status_2" name="payment_status_2"
                                                    class="form-control text-center" readonly="" value="OP">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-1 control-label text-right"><b>DP PAID</b></label>
                                            <div class="col-sm-4">
                                                <input type="text" id="payment_dp_paid" name="payment_dp_paid"
                                                    class="form-control text-right currency" placeholder="Dp Amount" readonly="">
                                            </div>
                                            <label class="col-sm-1 control-label"><b>INV.PAID</b></label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control text-right" name="payment_invoice_paid"
                                                    id="payment_invoice_paid" placeholder="Invoice Amount" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">TOTAL Sparepart:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="calc_total_sparepart" name="calc_total_sparepart"
                                                    class="form-control text-right currency" readonly>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">&nbsp;&nbsp;TOTAL Service:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="calc_total_service" name="calc_total_service"
                                                    class="form-control text-right currency" readonly>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-2 control-label">DISC :</label>
                                            <div class="col-sm-4">
                                                <input type="number" id="calc_discount_percent" min="0" max="100"
                                                    name="calc_discount_percent" class="form-control text-center" value=0>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" id="calc_discount_nominal" name="calc_discount_nominal"
                                                    class="form-control text-right currency">
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-2 control-label">PPN :</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="calc_ppn_percent" name="calc_ppn_percent"
                                                    class="form-control text-center" value=10 readonly>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" id="calc_ppn_nominal" name="calc_ppn_nominal"
                                                    class="form-control text-right currency" readonly="">
                                            </div>
                                        </div>
                                        <div class="form-group row text-right">
                                            <label class="col-sm-7 control-label no-padding-right">GRAND TOTAL :</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control text-right" name="calc_grand_total_display"
                                                    id="calc_grand_total_display" readonly="">
                                                <input type="hidden" class="form-control text-right" name="calc_grand_total"
                                                    id="calc_grand_total" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

                <section class="col-md-12" id="section_payment">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Payment</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">PAYMENT:</label>
                                            <div class="col-sm-9">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <button type="button" class="btn btn-warning"
                                                            id="open_payment_details_modal"><i
                                                                class="fa fa-search disabled"></i></button>
                                                    </div>
                                                    <!-- /btn-group -->
                                                    <input type="text" class="form-control" name="payment" id="payment"
                                                        readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">REMAINING:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control text-right" name="remaining"
                                                    id="remaining" readonly>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row justify-content-end">
                                        <button type="submit" class="btn btn-success mr-2" id="save_data"><i
                                                class="fa fa-save"></i> Save Data</button>
                                        <button type="button" class="btn btn-primary mr-2 disabled" id="open_payment_modal"><i
                                                class="fa fa-print"></i> Payment</button>
                                        <a href="{{ url('/transaction/quotation_orders') }}" class="btn btn-danger mr-2" id="back"><i class="fa fa-reply"></i>
                                            Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </form>
        </div>
    </section>


    <!-- Modal Find Sparepart-->
    <div class="modal fade " id="sparepart_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Sparepart</h5>
                </div>
                <div class="modal-body">
                    <table id="table-find-sparepart" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode Sparepart</th>
                                <th>Deskripsi</th>
                                <th>UOM</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_sparepart_modal"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="select_sparepart_modal" data-dismiss="modal"
                        data-uniq="xxx" data-sparepart-id="" data-sparepart-code="" data-sparepart-description=""
                        data-sparepart-price="">Select</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Find Service-->
    <div class="modal fade " id="service_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Service</h5>
                </div>
                <div class="modal-body">
                    <table id="table-find-service" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode Sparepart</th>
                                <th>SVC Type</th>
                                <th>Defect Type</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_service_modal"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="select_service_modal" data-dismiss="modal"
                        data-uniq="xxx" data-service-id="" data-service-code="" data-service-description=""
                        data-service-price="">Select</button>
                </div>
            </div>
        </div>
    </div>



    @endsection

    @section('script')
        <style>
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {

                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance: textfield;
                /* Firefox */
            }

        </style>

        @include('transaction.quotation_orders._main_js')
        @include('transaction.quotation_orders._sparepart_js')
        @include('transaction.quotation_orders._service_js')
        <script>
            $(function() {
                Helper.dateFormat('.date_format');
                // Helper.currency('.currency');
                ClassApp.selectCode();
                ClassApp.hitungTotal();
                $("#calc_discount_percent").change(function() {
                    var max = parseInt($(this).attr('max'));
                    var min = parseInt($(this).attr('min'));
                    if ($(this).val() > max) {
                        $(this).val(max);
                    } else if ($(this).val() < min) {
                        $(this).val(min);
                    }
                });


            });

            $(document).on('click', '#open_payment_details_modal', function() {
                $('#payment_details_modal').modal('show');
            });
            $(document).on('click', '#close_payment_details_modal', function() {
                $('#payment_details_modal').modal('hide');
            });
            $(document).on('click', '#close_payment_modal', function() {
                $('#payment_modal').modal('hide');
            });

            $(document).on('change', 'select[name="type_transaction"]', function() {
                console.log('asdas');
                if ($(this).val() === 'sparepart') {
                    $('#section_service').hide();
                } else {
                    $('#section_sparepart').show();
                    $('#section_service').show();
                }
            })

            globalCRUD.select2Tags('#type_product', '/transaction/quotation_orders/type_product/select2', function(item) {
                return {
                    id: item.type_product,
                    text: item.type_product,
                }
            });

            globalCRUD.select2('#ms_brands_id', '/master/brands/select2', function(item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });

            $("#form-data").submit(function(e) {
                ClassApp.saveData($(this), e);
            });

            $(document).on('keyup', 'input[name="calc_discount_percent"]', function() {
                ClassApp.setCalcDiscountPercent($(this).attr('data-uniq'));
                ClassApp.hitungTotal();
            });
            $(document).on('keyup', 'input[name="calc_discount_nominal"]', function() {
                ClassApp.setCalcDiscountPercent($(this).attr('data-uniq'));
                ClassApp.hitungTotal();
            });

            var tbl_payment = $('#table-payment').DataTable({
                ajax: {
                    url: Helper.apiUrl('/transaction/quotation_orders/payment_details/datatables'),
                    type: "get",
                    error: function() {},
                    complete: function() {},
                },
                selector: '#table-payment',
                dom: 't',
                columns: [{
                        data: "DT_RowIndex",
                        name: "DT_RowIndex",
                        sortable: false,
                    },
                    {
                        data: 'payment.name',
                        name: 'payment.name',
                        class: 'text-danger'
                    },
                    {
                        data: 'date',
                        name: 'date',
                        class: 'text-center',
                    },
                    {
                        data: 'nominal',
                        name: 'nominal',
                        render: function(data, type, full) {
                            return Helper.toCurrency(data);
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: "id",
                        name: "id",
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full) {
                            return "<button  class='btn btn-danger btn-sm' onclick='deleteData" + full.id +
                                "'><i class='fa fa-trash'></i></button>";
                        }
                    }
                ]
            });
        </script>
    @endsection
