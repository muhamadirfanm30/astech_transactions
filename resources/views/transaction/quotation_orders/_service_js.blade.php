<script>
    // add service btn
    // var _uniq_counter_service = 1;
    var _uniq_counter_service = parseInt($('#count_service_details_data').val())+1;
    $(document).on('click', ".btn_add_service", function(e) {
        uniq_service = _uniq_counter_service;
        ClassApp.templateNewInputService(uniq_service);
        _uniq_counter_service++;
    });

    // delete service btn
    $(document).on('click', '.btn_remove_service', function() {
        // var button_id = $(this).attr("data-uniq");
        // console.log(button_id)
        // $('#service_row-' + button_id + '').remove();
        _uniq_counter_service--;
        ClassApp.hitungTotal();
        // delete desciption data
        ClassApp.deleteService($(this));
    });

    $(document).on('keyup', 'input[name="service_qty[]"]', function() {
        ClassApp.setServiceQty($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });


    $(document).on('keyup', 'input[name="service_price[]"]', function() {
        ClassApp.setServicePrice($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });

    $(document).on('keyup', 'input[name="service_discount[]"]', function() {
        $('.service_discount-' + $(this).attr('data-uniq') +'').removeClass('is-invalid');
        ClassApp.setServiceDiscount($(this).attr('data-uniq'));
        ClassApp.hitungTotal();

        if($('.sub_total_service-' + $(this).attr('data-uniq')).val() < 0 ) {
            $('.service_discount-' + $(this).attr('data-uniq') +'').addClass('is-invalid');
            $('.service_discount-' + $(this).attr('data-uniq') +'').val(0);
            Helper.errorNotif('Nilai diskon harus kurang dari subtotal');
            $('.sub_total_service-' + $(this).attr('data-uniq') +'').val(0);
        }
    });

    $(document).ready(function() {
        $('#select_service_modal').prop('disabled', true);
        $('#close_service_modal').click( function() {
            $('#service_modal').modal('hide');
        });
    });


    var tbl_find_service = $('#table-find-service').DataTable({
        ajax: {
            url: Helper.url('/master/services/datatables'),
            type: "get",
            error: function() {},
            complete: function() {},
        },
        select: {
            style: 'single'
        },
        selector: '#table-find-service',

        columns: [
            {
                data: 'service_code',
                name: 'service_code',
            },
            {
                data: 'description',
                name: 'description',
                class: 'text-center',
            },
            {
                data: 'svc_type',
                name: 'svc_type',
            },
            {
                data: 'defect_type',
                name: 'defect_type',
            },
            {
                data: 'selling_price',
                name: 'selling_price',
                class: 'text-center',
                sortable: false
            },
        ]
    });

    $('#table-find-service tbody').on( 'click', 'tr', function () {
        $('#select_service_modal').prop('disabled', false);
        var service_id = tbl_find_service.row( this ).data().id;
        var service_code = tbl_find_service.row( this ).data().service_code;
        var description = tbl_find_service.row( this ).data().description;
        var svc_type = tbl_find_service.row( this ).data().svc_type;
        var defect_type = tbl_find_service.row( this ).data().defect_type;
        var price = parseInt(tbl_find_service.row( this ).data().selling_price);
        console.log(tbl_find_service.row( this ).data());
        $('#select_service_modal').attr('data-service-id',service_id);
        $('#select_service_modal').attr('data-service-code',service_code);
        $('#select_service_modal').attr('data-service-description',description);
        $('#select_service_modal').attr('data-service-price',svc_type);
        $('#select_service_modal').attr('data-service-price',defect_type);
        $('#select_service_modal').attr('data-service-price',price);
    });
</script>
