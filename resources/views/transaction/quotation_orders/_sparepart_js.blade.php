<script>
    // add sparepart btn
    // var _uniq_counter_sparepart = 1;
    var _uniq_counter_sparepart = parseInt($('#count_sparepart_details_data').val())+1;
    $(document).on('click', ".btn_add_sparepart", function(e) {
        uniq_sparepart = _uniq_counter_sparepart;
        ClassApp.templateNewInputSparepart(uniq_sparepart);
        _uniq_counter_sparepart++;
    });

    // delete sparepart btn
    $(document).on('click', '.btn_remove_sparepart', function() {
       // var button_id = $(this).attr("data-uniq");
        // console.log(button_id)
        // $('#service_row-' + button_id + '').remove();
        _uniq_counter_sparepart--;
        ClassApp.hitungTotal();
        // delete desciption data
        ClassApp.deleteSparepart($(this));
    });

    $(document).on('keyup', 'input[name="sparepart_qty[]"]', function() {
        ClassApp.setSparepartQty($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });


    $(document).on('keyup', 'input[name="sparepart_price[]"]', function() {
        ClassApp.setSparepartPrice($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });

    $(document).on('keyup', 'input[name="sparepart_discount[]"]', function() {
        $('.sparepart_discount-' + $(this).attr('data-uniq') +'').removeClass('is-invalid');
        ClassApp.setSparepartDiscount($(this).attr('data-uniq'));
        ClassApp.hitungTotal();

        if($('.sub_total_sparepart-' + $(this).attr('data-uniq')).val() < 0 ) {
            $('.sparepart_discount-' + $(this).attr('data-uniq') +'').addClass('is-invalid');
            $('.sparepart_discount-' + $(this).attr('data-uniq') +'').val(0);
            Helper.errorNotif('Nilai diskon harus kurang dari subtotal');
            $('.sub_total_sparepart-' + $(this).attr('data-uniq') +'').val(0);
        }
    });


    $(document).ready(function() {
        $('#select_sparepart_modal').prop('disabled', true);
        $('#close_sparepart_modal').click( function() {
            $('#sparepart_modal').modal('hide');
        });
    });

    var tbl_find_sparepart = $('#table-find-sparepart').DataTable({
        ajax: {
            url: Helper.url('/master/spareparts/datatables'),
            type: "get",
            error: function() {},
            complete: function() {},
        },
        select: {
            style: 'single'
        },
        selector: '#table-find-sparepart',

        columns: [
            {
                data: 'sparepart_code',
                name: 'sparepart_code',
            },
            {
                data: 'description',
                name: 'description',
                class: 'text-center',
            },
            {
                data: 'uom',
                name: 'uom',
                class: 'text-center',
            },
            {
                data: 'selling_price',
                name: 'selling_price',
                class: 'text-center',
                sortable: false
            },
        ]
    });

    $('#table-find-sparepart tbody').on( 'click', 'tr', function () {
        $('#select_sparepart_modal').prop('disabled', false);
        var sparepart_id = tbl_find_sparepart.row( this ).data().id;
        var sparepart_code = tbl_find_sparepart.row( this ).data().sparepart_code;
        var description = tbl_find_sparepart.row( this ).data().description;
        var uom = tbl_find_sparepart.row( this ).data().uom;
        var price = parseInt(tbl_find_sparepart.row( this ).data().selling_price);
        console.log(tbl_find_sparepart.row( this ).data());
        $('#select_sparepart_modal').attr('data-sparepart-id',sparepart_id);
        $('#select_sparepart_modal').attr('data-sparepart-code',sparepart_code);
        $('#select_sparepart_modal').attr('data-sparepart-description',description);
        $('#select_sparepart_modal').attr('data-sparepart-uom',uom);
        $('#select_sparepart_modal').attr('data-sparepart-price',price);
    });
</script>
