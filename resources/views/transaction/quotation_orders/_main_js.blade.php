<script>

var _uniq_counter_description = 1;

Helper.onlyNumberInput('.number_only');


var ClassApp = {
    setSparepartQty: function (uniq) {
        return $('.sparepart_qty-' + uniq).val();
    },
    setSparepartPrice: function (uniq) {
        return $('.sparepart_price-' + uniq).val();
    },
    setSparepartDiscount: function (uniq) {
        return $('.sparepart_discount-' + uniq).val();
    },
    setServiceQty: function (uniq) {
        return $('.service_qty-' + uniq).val();
    },
    setServicePrice: function (uniq) {
        return $('.service_price-' + uniq).val();
    },
    setServiceDiscount: function (uniq) {
        return $('.service_discount-' + uniq).val();
    },
    setCalcDiscountPercent: function (uniq) {
        return $('.calc_discount_percent-' + uniq).val();
    },
    setCalcDiscountNominal: function (uniq) {
        return $('.calc_discount_nominal-' + uniq).val();
    },

    hitungTotal: function() {

        Helper.unMask('input[name="sub_total_sparepart[]"]');
        Helper.unMask('input[name="total_sparepart[]"]');
        // Helper.unMask('input[name="sub_total_service[]"]');
        // Helper.unMask('input[name="total_service[]"]');

        var total_sparepart_qty = 0;
        var total_service_qty = 0;
        var sub_total_sparepart = 0;
        var sub_total_service = 0;
        var total_sparepart = 0;
        var total_service = 0;
        var total = 0;
        var grand_total = 0;
        var discount_percent = 0;
        var discount_nominal = 0;
        var ppn_percent = 0;
        var ppn_nominal = 0;


        $('input[name="sparepart_qty[]"]').each(function() {
            var uniq = $(this).attr('data-uniq');
            var qty = $('.sparepart_qty-' + uniq).val() == '' ? 0 : parseInt($('.sparepart_qty-' + uniq).val());
            var price = $('.sparepart_price-' + uniq).val() == '' ? 0 : parseInt($('.sparepart_price-' + uniq).val());
            var discount = $('.sparepart_discount-' + uniq).val() == '' ? 0 : parseInt($('.sparepart_discount-' + uniq).val());
            var sub_total_sparepart = (qty * price) - discount;

            total_sparepart_qty += qty;
            $('.sub_total_sparepart-' + uniq).val(Helper.toCurrency(sub_total_sparepart));
            total_sparepart += sub_total_sparepart;
            $('#total_qty_sparepart').val(total_sparepart_qty);
            $('#total_sparepart').val(Helper.toCurrency(total_sparepart));
            $('#calc_total_sparepart').val(Helper.toCurrency(total_sparepart));

            total = total_sparepart + total_service;
            ClassApp.calculateGrandTotal(total);

        });

        $('input[name="service_qty[]"]').each(function() {
            var uniq = $(this).attr('data-uniq');
            var qty = $('.service_qty-' + uniq).val() == '' ? 0 : parseInt($('.service_qty-' + uniq).val());
            var price = $('.service_price-' + uniq).val() == '' ? 0 : parseInt($('.service_price-' + uniq).val());
            var discount = $('.service_discount-' + uniq).val() == '' ? 0 : parseInt($('.service_discount-' + uniq).val());
            var sub_total_service = (qty * price) - discount;

            total_service_qty += qty;
            $('.sub_total_service-' + uniq).val(Helper.toCurrency(sub_total_service));
            total_service += sub_total_service;
            $('#total_qty_service').val(total_service_qty);
            $('#total_service').val(Helper.toCurrency(total_service));
            $('#calc_total_service').val(Helper.toCurrency(total_service));

            total = total_service + total_sparepart;
            ClassApp.calculateGrandTotal(total);


        });

    },

    calculateGrandTotal: function(total) {
        discount_percent_dec = (parseInt($('#calc_discount_percent').val())/100).toFixed(2); //its convert 10 into 0.10
        discount_nominal = total * discount_percent_dec;
        $('#calc_discount_nominal').val(Helper.toCurrency(discount_nominal));
        grand_total = total - discount_nominal;
        ppn_percent_dec = (parseInt($('#calc_ppn_percent').val())/100).toFixed(2);
        ppn_nominal = grand_total * ppn_percent_dec;
        $('#calc_ppn_nominal').val(Helper.toCurrency(ppn_nominal));
        grand_total = Math.ceil((grand_total + ppn_nominal));
        $('.grand_total').html(Helper.toCurrency(grand_total));
        $('#calc_grand_total').val(grand_total);
        $('#calc_grand_total_display').val('Rp '+Helper.toCurrency(grand_total));
        $('#payment_dp_calculated').val(Helper.toCurrency(grand_total/2));
    },

    calculateRemaining: function() {
        var remaining = parseInt($('#calc_grand_total').val()) - parseInt($('#payment_nominal').val());
        $('#remaining_payment').val(remaining);
    },

    templateNewInputSparepart: function(xx) {
        $('.dynamic_sparepart').append((`
            <tr id="sparepart_row-${xx}">
                <input type="hidden" value="0" name="sparepart_details_id[]">
                <td>
                    <input name="sparepart_status[]" type="hidden" class="form-control sparepart_status-${xx}" data-uniq="${xx}" value="PART" required>
                </td>
                <td>
                    <input name="ms_spareparts_id[]" type="hidden" class="form-control ms_spareparts_id-${xx}" data-uniq="${xx}">
                    <input name="sparepart_code[]" type="text" class="form-control sparepart_code-${xx}" placeholder="Kode Sparepart" data-uniq="${xx}">
                </td>
                <td width="30%">
                    <input name="sparepart_description[]" type="text" class="form-control sparepart_description-${xx}" data-uniq="${xx}">
                </td>
                <td>
                    <input name="sparepart_qty[]" type="text" class="form-control sparepart_qty-${xx} number_only" data-uniq="${xx}">
                </td>
                <td>
                    <input name="sparepart_price[]" type="text" class="form-control sparepart_price-${xx} number_only currency" data-uniq="${xx}">
                </td>
                <td>
                    <input name="sparepart_discount[]" type="text" class="form-control sparepart_discount-${xx} number_only currency" data-uniq="${xx}">
                </td>
                <td>
                    <input name="sub_total_sparepart[]" type="text" class="form-control sub_total_sparepart-${xx} number_only currency" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <div role="group" class="mb-2 btn-group-sm btn-group">
                        <button type="button" sparepart_details_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_sparepart" sparepart_data_id="0"><i class="fa fa-times"></i></button>
                    </div>
                </td>
            </tr>
        `));
    },

    templateNewInputService: function(xx) {
        $('.dynamic_service').append((`
            <tr id="service_row-${xx}">
                <input type="hidden" value="0" name="service_details_id[]">
                <td>
                    <input name="service_status[]" type="hidden" class="form-control service_status-${xx}" data-uniq="${xx}" value="LABOR" required>
                </td>
                <td>
                    <input name="ms_services_id[]" type="hidden" class="form-control ms_services_id-${xx}" data-uniq="${xx}">
                    <input name="service_code[]" type="text" class="form-control service_code-${xx}" placeholder="Kode Service" data-uniq="${xx}">
                </td>
                <td width="30%">
                    <input name="service_description[]" type="text" class="form-control service_description-${xx}" data-uniq="${xx}">
                </td>
                <td>
                    <input name="service_qty[]" type="text" class="form-control service_qty-${xx} number_only" data-uniq="${xx}">
                </td>
                <td>
                    <input name="service_price[]" type="text" class="form-control service_price-${xx}  number_only currency" data-uniq="${xx}">
                </td>
                <td>
                    <input name="service_discount[]" type="text" class="form-control service_discount-${xx} number_only currency" data-uniq="${xx}">
                </td>
                <td>
                    <input name="sub_total_service[]" type="text" class="form-control sub_total_service-${xx} number_only currency" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <div role="group" class="mb-2 btn-group-sm btn-group">
                        <button type="button" service_details_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_service" service_data_id="0"><i class="fa fa-times"></i></button>
                    </div>
                </td>
            </tr>
        `));
    },

    deleteSparepart: function($el) {
        var uniq = $el.attr("data-uniq");
        var details_id = parseInt($el.attr("sparepart_details_data_id"));

        if (details_id != 0) {
            Helper.confirm(function() {
                Axios.delete('/transaction/quotation_orders/destroy_details_data/' + details_id)
                    .then(function(response) {
                        Helper.successNotif('Success Delete');
                        $('#sparepart_row-' + uniq + '').remove();
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
            return false;
        } else {
            $('#sparepart_row-' + uniq + '').remove();
        }
    },

    deleteService: function($el) {
        var uniq = $el.attr("data-uniq");
        var details_id = parseInt($el.attr("service_details_data_id"));

        if (details_id != 0) {
            Helper.confirm(function() {
                Axios.delete('/transaction/quotation_orders/destroy_details_data/' + details_id)
                    .then(function(response) {
                        Helper.successNotif('Success Delete');
                        $('#service_row-' + uniq + '').remove();
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
            return false;
        } else {
            $('#service_row-' + uniq + '').remove();
        }
    },

    selectCode: function() {

        $(document).on('click', 'input[name="sparepart_code[]"]', function() {
            $('#sparepart_modal').modal('show');
            $('#select_sparepart_modal').attr('data-uniq', $(this).attr('data-uniq'));
        });
        $(document).on('click', '#select_sparepart_modal', function() {
            $('#sparepart_modal').modal('hide');
            $('.ms_spareparts_id-' + $(this).attr('data-uniq')).val($(this).attr('data-sparepart-id'));
            $('.sparepart_code-' + $(this).attr('data-uniq')).val($(this).attr('data-sparepart-code'));
            $('.sparepart_description-' + $(this).attr('data-uniq')).val($(this).attr('data-sparepart-description'));
            $('.sparepart_price-' + $(this).attr('data-uniq')).val($(this).attr('data-sparepart-price'));
            ClassApp.hitungTotal();
        });

        $(document).on('click', 'input[name="service_code[]"]', function() {
            console.log('asd');
            $('#service_modal').modal('show');
            $('#select_service_modal').attr('data-uniq', $(this).attr('data-uniq'));
        });
        $(document).on('click', '#select_service_modal', function() {
            $('#service_modal').modal('hide');
            $('.ms_services_id-' + $(this).attr('data-uniq')).val($(this).attr('data-service-id'));
            $('.service_code-' + $(this).attr('data-uniq')).val($(this).attr('data-service-code'));
            $('.service_description-' + $(this).attr('data-uniq')).val($(this).attr('data-service-description'));
            $('.service_price-' + $(this).attr('data-uniq')).val($(this).attr('data-service-price'));
            ClassApp.hitungTotal();
        });
    },

    saveData: function(el, e, id) {
        Helper.unMask('.currency');
        var form = Helper.serializeForm(el);
        var fd = new FormData(el[0]);
        fd.append("_token", "{{ csrf_token() }}");
        $.ajax({
            url:Helper.url('/transaction/quotation_orders/store'),
            type: 'post',
            data: fd,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);

                if (response != 0) {
                    Helper.successNotif(response.msg);
                    window.location.href = Helper.redirectUrl('/transaction/quotation_orders/edit/'+response.data.id);
                }
            },
            error: function(xhr, status, error) {
                handleErrorResponse(error);
            },
        });

        e.preventDefault();
    },
    savePayment: function(el, e, id) {

        // Helper.unMask('.currency');
        ClassApp.calculateRemaining();
        var form = Helper.serializeForm(el);
        var fd = new FormData(el[0]);
        fd.append("_token", "{{ csrf_token() }}");
        $.ajax({
            url:Helper.url('/transaction/quotation_orders/payment/store'),
            type: 'post',
            data: fd,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);

                if (response != 0) {
                    Helper.successNotif(response.msg);
                    window.location.href = Helper.redirectUrl('/transaction/quotation_orders/edit/'+id);
                }
            },
            error: function(xhr, status, error) {
                handleErrorResponse(error);
            },
        });
        e.preventDefault();


    },

    updateData: function(el, e, id) {
        Helper.unMask('.currency');
        var form = Helper.serializeForm(el);
        var fd = new FormData(el[0]);

        $.ajax({
            url:Helper.url('/transaction/quotation_orders/update/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Quotation Orders Detail Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/transaction/quotation_orders/edit/'+id+'');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });

        e.preventDefault();
    },


}

</script>
