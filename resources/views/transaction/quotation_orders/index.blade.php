@extends('layouts.master')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }

    .dataTables_wrapper {
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-size: 10px;
    }

</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <h2>Quotation Orders List</h2>
            </div>
            <div class="btn-actions-pane-right text-capitalize" style="margin-left:150px;">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fr</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                <a href="{{ url('transaction/quotation_orders/create') }}" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-quotation-orders" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>New.Bill</th>
                        <th>Date</th>
                        <th>DP. Date</th>
                        <th>In Date</th>
                        <th>Customer</th>
                        <th>Sub-Total</th>
                        <th>PPN</th>
                        <th>DISC</th>
                        <th>Total</th>
                        <th>DP</th>
                        <th>Invoice</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
     $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            allowBlank: false,
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });

    var tableQO = globalCRUD.datatables({
        url: '/transaction/quotation_orders/datatables',
        selector: '#table-quotation-orders',
        columnsField: [{
                    "className": 'details-control',
                    "defaultContent": ''
                },
                {
                    data: 'bill_number',
                    name: 'bill_number',
                    class: 'text-center',
                    render: function(data, type, full) {
                        return '<a href="'+Helper.url("/transaction/quotation_orders/edit/" + full.id + "")+'"><u>'+data+'</u></a>';
                    }
                },
                {
                    data: 'date',
                    name: 'date',
                    class: 'text-center',
                    render: function(data, type, full) {
                        return moment(data).format("DD-MM-YYYY");
                    }
                },
                'dp_date',
                'in_date',
                'quotation_order_customer.customer_name',
                {
                    render: function(row,type,full) { //subtotal
                        return Helper.toCurrency((full.total_price_sparepart) + parseInt(full.total_price_service));
                    }
                },
                {
                    render: function(row,type,full) { //ppn
                        return full.ppn != null ? Helper.toCurrency(full.ppn) : '-';
                    }
                },
                {
                    render: function(row,type,full) { //discount
                        return full.discount != null ? Helper.toCurrency(full.discount) : '-';
                    }
                },
                {
                    render: function(row,type,full) { //total
                        return full.total != null ? Helper.toCurrency(full.total) : '-';
                    }
                },
                {
                    data: "dp_paid",
                    name: "dp_paid",
                    render: function(row,type,full) {
                        return row != null ? Helper.toCurrency(row) : '-';
                    }
                },
                {
                    data: "inv_paid",
                    name: "inv_paid",
                    render: function(row,type,full) {
                        return row != null ? Helper.toCurrency(row) : '-';
                    }
                },
                {
                    data: "status_payment.name_status",
                    name: "status_payment.name_status",
                    render: function(row,type,full) {
                        if(row != null) {
                            return full.status_payment.status_1+ ' - '+full.status_payment.status_2;
                        } else {
                            return '-';
                        }


                    }
                }
                ],
        actionLink: {

            update: function(row) {
                return "/transaction/quotation_orders/edit/" + row.id;
            },
            delete: function(row) {
                return "/transaction/quotation_orders/delete/" + row.id;
            }
        }
    });

    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }


    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/transaction/quotation_orders/datatables/search' + playload);
            tableQO.table.reloadTable(url);
        }

    })

    function templatex(d) {
        console.log(d)
        template = '';
        template += '<div class="col-md-12">';
        template += '<h5>Sparepart & Service Details</h5>';
        template += '<table class="table" id="example">';
        template += '<tr>';
        template += '<td>NO</td>';
        template += '<td>STATUS</td>';
        template += '<td>KODE</td>';
        template += '<td>NAMA PART/JASA</td>';
        template += '<td>QTY</td>';
        template += '<td>HARGA</td>';
        template += '<td>DISC</td>';
        template += '<td>TOTAL</td>';
        template += '<td>ACTION</td>';
        template += '</tr>';

        _.each(d.quotation_order_servicepart_detail, function(i, key) {

            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.status + '</td>';
            template += '<td>' + (i.sparepart_code != null ? i.sparepart_code : i.service_code) + '</td>';
            template += '<td>' + (i.sparepart_name != null ? i.sparepart_name : i.service_name) + '</td>';
            template += '<td>' + i.qty + '</td>';
            template += '<td>' + Helper.toCurrency(i.price) + '</td>';
            template += '<td>' + Helper.toCurrency(i.discount) + '</td>';
            template += '<td>' + Helper.toCurrency(i.total) + '</td>';
            template += '<td><a title="Delete Part/Jasa" data-id="' + i.id + '" class="btn-hover-shine btn btn-sm btn-danger detail_delete"><i class="fa fa-trash"></i></a></td>';

            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';

        return template;
    }


    var table = $('#table-quotation-orders').DataTable();
    $('#table-quotation-orders tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(templatex(row.data())).show();
            $(row.child()).addClass('smalltable');
            tr.addClass('shown');
        }
    });

    // delete detail
    $(document).on('click', ".detail_delete", function() {
        var id = $(this).attr("data-id");
        globalCRUD.delete(Helper.url('/transaction/quotation_orders/destroy_details_data/'+id))
        // tableQO.table.reloadTable();
    });
    </script>

@endsection
