<head>
    <title>Print | Quotation</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/css-loader/3.3.3/css-loader.css">
    <link rel="stylesheet"
        href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/font-icons/entypo/css/entypo.css">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/neon-fonts.css">
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/bootstrap.css">
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/neon-core.css">
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/neon-theme.css">
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/neon-forms.css">
    <link rel="stylesheet" href="https://csadpv6.0.astech.co.id/neon/backend/assets/css/custom.css">
    <style>
        tr {
            font-size: 12px;
        }
        td {
            font-size: 12px !important;
        }
    </style>
</head>
<?php
    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
	}

    function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut(ceil($nilai)));
		} else {
			$hasil = trim(penyebut(ceil($nilai)));
		}
		return $hasil;
	}
?>
<body class="page-body">
    <div class="page-container">
        <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
        <div class="main-content">
            <header class="header b-b b-light hidden-print">
                <div class="row">
                    <div class="col-sm-8">
                        <strong>QUOTATION</strong>
                    </div>
                    <div class="col-sm-4">
                        <button href="#" class="btn btn-sm btn-info pull-right" onclick="window.print();">Print
                            Data</button>
                    </div>
                </div>
            </header>
            <br class="hidden-print">
            <div class="invoice">
                <div class="row">
                    <div class="col-sm-8 text-center">
                        <h3><b>QUOTATION</b></h3>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{ url('storage/brand-images/'.$maindata->brand->logo.'') }}" width="130" height="25"
                            alt="SAMSUNG" longdesc="SAMSUNG Logo">
                    </div>
                </div>&nbsp;
                <font size="-1">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="col-xs-8"><b>SAMSUNG CARE</b></div><br>
                            <div class="col-xs-8"><b>GEDUNG SSK LT.3 JL. DAAN MOGOT KM.11</b></div><br>
                            <div class="col-xs-8"><b>021-543671231/</b></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-xs-4">Print</div>
                            <div class="col-xs-8">: {{ \Carbon\Carbon::now()->format('j M Y h:i:s A') }}</div><br>
                            <div class="col-xs-4">Number</div>
                            <div class="col-xs-8">: xxx-001-xxx</div><br>
                            <div class="col-xs-4">Date</div>
                            <div class="col-xs-8">: {{ $maindata->date->format('d-m-Y') }}</div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="col-xs-2"><b>Bill To </b> :</div><br>
                            <div class="col-xs-2">Nama</div>
                            <div class="col-xs-9">: {{ $maindata->quotation_order_customer->customer_name }}</div>
                            <div class="col-xs-2">Alamat</div>
                            <div class="col-xs-9">: {{ $maindata->quotation_order_customer->address }} </div>
                            <div class="col-xs-2">No.Telp</div>
                            <div class="col-xs-9">: {{ $maindata->quotation_order_customer->handphone }}</div>
                            <div class="col-xs-2">Email</div>
                            <div class="col-xs-9">: {{ $maindata->quotation_order_customer->email }}</div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-xs-2"></div><br>
                            <div class="col-xs-4">No.Bill </div>
                            <div class="col-xs-8">: {{ $maindata->bill_number }}</div><br>
                            <div class="col-xs-4">Type Product </div>
                            <div class="col-xs-8">: {{ $maindata->type_product }}</div><br>
                            <div class="col-xs-4">No.Imei</div>
                            <div class="col-xs-8">: {{ $maindata->imei_number }}</div>
                        </div>
                    </div>
                </font>
                &nbsp;
                <font size="5">
                    <table class="table table-bordered m-b-none" width="100%">
                        <thead>
                            <tr>
                                <th width="35">NO</th>
                                <th>PART/SERVICE CODE</th>
                                <th>PART NAME</th>
                                <th width="60" class="text-center">QTY</th>
                                <th width="100" class="text-right">PRICE</th>
                                <th width="110" class="text-right">TOTAL</th>
                            </tr>
                        </thead>
                        <tbody class="fontdtl">
                            @foreach($maindata->quotation_order_servicepart_detail as $key => $data)
                                <tr>

                                    <td class="text-center">{{ ($key+1) }}</td>
                                    <td>{{ $data->sparepart_code != null ? $data->sparepart_code : $data->service_code; }}</td>
                                    <td>{{ $data->sparepart_name != null ? $data->sparepart_name : $data->service_cname; }}</td>
                                    <td class="text-center">{{ number_format($data->qty, 0, '.', '.'); }}</td>
                                    <td class="text-right">{{ number_format($data->price, 0, '.', '.'); }}</td>
                                    <td class="text-right">Rp. {{ number_format($data->total, 0, '.', '.'); }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="5" class="text-right"><b>SUB TOTAL [+]</b></td>
                                <td class="text-right">{{ number_format(($maindata->total_price_sparepart + $maindata->total_price_service), 0, '.', '.'); }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>DISKON {{ $maindata->discount_percent }}% [+]</b></td>
                                <td class="text-right">{{ number_format($maindata->discount, 0, '.', '.'); }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>PPN {{ $maindata->ppn_percent }}% [+]</b></td>
                                <td class="text-right">{{ number_format($maindata->ppn, 0, '.', '.'); }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>TOTAL [=]</b></td>
                                <td class="text-right"><b>{{ number_format($maindata->total, 0, '.', '.'); }}</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">## &nbsp;{{ terbilang($maindata->total) }}&nbsp; ##</td>
                            </tr>
                        </tbody>
                    </table>
                </font>
                <p>
                    <font size="1">
                        <span>Perbaikan akan dilakukann setelah menerima pembayaran DP (50%) sebesar Rp.
                            <b>{{ number_format((ceil($maindata->total / 2)), 0, '.', '.') }}</b></span><br>

                        <span>Pembayaran dapat dilakukan melalui :</span><br>
                        <span><b>1. Cash / Tunai di Service Center</b></span><br>
                        <span><b>2. Transfer</b></span><br>
                    </font>
                </p>
                <div class="row">
                    <font size="1">
                        <div class="col-xs-2">&nbsp; &nbsp; NAMA BANK</div>
                        <div class="col-xs-8">: BANK CENTRAL ASIA</div><br>
                        <div class="col-xs-2">&nbsp; &nbsp; NO.REKENING</div>
                        <div class="col-xs-8">: 1234567890</div><br>
                        <div class="col-xs-2">&nbsp; &nbsp; PEMILIK REKENING</div>
                        <div class="col-xs-8">: PT.ANEKA DAYA PRIMA</div>
                    </font>
                </div>
                <font size="1">
                </font>
                <p></p>&nbsp;
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <p class="m-t m-b"><span class="label bg-primary">CUSTOMER</span><br>
                            #Date : {{ \Carbon\Carbon::now()->format('d-m-Y'); }}</p><br><br>
                        {{-- <img src="" alt="..." class="logo-default" width="100px" height="100"><br>&nbsp; --}}
                        <p></p>
                        <h5>(&nbsp;{{ $maindata->quotation_order_customer->customer_name }}&nbsp;)</h5>
                        <p></p>
                    </div>
                    <div class="col-xs-6 text-center">
                        <p class="m-t m-b"><span class="label bg-primary">CS / ADMIN</span></p>
                        <img src="{{ url('images/cs_stempel.png') }}" width="120px" height="100px"
                            alt="Paid">
                        <p></p>
                        <h5>(&nbsp;ACHMAD ICHSAN&nbsp;)</h5>
                        <p></p>
                    </div>
                </div>
                <hr>
            </div>
        </div><!-- Footer -->

    </div>
</body>
