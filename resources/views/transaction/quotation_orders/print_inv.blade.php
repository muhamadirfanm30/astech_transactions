<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Print | QO Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    {{-- <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/Ionicons/css/ionicons.min.css"> --}}
    <!-- Theme style -->
    <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE//dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style>
            hr {
                background-color: #eee;
                border: 0 none;
                color: #eee;
                height: 1px;
            }
            tr {
                font-size: 12px;
            }
            td {
                font-size: 12px !important;
            }
        </style>
</head>

<?php
    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
	}

    function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut(ceil($nilai)));
		} else {
			$hasil = trim(penyebut(ceil($nilai)));
		}
		return $hasil;
	}
?>

<body onload="window.print()">
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-user"></i> INVOICE - ASTECH CASHIER
                        <small class="pull-right">Date: {{ $maindata->date->format('d/m/Y') }}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>SAMSUNG CARE</strong><br>
                        GEDUNG SSK LT.3 JL. DAAN MOGOT KM.11<br>
                        021-543671231<br><br>
                    </address>
                </div>

                <div class="col-sm-4 invoice-col">
                    <address>
                        <img src="{{ url('storage/brand-images/'.$maindata->brand->logo.'') }}" width="130" height="25"
                            alt="" longdesc="">
                    </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>INVOICE #001111XXX</b><br>
                    <br>
                    <b>Print:</b> {{ \Carbon\Carbon::now()->format('j M Y h:i:s A') }}<br>
                    <b>Date:</b> {{ $maindata->date->format('d-m-Y') }}
                </div>
                <!-- /.col -->
            </div>
            <hr>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-8 invoice-col">
                    <address>
                        <strong>BILL TO: </strong><br>
                        <b>Nama:</b> {{ $maindata->quotation_order_customer->customer_name }}<br>
                        <b>Alamat:</b> {{  $maindata->quotation_order_customer->address }}<br>
                        <b>No. Telp:</b> {{  $maindata->quotation_order_customer->handphone }}<br>
                        <b>Email:</b> {{  $maindata->quotation_order_customer->email }}
                    </address>
                </div>

                <div class="col-sm-4 invoice-col">
                    <address>

                    </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong></strong><br>
                        <b>No. Bill:</b> {{ $maindata->bill_number}}<br>
                        <b>Type Product:</b> {{ $maindata->type_product }}<br>
                        <b>No. Imei:</b> {{  $maindata->imei_number }}<br>
                    </address>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="35">NO</th>
                                <th>PART/SERVICE CODE</th>
                                <th>PART NAME</th>
                                <th width="60" class="text-center">QTY</th>
                                <th width="100" class="text-right">PRICE</th>
                                <th width="110" class="text-right">TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($maindata->quotation_order_servicepart_detail as $key => $data)
                                <tr>
                                    <td class="text-center">{{ ($key+1) }}</td>
                                    <td>{{ $data->sparepart_code != null ? $data->sparepart_code : $data->service_code; }}</td>
                                    <td>{{ $data->sparepart_name != null ? $data->sparepart_name : $data->service_cname; }}</td>
                                    <td class="text-center">{{ number_format($data->qty, 0, '.', '.'); }}</td>
                                    <td class="text-right">{{ number_format($data->price, 0, '.', '.'); }}</td>
                                    <td class="text-right">Rp. {{ number_format($data->total, 0, '.', '.'); }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="lead">Payment Methods:</p>
                    <span>Pembayaran dapat dilakukan melalui :</span><br>
                    <span><b>1. Cash / Tunai di Service Center</b></span><br>
                    <span><b>2. Transfer</b></span><br>
                    <span><b>NAMA BANK: BANK CENTRAL ASIA</b></span><br>
                    <span><b>NO.REKENING: 1234567890</b></span><br>
                    <span><b>PEMILIK REKENING: PT.ANEKA DAYA PRIMA</b></span><br><br>
                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        ## &nbsp;{{ terbilang($maindata->total) }}&nbsp; ##
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <p class="lead">Amount Due {{ $maindata->date->format('d-m-Y') }}</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="width:50%">SUBTOTAL:</th>
                                    <td>{{ number_format(($maindata->total_price_sparepart + $maindata->total_price_service), 0, '.', '.'); }}</td>
                                </tr>
                                <tr>
                                    <th>PPN</th>
                                    <td>{{ number_format($maindata->ppn, 0, '.', '.'); }}</td>
                                </tr>
                                <tr>
                                    <th>DISKON:</th>
                                    <td>{{ number_format($maindata->discount, 0, '.', '.'); }}</td>
                                </tr>
                                <tr>
                                    <th>TOTAL:</th>
                                    <td><b>Rp. {{ number_format($maindata->total, 0, '.', '.'); }}</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- ./wrapper -->


</body>
