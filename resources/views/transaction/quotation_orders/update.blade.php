@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="card-title">
                                    Update Quotation Orders
                                </h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ url('/transaction/quotation_orders') }}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-back"></i>&nbsp;
                                    Back
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="col-md-12">
                <div class="card">
                    <div class="card-body card-outline card-info">
                        <div class="row text-right">
                            <div class="col-md-6 text-right">
                                <h2><b>TOTAL Rp. </b></h2>
                            </div>
                            <div class="col-md-6 text-left">
                                <b>
                                    <h2 class="text-success grand_total" >{{ $maindata->total }}</h2>
                                </b>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <form id="form-data" style="display: contents;" autocomplete="off">
                @csrf
                <input type="hidden" name="id" value="{{ $maindata->id }}">
                <section class="col-md-6">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Info</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Date*</label>
                                        <div class="input-group">
                                            <input name="date" type="text" class="form-control date_format" value="{{ $maindata->date }}" required readonly >
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Type Transaction*</label>
                                        <select name="type_transaction" class="form-control required">
                                            <option value="service" <?php if($maindata->type_transaction == 'service') { echo 'selected'; } ?> >service</option>
                                            <option value="sparepart" <?php if($maindata->type_transaction == 'sparepart') { echo 'selected'; } ?> >sparepart</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Brand *</label>
                                        <select name="ms_brands_id" id="ms_brands_id" class="form-control" required>
                                            <option value="{{ $maindata->ms_brands_id }}" selected>{{ $maindata->brand->name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Type Product *</label>
                                        <select name="type_product" id="type_product" class="form-control" required>
                                            <option value="{{ $maindata->type_product }}" selected>{{ $maindata->type_product }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">RO / BILL *</label>
                                        <input type="text" name="bill_number" class="form-control" value="{{ $maindata->bill_number }}" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">SO NUMBER *</label>
                                        <input type="text" name="so_number" class="form-control" value="{{ $maindata->so_number }}" required>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="">IMEI/SN</label>
                                        <input type="text" name="imei_number" class="form-control" value="{{ $maindata->imei_number }}" required>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="">BILL TO</label>
                                        <select name="bill_to" class="form-control required">
                                            <option value="{{ $maindata->bill_to }}" selected>{{ $maindata->bill_to }}</option>
                                            <option value="END USER">END USER</option>
                                            <option value="PRINCIPAL">PRINCIPAL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Keterangan *</label>
                                        <textarea name="description" class="form-control" rows="4">{{ $maindata->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="col-md-6">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Customer</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Customer Name *</label>
                                        <input type="text" name="customer_name" class="form-control" value="{{ $maindata->quotation_order_customer->customer_name }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Handphone *</label>
                                        <input type="text" name="handphone" class="form-control" value="{{ $maindata->quotation_order_customer->handphone }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email *</label>
                                        <input type="text" name="email" class="form-control" value="{{ $maindata->quotation_order_customer->email }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Alamat *</label>
                                        <textarea name="address" class="form-control" rows="4" required>{{ $maindata->quotation_order_customer->address }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="col-md-12" id="section_sparepart">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Sparepart</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="btn-actions-pane-left text-right">
                                <button type="button" class="btn btn-success btn_add_sparepart"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode</th>
                                        <th>Desc Sparepart</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Discount</th>
                                        <th>Sub Total</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="dynamic_sparepart">
                                    <input type="hidden" value="{{ count($maindata->sparepart_detail) }}" id="count_sparepart_details_data" />
                                    @foreach($maindata->sparepart_detail as $key=> $sparepart)
                                        <?php $uniq = $key+1; ?>
                                        <tr class="dynamic_sparepart_details_content" id="sparepart_row_{{ $uniq }}">
                                            <input type="hidden" value="{{ $sparepart->id }}" name="sparepart_details_id[]" />
                                            <td><span class="counter">{{ $uniq }}</span></td>
                                            <td>
                                                <input name="sparepart_status[]" type="hidden"
                                                    class="form-control sparepart_status-{{ $uniq }}" data-uniq="{{ $uniq }}" value="PART">
                                                <input name="ms_spareparts_id[]" type="hidden"
                                                    class="form-control ms_spareparts_id-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $sparepart->ms_spareparts_id }}">
                                                <input name="sparepart_code[]" type="text" placeholder="Kode Sparepart"
                                                    class="form-control sparepart_code-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $sparepart->sparepart_code }}" required
                                                    style="width:100%">
                                            </td>
                                            <td width="30%">
                                                <input name="sparepart_description[]" type="text"
                                                    placeholder="Description Sparepart"
                                                    class="form-control sparepart_description-{{ $uniq }}" data-uniq="{{ $uniq }}"
                                                    style="width:100%" value="{{ $sparepart->sparepart_name }}" readonly required>
                                            </td>
                                            <td>
                                                <input name="sparepart_qty[]" type="text" placeholder="Quantity"
                                                    class="form-control sparepart_qty-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $sparepart->qty }}" readonly required>
                                            </td>
                                            <td>
                                                <input name="sparepart_price[]" type="text" placeholder="Price"
                                                    class="form-control sparepart_price-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $sparepart->price }}" readonly required>
                                            </td>
                                            <td>
                                                <input name="sparepart_discount[]" type="text" placeholder="Discount"
                                                    class="form-control sparepart_discount-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $sparepart->discount }}"
                                                    readonly >
                                            </td>
                                            <td>
                                                <input name="sub_total_sparepart[]" type="text"
                                                    class="form-control sub_total_sparepart-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $sparepart->total }}"
                                                    readonly>
                                            </td>
                                            <td>
                                                <button type="button" sparepart_details_data_id="{{ $sparepart->id }}" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_sparepart"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL SPAREPART</b></td>
                                        <td><input name="total_qty_sparepart" type="text" class="form-control"
                                                id="total_qty_sparepart" readonly></td>
                                        <td></td>
                                        <td></td>
                                        <td><input name="total_sparepart" type="text" class="form-control" id="total_sparepart"
                                                readonly></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <section class="col-md-12" id="section_service">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Service</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="btn-actions-pane-left text-right">
                                <button type="button" class="btn btn-success btn_add_service"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode</th>
                                        <th>Desc Service</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Discount</th>
                                        <th>Sub Total</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="dynamic_service">
                                    <input type="hidden" value="{{ count($maindata->service_detail) }}" id="count_service_details_data" />
                                    @foreach($maindata->service_detail as $key=> $service)
                                        <?php $uniq = $key+1; ?>
                                        <tr class="dynamic_service_details_content" id="service_row_{{ $uniq }}">
                                            <input type="hidden" value="{{ $service->id }}" name="service_details_id[]" />
                                            <td><span class="counter">1</span></td>
                                            <td>
                                                <input name="service_status[]" type="hidden" class="form-control service_status-{{ $uniq }}"
                                                    data-uniq="{{ $uniq }}" value="LABOR">
                                                <input name="ms_services_id[]" type="hidden" class="form-control ms_services_id-{{ $uniq }}"
                                                    data-uniq="{{ $uniq }}"  value="{{ $service->ms_services_id }}">
                                                <input name="service_code[]" type="text" placeholder="Kode Service"
                                                    class="form-control service_code-{{ $uniq }}" data-uniq="{{ $uniq }}"
                                                    style="width:100%" value="{{ $service->service_code }}">
                                            </td>
                                            <td width="30%">
                                                <input name="service_description[]" type="text" placeholder="Description Sparepart"
                                                    class="form-control service_description-{{ $uniq }}" data-uniq="{{ $uniq }}" style="width:100%" value="{{ $service->service_name }}" readonly>
                                            </td>
                                            <td>
                                                <input name="service_qty[]" type="text" placeholder="Quantity"
                                                    class="form-control service_qty-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $service->qty }}" readonly>
                                            </td>
                                            <td>
                                                <input name="service_price[]" type="text" placeholder="Price"
                                                    class="form-control service_price-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $service->price }}" readonly>
                                            </td>
                                            <td>
                                                <input name="service_discount[]" type="text" placeholder="Discount"
                                                    class="form-control service_discount-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $service->discount }}" readonly>
                                            </td>
                                            <td>
                                                <input name="sub_total_service[]" type="text"
                                                    class="form-control sub_total_service-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ $service->total }}" readonly>
                                            </td>
                                            <td>
                                                <button type="button" service_details_data_id="{{ $service->id }}" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_service"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>TOTAL SERVICE</b></td>
                                        <td><input name="total_qty_service" type="text" class="form-control"
                                                id="total_qty_service" readonly></td>
                                        <td></td>
                                        <td></td>
                                        <td><input name="total_service" type="text" class="form-control" id="total_service"
                                                readonly></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <section class="col-md-12" id="section_price_calculation">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Price Calculation</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group row">
                                            <input type="hidden" id="status_payments_id" name="status_payments_id"
                                            class="" value="{{ $maindata->status_payments_id }}">
                                            <label class="col-sm-1 control-label text-right text-danger">DP</label>
                                            <div class="col-sm-1">
                                                <input type="text" id="payment_dp_default" class="form-control text-center" value="50" readonly="">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" id="payment_dp_calculated" name="payment_dp_calculated"
                                                    class="form-control text-right" readonly="">
                                            </div>
                                            <label class="col-sm-1 control-label no-padding-right text-danger">STATUS</label>
                                            <div class="col-sm-2">
                                                <input type="text" id="payment_status_1" name="payment_status_1"
                                                    class="form-control text-center" readonly="" value="{{ $maindata->status_payment->status_1 }}">
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" id="payment_status_2" name="payment_status_2"
                                                    class="form-control text-center" readonly="" value="{{ $maindata->status_payment->status_2 }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-6 control-label text-right text-danger"></label>
                                            <div class="col-sm-4">
                                                <input type="text" id="status_description" class="form-control text-center" value="{{ $maindata->status_payment->description }}" readonly="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-1 control-label text-right"><b>DP PAID</b></label>
                                            <div class="col-sm-4">
                                                <input type="text" id="payment_dp_paid" name="payment_dp_paid"
                                                    class="form-control text-right" placeholder="Dp Amount" readonly="" value="{{ \App\Helpers\thousandSparator($maindata->dp_paid) }}">
                                            </div>
                                            <label class="col-sm-1 control-label"><b>INV.PAID</b></label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control text-right" name="payment_invoice_paid"
                                                    id="payment_invoice_paid" placeholder="Invoice Amount" readonly="" value="{{ \App\Helpers\thousandSparator($maindata->inv_paid) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">TOTAL Sparepart:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="calc_total_sparepart" name="calc_total_sparepart"
                                                    class="form-control text-right currency" readonly>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">&nbsp;&nbsp;TOTAL Service:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="calc_total_service" name="calc_total_service"
                                                    class="form-control text-right currency" readonly>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-2 control-label">DISC :</label>
                                            <div class="col-sm-4">
                                                <input type="number" id="calc_discount_percent" min="0" max="100"
                                                    name="calc_discount_percent" class="form-control text-center" value={{ $maindata->discount_percent != null ? $maindata->discount_percent : 0 }}>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" id="calc_discount_nominal" name="calc_discount_nominal"
                                                    class="form-control text-right currency" value={{ $maindata->discount != null ? $maindata->discount : 0 }}>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-2 control-label">PPN :</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="calc_ppn_percent" name="calc_ppn_percent"
                                                    class="form-control text-center" value={{ $maindata->ppn_percent != null ? $maindata->ppn_percent : 0 }} readonly>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" id="calc_ppn_nominal" name="calc_ppn_nominal"
                                                    class="form-control text-right currency" readonly="" value={{ $maindata->ppn != null ? $maindata->ppn : 0 }}>
                                            </div>
                                        </div>
                                        <div class="form-group row text-right">
                                            <label class="col-sm-7 control-label no-padding-right">GRAND TOTAL :</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control text-right" name="calc_grand_total_display"
                                                    id="calc_grand_total_display" readonly="">
                                                <input type="hidden" class="form-control text-right" name="calc_grand_total"
                                                    id="calc_grand_total" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

                <section class="col-md-12" id="section_payment">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold">Payment {!! $maindata->total == $maindata->remaining ? "<span class='text-danger'>(haven't paid yet)</span>" : ($maindata->remaining == 0 ? "<span class='text-success'>(LUNAS)</span>" : "") !!}</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">PAYMENT:</label>
                                            <div class="col-sm-9">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <button type="button" class="btn btn-warning"
                                                            id="open_payment_details_modal"><i
                                                                class="fa fa-search" title="DATA PAYMENT"></i></button>
                                                    </div>
                                                    <!-- /btn-group -->
                                                    <input type="text" class="form-control text-right" id="payment" value="{{ \App\Helpers\thousandSparator($maindata->dp_paid == 0 ? $maindata->inv_paid : $maindata->dp_paid); }}"
                                                        readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right form-group row justify-content-end">
                                            <label class="col-sm-3 control-label">REMAINING:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control text-right text-danger currency"
                                                    id="remaining" readonly value="{{ \App\Helpers\thousandSparator($maindata->remaining) }}">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row justify-content-end">
                                        @if($maindata->remaining != 0 )
                                        <button type="submit" class="btn btn-success mr-2" id="save_data"><i
                                            class="fa fa-save"></i> Save Data</button>
                                        <button type="button" class="btn btn-primary mr-2" id="open_payment_modal"><i
                                            class="fa fa-print"></i> Payment</button>
                                        @else
                                        <a type="button" class="btn btn-secondary mr-2" id="cetak_inv" type="button" href="{{ url('transaction/quotation_orders/print_inv/'.$maindata->id.'') }}" target="_blank"><i
                                            class=" fa fa-calculator"></i> Cetak Invoice</a>
                                        @endif
                                        <a type="button" href="{{ url('transaction/quotation_orders/print_qo/'.$maindata->id.'') }}" target="_blank" class="btn btn-secondary mr-2" id="cetak_qo"><i
                                            class="fa fa-print"></i> Cetak Quotation Order</a>
                                        <a href="{{ url('/transaction/quotation_orders') }}" class="btn btn-danger mr-2" id="back" target="_blank"><i class="fa fa-reply"></i>
                                            Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </form>
        </div>
    </section>


    <!-- Modal Find Sparepart-->
    <div class="modal fade " id="sparepart_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Sparepart</h5>
                </div>
                <div class="modal-body">
                    <table id="table-find-sparepart" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode Sparepart</th>
                                <th>Deskripsi</th>
                                <th>UOM</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_sparepart_modal"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="select_sparepart_modal" data-dismiss="modal"
                        data-uniq="xxx" data-sparepart-id="" data-sparepart-code="" data-sparepart-description=""
                        data-sparepart-price="">Select</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Find Service-->
    <div class="modal fade " id="service_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Select Service</h5>
                </div>
                <div class="modal-body">
                    <table id="table-find-service" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode Sparepart</th>
                                <th>SVC Type</th>
                                <th>Defect Type</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_service_modal"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="select_service_modal" data-dismiss="modal"
                        data-uniq="xxx" data-service-id="" data-service-code="" data-service-description=""
                        data-service-price="">Select</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Payment Details-->
    <div class="modal fade " id="payment_details_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Payment</h5>
                </div>
                <div class="modal-body">
                    <table id="table-payment" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Type</th>
                                <th>Tanggal</th>
                                <th>Nominal</th>
                                <th>Status</th>
                                <th>Del</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_payment_details_modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Payment -->
    <div class="modal fade " id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-default" id="titlePayment" style="display: block;">
                    <div class="row">
                        <div class="col-sm-8 text-right align-self-center">
                            <font size="6">TOTAL (sisa bayar)  Rp.</font>
                        </div>
                        <div class="col-sm-4 text-left align-self-center">
                            <font size="8" class="text-danger "><span class="remaining" id="sisa_bayar">{{ \App\Helpers\thousandSparator($maindata->remaining) }}</span></font>
                        </div>
                    </div>
                </div>
                <form class="form-vertical" name="form-payment" id="form-payment">
                    @csrf
                <input type="hidden" name="remaining" class="currency" id="remaining_payment" value="{{ $maindata->remaining }}">
                <input type="hidden" name="id" value="{{ $maindata->id }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label"><b>PAYMENT VIA</b></label>
                                <div class="controls">
                                    <select name="ms_payments_id" id="ms_payments_id" class="form-control"></select>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label"><b>TOTAL INVOICE</b></label>
                                <div class="controls">
                                    <input type="text" readonly="" class="form-control text-right total_invoice" value="{{ \App\Helpers\thousandSparator($maindata->total) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="pay-method" style="display: none;">
                        <div class="form-group">
                            <label class="control-label">NO.KARTU :</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="card_number" id="card_number"
                                    placeholder="Card Number" maxlength="35">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">NAMA BANK :</label>
                            <div class="controls">
                                <input list="lstbank" class="form-control" name="bank_name" id="bank_name"
                                    placeholder="Bank Name" maxlength="75">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">KODE APPROVAL :</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="approval_code" id="approval_code"
                                    placeholder="Apporval Code" maxlength="35">
                            </div>
                        </div>
                    </div>
                    <div id="trn-method" style="display: none;">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">TGL. TRANSFER</label>
                                    <div class="input-group">
                                        <input name="transfer_date" type="text" class="form-control date_format" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" required readonly >
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">TRANSFER A/N</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="transfer_name" id="transfer_name"
                                            placeholder="Transfer Name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="panel-heading text-right">
                        <div class="h1 font-bold ">BAYAR Rp.
                            <input type="text" id="payment_nominal" name="payment_nominal" class="text-right number_only">&nbsp;
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close_payment_modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="save_payment">Save</button>
                </div>

                </form>
            </div>
        </div>
    </div>


    @endsection

    @section('script')
        <style>
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {

                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance: textfield;
                /* Firefox */
            }

        </style>

        @include('transaction.quotation_orders._main_js')
        @include('transaction.quotation_orders._sparepart_js')
        @include('transaction.quotation_orders._service_js')
        <script>
             var id = $('input[name="id"]').val();
            //   Helper.currency('.currency');
            $(function() {
                Helper.dateFormat('.date_format');

                ClassApp.selectCode();
                ClassApp.hitungTotal();
                $("#calc_discount_percent").change(function() {
                    var max = parseInt($(this).attr('max'));
                    var min = parseInt($(this).attr('min'));
                    if ($(this).val() > max) {
                        $(this).val(max);
                    } else if ($(this).val() < min) {
                        $(this).val(min);
                    }
                });

            });

            $(document).on('click', '#open_payment_details_modal', function() {
                $('#payment_details_modal').modal('show');
            });
            $(document).on('click', '#close_payment_details_modal', function() {
                $('#payment_details_modal').modal('hide');
            });
            $(document).on('click', '#open_payment_modal', function() {
                $('#payment_modal').modal('show');
            });
            $(document).on('click', '#close_payment_modal', function() {
                $('#payment_modal').modal('hide');
            });

            $(document).on('change', 'select[name="type_transaction"]', function() {
                console.log('asdas');
                if ($(this).val() === 'sparepart') {
                    $('#section_service').hide();
                } else {
                    $('#section_sparepart').show();
                    $('#section_service').show();
                }
            })

            globalCRUD.select2Tags('#type_product', '/transaction/quotation_orders/type_product/select2', function(item) {
                return {
                    id: item.type_product,
                    text: item.type_product,
                }
            });

            globalCRUD.select2Tags('#ms_payments_id', '/master/payments/select2', function(item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });
            $('#ms_payments_id').on('select2:select', function (e) {
                if( e.params.data.id == 1 ){
                    $('#pay-method').hide();
                    $('#trn-method').hide();
                } else if(e.params.data.id == 6) {
                    $('#trn-method').show();
                    $('#pay-method').hide();
                } else if(e.params.data.id != 6){
                    $('#trn-method').hide();
                    $('#pay-method').show();
                }
            });

            globalCRUD.select2('#ms_brands_id', '/master/brands/select2', function(item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });

            $("#form-data").submit(function(e) {
                ClassApp.updateData($(this), e, id);
            });

            $("#form-payment").submit(function(e) {
                ClassApp.savePayment($(this), e, id);
            });

            $(document).on('keyup', 'input[name="calc_discount_percent"]', function() {
                ClassApp.setCalcDiscountPercent($(this).attr('data-uniq'));
                ClassApp.hitungTotal();
            });
            $(document).on('keyup', 'input[name="calc_discount_nominal"]', function() {
                ClassApp.setCalcDiscountPercent($(this).attr('data-uniq'));
                ClassApp.hitungTotal();
            });



            var tbl_payment = $('#table-payment').DataTable({
                ajax: {
                    url: Helper.url('/transaction/quotation_orders/payment_details/datatables/'+id+''),
                    type: "get",
                    error: function() {},
                    complete: function() {},
                },
                selector: '#table-payment',
                dom: 't',
                columns: [{
                        data: "DT_RowIndex",
                        name: "DT_RowIndex",
                        sortable: false,
                    },
                    {
                        data: 'payment.name',
                        name: 'payment.name',
                        class: 'text-danger'
                    },
                    {
                        data: 'date',
                        name: 'date',
                        class: 'text-center',
                        render: function(data, type, full) {
                            return moment(data).format("DD-MM-YYYY");
                        }
                    },
                    {
                        data: 'nominal',
                        name: 'nominal',
                        render: function(data, type, full) {
                            return Helper.toCurrency(data);
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: "id",
                        name: "id",
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full) {
                            return "<button  class='btn btn-danger btn-sm' onclick='deleteData" + full.id +
                                "'><i class='fa fa-trash'></i></button>";
                        }
                    }
                ]
            });

            // payment
            $(document).on('keyup', 'input[name="payment_nominal"]', function() {
                $(this).removeClass('is-invalid');
                var remaining = parseInt($('#remaining').val().split('.').join(""));
                if($(this).val() > remaining) {
                    $('.service_discount-' + $(this).attr('data-uniq') +'').addClass('is-invalid');
                    $(this).val(remaining);
                    Helper.errorNotif('Nilai Bayar tidak boleh lebih besar dari sisa bayar');
                }
            });

        </script>
    @endsection
