@extends('layouts.master')
@section('content')


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">
                                List Permission 
                            </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{url('permission/create')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>&nbsp;
                                Add Permission
                            </a>
                        </div>
                    </div>
                   
                </div>
                <div class="card-body">
                    <table id="example" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name </th>
                                <th>Group Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('script')
<script>
    globalCRUD.datatables({
        url: '/permission/datatables',
        selector: '#example',
        columnsField: ['DT_RowIndex', 'name', 'groups'],
        actionLink: {
            update: function(row) {
                return "/permission/update/" + row.id;
            },
            delete: function(row) {
                return "/permission/" + row.id;
            }
        }
    })
</script>
@endsection
