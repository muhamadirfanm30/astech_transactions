@extends('layouts.master')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="card-title">
                                Create Permissions
                            </h3>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ url('permission/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <form action="{{ route('permission.store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="col-md-12">
                            <div data-role="dynamic-fields">
                                <div class="form-inline">
                                    <div class="col-md-5" style="margin-bottom: 19px">
                                        <div class="form-group" >
                                            <label>Permission Name</label>
                                            <input type="text" name="name[]" class="form-control" id="field-name" placeholder="Permission Name" style="width:100%" required>
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="margin-bottom: 19px">
                                        <div class="form-group" >
                                            <label>Permission Group</label>
                                            <input type="text" name="groups[]" class="form-control" id="field-name" placeholder="Permission Group" style="width:100%" required>
                                        </div>
                                    </div>
                                    <div class="col-md-1" style="margin-bottom: 19px">
                                        <label for="" style="color:white">add btn</label>
                                        <button class="btn btn-danger btn-sm" data-role="remove">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <button class="btn btn-primary btn-sm" data-role="add">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </div><br><br><br>
                                </div>  <!-- /div.form-inline -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<style>
    .entry:not(:first-of-type)
        [data-role="dynamic-fields"] > .form-inline + .form-inline {
        margin-top: 0.5em;
    }

    [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
        display: none;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
        display: inline-block;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
        display: none;
    }
</style>
@endsection
@section('script')
<script>
    $(function() {
        // Remove button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
            function(e) {
                e.preventDefault();
                $(this).closest('.form-inline').remove();
            }
        );
        // Add button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
            function(e) {
                e.preventDefault();
                var container = $(this).closest('[data-role="dynamic-fields"]');
                new_field_group = container.children().filter('.form-inline:first-child').clone();
                new_field_group.find('input').each(function(){
                    $(this).val('');
                });
                container.append(new_field_group);
            }
        );
    });
</script>
@endsection
