<head>
    <title>User System | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title">
                                        List Users 
                                    </h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('/utility/list-user/create')}}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i>&nbsp;
                                        Add Users
                                    </a>
                                </div>
                            </div>
                           
                        </div>
                        <div class="card-body">
                            
                            <table id="user_list" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Full Name</th>
                                        <th>Level </th>
                                        <th>Location</th>
                                        <th>Activity</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer"></div>
                    </div>
                </div>
            </div>
    </section>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    $(document).on('click', '#deleteData', function(){
        swal({
            title: "Are you sure?",
            text: "want to delete this data?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = $(this).attr('data-delete');
                Helper.loadingStart();
                if(id != 1){
                    $.ajax({
                        url: Helper.url('/utility/list-user/destroy/' + id),
                        type: 'post',
                        data:{
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(resp) {
                            Helper.loadingStop();
                            swal("success", "Data Has Been Saved", "success");
                            window.location.href = Helper.url('/utility/list-user');
                        },
                        error: function(resp, xhr, status, error) {
                            Helper.errorNotif('Error : '+resp.responseJSON.msg);
                            Helper.loadingStop();
                        },
                    })
                }else{
                    Helper.loadingStop();
                    swal("error", "This account can't be deleted", "error");
                }
            } else {
                swal("Data is not deleted");
            }
        });
    });

  $(document).ready(function() {
        var tableOrder = $('#user_list').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            ordering: 'true',
            // order: [2, 'desc'],
            responsive: false,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/utility/list-user/datatables'),
                
                type: 'get',
            },
            columns: [
                {
                    data: "email",
                    name: "email",
                    render: function(data, type, full) {
                        return full.email;
                    }
                },
                {
                    data: "name",
                    name: "name",
                    render: function(data, type, full) {
                        return full.name;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return full.get_role.name;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return '-';
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return '-';
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return '<a href="list-user/update/' + full.id + '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <button type="submit" class="btn btn-danger btn-sm" id="deleteData" data-delete="'+ full.id +'"><i class="fa fa-trash"></i></button>';
                    }
                }
            ]
        });
    }); 
</script>
@endsection
