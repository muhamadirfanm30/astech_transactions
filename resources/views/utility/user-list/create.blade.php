<head>
    <title>User System | Cashier System</title>
</head>
@extends('layouts.master')
@section('content')
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title">
                                        Create Users 
                                    </h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    {{-- <a href="{{url('/utility/user-list/create')}}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i>&nbsp;
                                        Add Users
                                    </a> --}}
                                </div>
                            </div>
                        
                        </div>
                        <form id="save_users">
                            {{-- <form method="POST" action="{{ url('/utility/list-user/store') }}" id="save_users"> --}}
                            {{-- @csrf --}}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 13px">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Username</label>
                                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="{{Request::old('username')}}"  required>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Password</label>
                                                <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" autocomplete="new-password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 13px">
                                        <label for="asd">Full Name</label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="{{Request::old('name')}}"  required>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 13px">
                                        <label for="asd">Brand</label>
                                        <select class="js-example-basic-single" name="ms_brand_id" id="brand_id" style="width: 100%" required>
                                            <option value="">choose  Brand</option>
                                            @if (!empty($brand))
                                                @foreach ($brand as $brn)
                                                    <option value="{{ $brn->id }}">{{$brn->name}}</option>
                                                @endforeach
                                            @else
                                                <option value="">Data Not Found</option>
                                            @endif
                                            
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 13px">
                                        <label for="asd">Make it a branch head</label>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="custom-control custom-radio">
                                                            <input class="custom-control-input" type="radio" id="customRadio2" name="is_head_office" id="head_office" value="0" checked>
                                                            <label for="customRadio2" class="custom-control-label">No</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="custom-control custom-radio">
                                                            <input class="custom-control-input" type="radio" id="customRadio1" name="is_head_office" id="head_office" value="1">
                                                            <label for="customRadio1" class="custom-control-label">Yes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 13px">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Company</label>
                                                <select class="js-example-basic-single" name="ms_companies_id" id="company_id" style="width: 100%" required>
                                                    <option value="">choose  company</option>
                                                    @if (!empty($company))
                                                        @foreach ($company as $com)
                                                            <option value="{{ $com->id }}">{{$com->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">Data Not Found</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                            <div class="col-md-8">
                                                <label for="">Branch Company</label>
                                                <select class="js-example-basic-multiple" name="ms_branch_id[]" id="branch_id" multiple="multiple" style="width: 100%">
                                                    <option value="" selected-disabled>Choose Branch (Optional)</option>
                                                    @foreach ($branchOffice as $bo)
                                                        <option value="{{ $bo->id }}">{{$bo->name}} ({{ $bo->company->name }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12"  style="margin-bottom: 13px">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Email</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{Request::old('email')}}"  required>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Phone Number</label>
                                                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" value="{{Request::old('phone_number')}}"  required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 13px">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Status</label>
                                                <select class="js-example-basic-single" name="status" id="status" id="exampleFormControlSelect1"  style="width: 100%" required>
                                                    <option value="">choose  status</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="approve">Approve</option>
                                                    <option value="reject">Reject</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Type</label>
                                                <select class="js-example-basic-single" name="type" id="type" id="exampleFormControlSelect1"  style="width: 100%" required>
                                                    <option value="">choose  type</option>
                                                    <option value="user">User</option>
                                                    <option value="admin">Admin</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">N.I.P</label>
                                                <select class="js-example-basic-single" class="js-example-basic-single" name="nip" id="nip" style="width: 100%" required>
                                                    <option>Udin Sarudin</option>
                                                    <option>Fahmi Nur Aulia</option>
                                                    <option>Arshad</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 13px">
                                        <label for="">Level</label>
                                        <select class="js-example-basic-single" name="role_id" id="level"  style="width: 100%" required>
                                            <option value="">choose  level</option>
                                            @foreach ($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <a href="{{ url('utility/list-user') }}" class="btn btn-danger">Back</a>
                                        <button class="btn btn-primary" id="save_users">Create User</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
 

    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
        $('.js-example-basic-single').select2();
    });


    // $(document).on('click', '#save_users', function(){
    $("#save_users").submit(function(e) {
        Helper.loadingStart();
        $.ajax({
            url: Helper.url('/utility/list-user/store'),
            type: 'post',
            data:{
                "_token": "{{ csrf_token() }}",
                'name': $("#name").val(),
                'email': $("#email").val(),
                'pass': $("#pass").val(),
                'username': $("#username").val(),
                'company_id': $("#company_id").val(),
                'brand_id': $("#brand_id").val(),
                'branch_id': $("#branch_id").val(),
                'nip': $("#nip").val(),
                'email': $("#email").val(),
                'phone_number': $("#phone_number").val(),
                'status': $("#status").val(),
                'type': $("#type").val(),
                'level': $("#level").val(),
                'head_office': $('input[name="is_head_office"]:checked').val()
            },
            success: function(resp) {
                Helper.loadingStop();
                swal("success", "Data Has Been Saved", "success");
                window.location.href = Helper.url('/utility/list-user');
            },
            error: function(resp, xhr, status, error) {
                Helper.errorNotif('Error : '+resp.responseJSON.msg);
                Helper.loadingStop();
            },
        })

    });
</script>
@endsection
