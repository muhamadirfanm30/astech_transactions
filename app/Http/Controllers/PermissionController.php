<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\DataTables;
use App\Traits\RoleWebService;
use DB;

class PermissionController extends Controller
{
    public function index()
    {
        return view('permission.index');
    }

    public function datatables()
    {
        return DataTables::of(Permission::query())
            ->addIndexColumn()
            ->toJson();
    }

    public function create()
    {
        return view('permission.create');
    }

    public function update($id)
    {
        return view('permission.update');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
        ]);

        for ($i = 0; $i < count($request->name); $i++) {
            $answers[] = [
                'name' => $request->name[$i],
                'guard_name' => 'web',
                'groups' => $request->groups[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        Permission::insert($answers);
        return redirect()->route('index.permission')
                        ->with('success','Permission created successfully');
        
    }


}
