<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserLoginRequest;
use App\Services\UserService;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginUser(UserLoginRequest $request, UserService $userService)
    {
        // validasi
        $request->validated();

        if ($userService->login($request->all())) {
            // check user verify
            // if ($userService->isVerify() == false) {
            //     return redirect('/verify')->with('error', 'your account has not been validated');
            // }

            // // redirect

            // if (Auth::user()->hasRole('Technician') && Auth::user()->hasRole('Customer')) {
            //     return redirect(route('teknisi.home'));
            // }

            // if (Auth::user()->hasRole('Customer')) {
            //     return redirect(route('user.home'));
            // }

            // if (Auth::user()->hasRole('Technician')) {
            //     return redirect(route('teknisi.home'));
            // }


            return redirect(route('dashboard'));
            // return 'dashboard';
        }
        // redirect
        // return 'gagal';
        return redirect('/')->with('error', 'Profile updated!');
        // return redirect('/')->with('error', 'wrong email or password');
    }

}
