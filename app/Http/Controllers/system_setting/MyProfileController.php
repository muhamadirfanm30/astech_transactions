<?php

namespace App\Http\Controllers\system_setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\User;
use App\Models\BranchOffice;
use DB;
use App\Models\Company;
use App\Models\Brand;
// use Hash;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;


class MyProfileController extends Controller
{
    public function index()
    {
        $company = Company::get();
        $brand = Brand::get();
        $branchOffice = BranchOffice::get();
        return view('system-setting.my-profile.index', [
            'brand' => $brand,
            'company' => $company,
            'branchOffice' => $branchOffice
        ]);
    }

    public function updateImage(Request $request, $id)
    {
        $cek = User::where('id', $id)->first();
        if(!empty($request->hasFile('image'))){
            if ($request->hasFile('image')) {
                $image      = $request->file('image');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/users-profile/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/users-profile/' . $cek->attachment);
                if ($exists) {
                    Storage::delete('public/users-profile/' . $cek->attachment);
                }
            }
        }else{
            $file_name = $cek->attachment;
        }

        try{
            $cek->update([
                'attachment' => $file_name,
            ]);
            DB::commit();
        }catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function update($id, Request $request)
    {
        try{
            $user = User::find($id);
            $user->update([
                'username' => $request->username,
                // 'password' => $password, 
                'name' => $request->name,
                'ms_brand_id' => $request->brand_id,
                'ms_companies_id' => $request->company_id,
                'ms_branch_id' => $request->branch_id,
                'nip' => $request->nip,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'address' => $request->address,
            ]);
            
            DB::commit();
        }catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        dd('Password change successfully.');
    }
}
