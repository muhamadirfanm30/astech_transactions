<?php

namespace App\Http\Controllers\transaction;

use Carbon\Carbon;
use App\Models\Payment;
use App\Helpers\utility;
use Illuminate\Http\Request;
use App\Models\PaymentDetail;
use App\Models\QuotationOrder;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\QuotationOrderCustomer;
use Illuminate\Database\QueryException;
use App\Models\QuotationOrderServicePartDetail;

class QuotationOrderController extends Controller
{
    public function index()
    {
        return view('transaction.quotation_orders.index');
    }

    public function create()
    {
        return view('transaction.quotation_orders.create');
    }

    public function list()
    {
        return QuotationOrder::with('status_payment','quotation_order_customer','payment_detail','quotation_order_servicepart_detail');
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesSearch(Request $request)
    {

        $query = $this->list();
        $by_date = 'date';
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
            $query->where($by_date, '>=', $date_from)
            ->where($by_date, '<=', $date_to);
        }
        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function paymentDetailDatatables($quotation_orders_id)
    {
        $query = PaymentDetail::with('payment')->where('quotation_orders_id', $quotation_orders_id);
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function typeProductSelect2(Request $request)
    {
        return QuotationOrder::where('type_product', 'like', '%' . $request->q . '%')
            ->groupBy('type_product')
            ->limit(20)
            ->get();
    }

    public function edit($id)
    {
        $maindata = QuotationOrder::with('brand','sparepart_detail','service_detail','status_payment','quotation_order_customer','payment_detail')->where('id',$id)->first();
        // retu rn $maindata;
        // return phpinfo();
        return view('transaction.quotation_orders.update', [
            'maindata' => $maindata
        ]);

    }

    public function printQo($id)
    {
        $maindata = QuotationOrder::with('brand','quotation_order_servicepart_detail','status_payment','quotation_order_customer','payment_detail')->where('id',$id)->first();
        // retu rn $maindata;
        return view('transaction.quotation_orders.print_qo', [
            'maindata' => $maindata
        ]);

    }

    public function printInv($id)
    {
        $maindata = QuotationOrder::with('brand','quotation_order_servicepart_detail','status_payment','quotation_order_customer','payment_detail')->where('id',$id)->first();
        // retu rn $maindata;
        return view('transaction.quotation_orders.print_inv', [
            'maindata' => $maindata
        ]);

    }

    public function paymentStore(Request $request) {
        DB::beginTransaction();
        try {
            $data_qo = QuotationOrder::where('id', $request->id)->first();
            $status_payments_id = 0;
            $date = $request->transfer_date != "" ? Carbon::parse($request->transfer_date)->format('Y-m-d') : Carbon::now()->format('Y-m-d');
            $remaining = $data_qo->total - ($data_qo->dp_paid + $request->payment_nominal);
            if($data_qo->total == $request->payment_nominal) { // lunas invoice
                $status_payments_id = 6;
                $update_data_qo = [
                    'inv_date' => $date,
                    'inv_paid'=> ($data_qo->dp_painv_paidid + $request->payment_nominal),
                    'remaining' => $remaining,
                    'status_payments_id' => $status_payments_id // check table status_payment_id
                ];

            } else { //dp
                if($data_qo->remaining == $request->payment_nominal) { // DP -> lunas
                    $status_payments_id = 5;
                } else if ($data_qo->remaining > $request->payment_nominal) { // DP -> DP Progress
                    $status_payments_id = 2;
                }
                $update_data_qo = [
                    'dp_date' => $date,
                    'dp_paid'=> ($data_qo->dp_paid + $request->payment_nominal),
                    'remaining' => $remaining,
                    'status_payments_id' => $status_payments_id
                ];
            }

            $update_qo = QuotationOrder::where('id', $request->id)->update($update_data_qo);
            $insert_payment = PaymentDetail::create([
                'ms_payments_id' => $request->ms_payments_id,
                'quotation_orders_id' => $request->id, //quotation orders_id
                'date' => $date,
                'status' => ($data_qo->total == $request->payment_nominal ? 'CASH' : 'DP'),
                'nominal' => $request->payment_nominal,
                'card_number' => isset($request->card_number) ? $request->card_number : null,
                'bank_name' => isset($request->bank_name) ? $request->bank_name : null,
                'approval_code' => isset($request->approval_code) ? $request->approval_code : null,
                'transfer_date' => isset($request->transfer_date) ? $request->transfer_date : null,
                'transfer_name' => isset($request->transfer_name) ? $request->transfer_name : null,
                'bank_name' => isset($request->bank_name) ? $request->bank_name : null,
            ]);

            DB::commit();

            return $this->successResponse(
                [$update_qo,$insert_payment],
                $this->successStoreMsg(),
                200
            );

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        // return $request->all();
        try {
            $quotation_orders = [
                'date' => Carbon::parse($request->date)->format('Y-m-d'),
                'ms_brands_id' => $request->ms_brands_id,
                'type_transaction' => $request->type_transaction,
                'type_product' => $request->type_product,
                'bill_number' => $request->bill_number,
                'so_number' => $request->so_number,
                'bill_to' => $request->bill_to,
                'description' => $request->description,
                'imei_number' => $request->imei_number,
                'total_price_sparepart' => $request->calc_total_sparepart,
                'total_price_service' => $request->calc_total_service,
                'status_payments_id' => 1,
                'dp_paid' => 0,
                'discount_percent' => $request->calc_discount_percent,
                'discount' => $request->calc_discount_nominal,
                'ppn_percent' => $request->calc_ppn_percent,
                'ppn' => $request->calc_ppn_nominal,
                'total' => $request->calc_grand_total,
                'payment' => $request->payment,
                'remaining' => $request->calc_grand_total,
            ];
            $insert_qo = QuotationOrder::create($quotation_orders);
            $quotation_order_customer = [
                'customer_name' => $request->customer_name,
                'handphone' => $request->handphone,
                'email' => $request->email,
                'address' => $request->address,
                'quotation_orders_id' => $insert_qo->id
            ];
            $insert_qoc = QuotationOrderCustomer::create($quotation_order_customer);
            // $data_spareparts = [];
            // $data_services = [];
            if($request->ms_spareparts_id != null) {
                foreach($request->ms_spareparts_id as $key => $id) {
                    $data_spareparts = [
                        'quotation_orders_id' => $insert_qo->id,
                        'status' => $request->sparepart_status[$key],
                        'ms_spareparts_id' => $id,
                        'sparepart_code' => $request->sparepart_code[$key],
                        'sparepart_name' => $request->sparepart_description[$key],
                        'qty' => $request->sparepart_qty[$key],
                        'price' => $request->sparepart_price[$key],
                        'discount' => $request->sparepart_discount[$key],
                        'total' => $request->sub_total_sparepart[$key],

                    ];
                    $insert_sparepart_detail = QuotationOrderServicePartDetail::create($data_spareparts);
                }
            }
            if($request->ms_services_id != null) {
                foreach($request->ms_services_id as $key => $id) {
                    $data_services = [
                        'quotation_orders_id' => $insert_qo->id,
                        'status' => $request->service_status[$key],
                        'ms_services_id' => $id,
                        'service_code' => $request->service_code[$key],
                        'service_name' => $request->service_description[$key],
                        'qty' => $request->service_qty[$key],
                        'price' => $request->service_price[$key],
                        'discount' => $request->service_discount[$key],
                        'total' => $request->sub_total_service[$key],

                    ];
                    $insert_service_detail = QuotationOrderServicePartDetail::create($data_services);
                }
            }

            DB::commit();

            return $this->successResponse(
                $insert_qo,
                $this->successStoreMsg(),
                200
            );

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }


    }

    public function update($id, Request $request) {
        $data = QuotationOrder::where('id',$id)->first();
        DB::beginTransaction();
        try {
            $data_qo = [
                'date' => Carbon::parse($request->date)->format('Y-m-d'),
                'ms_brands_id' => $request->ms_brands_id,
                'type_transaction' => $request->type_transaction,
                'type_product' => $request->type_product,
                'bill_number' => $request->bill_number,
                'so_number' => $request->so_number,
                'bill_to' => $request->bill_to,
                'description' => $request->description,
                'imei_number' => $request->imei_number,
                'total_price_sparepart' => $request->calc_total_sparepart,
                'total_price_service' => $request->calc_total_service,
                'status_payments_id' => $request->status_payments_id,
                'dp_paid' => $request->dp_paid,
                'discount_percent' => $request->calc_discount_percent,
                'discount' => $request->calc_discount_nominal,
                'ppn_percent' => $request->calc_ppn_percent,
                'ppn' => $request->calc_ppn_nominal,
                'total' => $request->calc_grand_total,
                'payment' => $request->payment,
                'remaining' => $request->remaining,
            ];

            $update_qo = QuotationOrder::where('id',$id)->update($data_qo);
            $update_sparepart = $this->updateSparepartDetail($id, $request);
            $update_service = $this->updateServiceDetail($id, $request);
            DB::commit();
            return $this->successResponse(
                [$data_qo ,$update_sparepart,$update_service],
                $this->successStoreMsg(),
                200
            );

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

    }


    public function updateSparepartDetail($id, $request)
    {
        $quotation_orders = QuotationOrder::where('id', $id)->firstOrFail();
        $details_update = [];
        $details_insert = [];
        // loop request
        if(isset($request['sparepart_details_id'])) {
            foreach ($request['sparepart_details_id'] as $key => $value) {
                // jika id tidak 0 == update
                // $check_details = !empty($request['sparepart_status'][$key]) &&
                // !empty($request['ms_spareparts_id'][$key]) &&
                // !empty($request['sparepart_code'][$key]) &&
                // !empty($request['sparepart_description'][$key]) &&
                // !empty($request['sparepart_code'][$key]) &&
                // ($request['sparepart_qty'][$key] != '') &&
                // ($request['sparepart_price'][$key] != '') &&
                // ($request['sparepart_discount'][$key] != '') &&
                // ($request['sub_total_sparepart'][$key] != '');

                // if($check_details) {
                    if ($value != 0) {
                        $details_update[] = [
                            'id' => $value,
                            'status' => $request['sparepart_status'][$key],
                            'ms_spareparts_id' => $request['ms_spareparts_id'][$key],
                            'sparepart_code' => $request['sparepart_code'][$key],
                            'sparepart_description' => $request['sparepart_description'][$key],
                            'sparepart_qty' => $request['sparepart_qty'][$key],
                            'sparepart_price' => $request['sparepart_price'][$key],
                            'sparepart_discount' => $request['sparepart_discount'][$key],
                            'sub_total_sparepart' => $request['sub_total_sparepart'][$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];

                    } else { // jika id 0 == insert

                        $details_insert[] = [
                            'quotation_orders_id' => $quotation_orders->id,
                            'status' => $request['sparepart_status'][$key],
                            'ms_spareparts_id' => $request['ms_spareparts_id'][$key],
                            'sparepart_code' => $request['sparepart_code'][$key],
                            'sparepart_description' => $request['sparepart_description'][$key],
                            'sparepart_qty' => $request['sparepart_qty'][$key],
                            'sparepart_price' => $request['sparepart_price'][$key],
                            'sparepart_discount' => $request['sparepart_discount'][$key],
                            'sub_total_sparepart' => $request['sub_total_sparepart'][$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }
                // }
            }

            DB::beginTransaction();
            try {
                // update
                if (count($details_update) > 0) {
                    // return $details_update;
                    foreach ($details_update as $data) {

                        QuotationOrderServicePartDetail::where('id', $data['id'])->update([
                            'status' => $data['status'],
                            'ms_spareparts_id' => $data['ms_spareparts_id'],
                            'sparepart_code' => $data['sparepart_code'],
                            'sparepart_name' => $data['sparepart_description'],
                            'qty' => $data['sparepart_qty'],
                            'price' => $data['sparepart_price'],
                            'discount' => $data['sparepart_discount'],
                            'total' => $data['sub_total_sparepart'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);
                    }
                }

                // insert
                if (count($details_insert) > 0) {
                    foreach($details_insert as $data) {
                        QuotationOrderServicePartDetail::create([
                            'quotation_orders_id' => $quotation_orders->id,
                            'status' => $data['status'],
                            'ms_spareparts_id' => $data['ms_spareparts_id'],
                            'sparepart_code' => $data['sparepart_code'],
                            'sparepart_name' => $data['sparepart_description'],
                            'qty' => $data['sparepart_qty'],
                            'price' => $data['sparepart_price'],
                            'discount' => $data['sparepart_discount'],
                            'total' => $data['sub_total_sparepart'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);
                    }
                }
                DB::commit();

            } catch (QueryException $e) {
                DB::rollback();
                throw new \Exception($e->getMessage());
            }
        }

    }

    public function updateServiceDetail($id, $request)
    {
        $quotation_orders = QuotationOrder::where('id', $id)->firstOrFail();
        $details_update = [];
        $details_insert = [];
        // loop request
        if(isset($request->service_details_id)) {
            foreach ($request->service_details_id as $key => $value) {
                    // jika id tidak 0 == update
                    if ($value != 0) {
                        $details_update[] = [
                            'id' => $value,
                            'status' => $request['service_status'][$key],
                            'ms_services_id' => $request['ms_services_id'][$key],
                            'service_code' => $request['service_code'][$key],
                            'service_description' => $request['service_description'][$key],
                            'service_qty' => $request['service_qty'][$key],
                            'service_price' => $request['service_price'][$key],
                            'service_discount' => $request['service_discount'][$key],
                            'sub_total_service' => $request['sub_total_service'][$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];

                    } else { // jika id 0 == insert
                        $details_insert[] = [
                            'quotation_orders_id' => $quotation_orders->id,
                            'status' => $request['service_status'][$key],
                            'ms_services_id' => $request['ms_services_id'][$key],
                            'service_code' => $request['service_code'][$key],
                            'service_description' => $request['service_description'][$key],
                            'service_qty' => $request['service_qty'][$key],
                            'service_price' => $request['service_price'][$key],
                            'service_discount' => $request['service_discount'][$key],
                            'sub_total_service' => $request['sub_total_service'][$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }
            }

            DB::beginTransaction();
            try {
                // update
                if (count($details_update) > 0) {
                    foreach ($details_update as $data) {
                        QuotationOrderServicePartDetail::where('id', $data['id'])->update([
                            'status' => $data['status'],
                            'ms_services_id' => $data['ms_services_id'],
                            'service_code' => $data['service_code'],
                            'service_name' => $data['service_description'],
                            'qty' => $data['service_qty'],
                            'price' => $data['service_price'],
                            'discount' => $data['service_discount'],
                            'total' => $data['sub_total_service'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);
                    }
                }

                // insert
                if (count($details_insert) > 0) {
                    foreach($details_insert as $data) {
                        QuotationOrderServicePartDetail::create([
                            'quotation_orders_id' => $quotation_orders->id,
                            'status' => $data['status'],
                            'ms_services_id' => $data['ms_services_id'],
                            'service_code' => $data['service_code'],
                            'service_name' => $data['service_description'],
                            'qty' => $data['service_qty'],
                            'price' => $data['service_price'],
                            'discount' => $data['service_discount'],
                            'total' => $data['sub_total_service'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);
                    }
                }
                DB::commit();
            } catch (QueryException $e) {
                DB::rollback();
                throw new \Exception($e->getMessage());
            }
        }
    }

    public function delete($id)
    {
        $data = QuotationOrder::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }

    public function destroyDetail($detail_id, Request $request )
    {
        DB::beginTransaction();
        // return $request->all();
        try {
            $data = QuotationOrderServicePartDetail::where('id', $detail_id)->firstOrFail();

            $this->updateTotal($data);
            $data->delete();

            DB::commit();
            return $this->successResponse([
                'data' => $data,
                'uniq' => time(),
            ], $this->successDeleteMsg(), 200);
        }
        catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

    }

    public function updateTotal($data) {
        $type_field = '';
        if($data->status == 'PART') {
            $type_field = 'total_price_sparepart';
        } else {
            $type_field = 'total_price_service';
        }
        $data_qo = QuotationOrder::where('id', $data->quotation_orders_id)->first();
        $total_update = $data_qo->total - $data->total;

        $discount_update = $total_update - ($total_update * ($data_qo->discount_percent / 100));
        $ppn_update = $total_update - ($total_update * ($data_qo->ppn_percent / 100));

        return $update_nominal = QuotationOrder::where('id', $data_qo->id)->update([
            $type_field => $data->total,
            'discount'  => $discount_update ,
            'ppn'       => $ppn_update,
            'total'     => $total_update,]);

    }
}
