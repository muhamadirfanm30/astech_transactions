<?php

namespace App\Http\Controllers\pos;

use Carbon\Carbon;
use App\Models\POSProduct;
use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class POSProductController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Products';
    }

    public function index()
    {
        return view('pos.products.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.products.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSProduct::with('barcode_symbology','unit_type');
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function edit($id)
    {
        $maindata = POSProduct::with('supplier','category','branch','tax','barcode_symbology','unit_type')->where('id',$id)->first();
        // return $maindata;
        return view('pos.products.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {
        $path = POSProduct::getImagePathUpload();
        $filename = null;
        $str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 16;

        if ($request->image != null) {
            $image = (new ImageUpload)->upload($request->image, $path);
            $filename = $image->getFilename();
        }

        $insert = POSProduct::create([
            'code'                       => substr(str_shuffle(str_repeat($str, 5)), 0, $length),
            'sku'                        => $request->sku,
            'name'                       => $request->name,
            'barcode'                    => $request->barcode,
            'cogs_price'                 => $request->cogs_price,
            'selling_price'              => $request->selling_price,
            // 'qty'                        => $request->qty,
            // 'alert_qty'                  => $request->alert_qty,
            'expiry_date'                => Carbon::parse($request->expiry_date)->format('Y-m-d'),
            'manufacturing_date'         => Carbon::parse($request->manufacturing_date)->format('Y-m-d'),
            'status'                     => $request->status,
            'discount_rate'              => $request->discount_rate,
            'discount_nominal'           => $request->discount_nominal,
            'product_details'            => $request->product_details,
            'image'                      => $filename,
            'pos_unit_types_id'          => $request->pos_unit_types_id,
            'pos_barcode_symbologies_id' => $request->pos_barcode_symbologies_id,
            'pos_categories_id'          => $request->pos_categories_id,
            'pos_suppliers_id'           => $request->pos_suppliers_id,
            'ms_branches_id'             => Auth::user()->ms_branch_id,
            'pos_taxes_id'               => $request->pos_taxes_id,
            // 'sold_out'                   => $request->address,
        ]);


        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {
        $cek_image = POSProduct::where('id', $id)->first();

        if(!empty($request->image)){
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = time(). '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/pos_products/' . $filename, $img);
                $exists2 = Storage::disk('local')->has('public/pos_products/' . $cek_image->image);
                if ($exists2) {
                    Storage::delete('public/pos_products/' . $cek_image->image);
                }
            }
        }else{
            $filename = $cek_image->image;
        }

        $update = POSProduct::find($id)->update([
            'sku'                        => $request->sku,
            'name'                       => $request->name,
            'barcode'                    => $request->barcode,
            'cogs_price'                 => $request->cogs_price,
            'selling_price'              => $request->selling_price,
            // 'qty'                        => $request->qty,
            // 'alert_qty'                  => $request->alert_qty,
            'expiry_date'                => Carbon::parse($request->expiry_date)->format('Y-m-d'),
            'manufacturing_date'         => Carbon::parse($request->manufacturing_date)->format('Y-m-d'),
            'status'                     => $request->status,
            'discount_rate'              => $request->discount_rate,
            'discount_nominal'           => $request->discount_nominal,
            'product_details'            => $request->product_details,
            'image'                      => $filename,
            'pos_unit_types_id'          => $request->pos_unit_types_id,
            'pos_barcode_symbologies_id' => $request->pos_barcode_symbologies_id,
            'pos_categories_id'          => $request->pos_categories_id,
            'pos_suppliers_id'           => $request->pos_suppliers_id,
            'ms_branches_id'             => Auth::user()->ms_branch_id,
            'pos_taxes_id'               => $request->pos_taxes_id,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function delete($id)
    {
        $data = POSProduct::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
