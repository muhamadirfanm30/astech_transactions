<?php

namespace App\Http\Controllers\pos;

use App\Models\POSTax;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class POSTaxController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Taxes';
    }

    public function index()
    {
        return view('pos.taxes.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.taxes.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSTax::query();
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function edit($id)
    {
        $maindata = POSTax::where('id',$id)->first();
        return view('pos.taxes.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {


        $insert = POSTax::create([
            'name' => $request->name,
            'code' => $request->code,
            'rate' => $request->rate,
        ]);

        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {

        $update = POSTax::find($id)->update([
            'name' => $request->name,
            'code' => $request->code,
            'rate' => $request->rate,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function select2(Request $request)
    {
        return POSTax::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function delete($id)
    {
        $data = POSTax::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
