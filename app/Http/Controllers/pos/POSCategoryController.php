<?php

namespace App\Http\Controllers\pos;

use App\Models\POSCategory;
use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class POSCategoryController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Categories';
    }

    public function index()
    {
        return view('pos.categories.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.categories.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSCategory::query();
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return POSCategory::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function edit($id)
    {
        $maindata = POSCategory::where('id',$id)->first();
        return view('pos.categories.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {
        $path = POSCategory::getImagePathUpload();
        $filename = null;
        $str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 6;

        if ($request->image != null) {
            $image = (new ImageUpload)->upload($request->image, $path);
            $filename = $image->getFilename();
        }
        $insert = POSCategory::create([
            'code'   => substr(str_shuffle(str_repeat($str, 5)), 0, $length),
            'name'   => $request->name,
            'detail' => $request->detail,
            'image'  => $filename,
        ]);

        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {

        $cek_image = POSCategory::where('id', $id)->first();

        if(!empty($request->image)){
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = time(). '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/pos_categories/' . $filename, $img);
                $exists2 = Storage::disk('local')->has('public/pos_categories/' . $cek_image->image);
                if ($exists2) {
                    Storage::delete('public/pos_categories/' . $cek_image->image);
                }
            }
        }else{
            $filename = $cek_image->image;
        }

        $update = POSCategory::find($id)->update([
            'name'   => $request->name,
            'detail' => $request->detail,
            'image'  => $filename,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function delete($id)
    {
        $data = POSCategory::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
