<?php

namespace App\Http\Controllers\pos;

use App\Models\POSUnitType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class POSUnitTypeController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Unit Types';
    }

    public function index()
    {
        return view('pos.unit_types.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.unit_types.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSUnitType::query();
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return POSUnitType::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function edit($id)
    {
        $maindata = POSUnitType::where('id',$id)->first();
        return view('pos.unit_types.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {


        $insert = POSUnitType::create([
            'name' => $request->name,
        ]);

        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {

        $update = POSUnitType::find($id)->update([
            'name' => $request->name,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function delete($id)
    {
        $data = POSUnitType::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
