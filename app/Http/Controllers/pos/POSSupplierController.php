<?php

namespace App\Http\Controllers\pos;

use App\Models\POSSupplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class POSSupplierController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Suppliers';
    }

    public function index()
    {
        return view('pos.suppliers.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.suppliers.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSSupplier::query();
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return POSSupplier::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function edit($id)
    {
        $maindata = POSSupplier::where('id',$id)->first();
        return view('pos.suppliers.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {


        $insert = POSSupplier::create([
            'name'    => $request->name,
            'email'   => $request->email,
            'company' => $request->company,
            'phone'   => $request->phone,
            'vat'     => $request->vat,
            'address' => $request->address,
        ]);

        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {

        $update = POSSupplier::find($id)->update([
            'name'    => $request->name,
            'email'   => $request->email,
            'company' => $request->company,
            'phone'   => $request->phone,
            'vat'     => $request->vat,
            'address' => $request->address,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function delete($id)
    {
        $data = POSSupplier::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
