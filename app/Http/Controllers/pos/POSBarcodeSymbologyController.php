<?php

namespace App\Http\Controllers\pos;

use App\Models\POSBarcodeSymbology;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;


class POSBarcodeSymbologyController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Pos Barcode Symbologies';
    }

    public function index()
    {
        return view('pos.barcode_symbologies.index',[
            'title' => $this->title
        ]);
    }

    public function create()
    {
        return view('pos.barcode_symbologies.create',[
            'title' => $this->title
        ]);
    }

    public function list()
    {
        return POSBarcodeSymbology::query();
    }

    public function datatables()
    {

        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return POSBarcodeSymbology::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function edit($id)
    {
        $maindata = POSBarcodeSymbology::where('id',$id)->first();
        return view('pos.barcode_symbologies.update', [
            'maindata' => $maindata,
            'title' => $this->title
        ]);

    }

    public function store(Request $request) {


        $insert = POSBarcodeSymbology::create([
            'name' => $request->name,
            'code' => $request->code,
            'desc' => $request->desc,
        ]);

        return $this->successResponse(
            $insert,
            $this->successStoreMsg(),
            200
        );
    }

    public function update($id, Request $request) {

        $update = POSBarcodeSymbology::find($id)->update([
            'name' => $request->name,
            'code' => $request->code,
            'desc' => $request->desc,
        ]);

        return $this->successResponse(
            $update,
            $this->successStoreMsg(),
            200
        );
    }

    public function delete($id)
    {
        $data = POSBarcodeSymbology::find($id)->delete();
        return $this->successResponse(
            $data,
            $this->successDeleteMsg(),
            204
        );
    }


}
