<?php

namespace App\Http\Controllers\master;

use App\Models\Brand;
use App\Models\Company;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\ImportLogService;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ServiceController extends Controller
{
    public function index()
    {
        $brand = brand::get();
        return view('master.services.index', [
            // 'showServices' => $showServices,
            'brand' => $brand,
        ]);
    }

    public function list()
    {
        return Service::query();
    }

    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function importLogdatatables()
    {
        $query = ImportLogService::with('user');
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function importedByDatatables($users_id)
    {
        $query = Service::with('user','getcompany','getbrand')->where('imported_by', $users_id)->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return Service::where('description', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function serviceSearch(Request $request)
    {
        $query = Service::orderBy('id');
        if (request('filter_periode')) {
            $filter_periode = now()->subDays(request('filter_periode'))->toDateString();
            $query->where('created_at', '>=', $filter_periode);
        }

        return datatables($query)->toJson();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function create()
    {
        $showServices = Service::get();
        $showCompany = Company::get();
        $showBrand = Brand::get();
        return view('master.services.create', [
            'showServices' => $showServices,
            'showCompany' => $showCompany,
            'showBrand' => $showBrand,
        ]);
    }

    public function update($id)
    {
        $seriveEdit = Service::find($id);
        $showCompany = Company::get();
        $showBrand = Brand::get();
        return view('master.services.update', [
            'seriveEdit' => $seriveEdit,
            'showCompany' => $showCompany,
            'showBrand' => $showBrand,
        ]);
    }

    public function store(Request $request, $length = 20)
    {
        $this->validate($request, [
            'status' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'service_code' => 'required',
            'svc_type' => 'required',
            'defect_type' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required',
        ]);

        Service::create([
            'status' => $request->status,
            'ms_companies_id' => $request->ms_companies_id,
            'ms_brands_id' => $request->ms_brands_id,
            'service_code' => $request->service_code,
            'svc_type' => $request->svc_type,
            'defect_type' => $request->defect_type,
            'cogs_price' => $request->cogs_price,
            'selling_price' => $request->selling_price,
            'description' => $request->description,
        ]);

        return redirect()->route('services.home')->with('success', 'This Service Has Been Saved.');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'service_code' => 'required',
            'svc_type' => 'required',
            'defect_type' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required',
        ]);

        Service::find($id)->update([
            'status' => $request->status,
            'ms_companies_id' => $request->ms_companies_id,
            'ms_brands_id' => $request->ms_brands_id,
            'service_code' => $request->service_code,
            'svc_type' => $request->svc_type,
            'defect_type' => $request->defect_type,
            'cogs_price' => $request->cogs_price,
            'selling_price' => $request->selling_price,
            'description' => $request->description,
        ]);

        return redirect()->route('services.home')->with('success', 'This Service Has Been Updated.');
    }

    public function destroy($id)
    {
        Service::find($id)->delete();
        return redirect()->route('services.home')->with('success', 'This Service Has Been Deleted.');
    }
}
