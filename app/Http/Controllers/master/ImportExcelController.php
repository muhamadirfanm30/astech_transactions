<?php

namespace App\Http\Controllers\master;

use DateTime;
use Exception;

use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Company;
use App\Models\Service;
use App\Models\ModelPart;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\CategorySparepart;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ImportLogCategorySparepart;
use App\Models\ImportLogModelPart;
use App\Models\ImportLogService;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Database\QueryException;



class ImportExcelController extends Controller
{
    /**
     * show list with paginate
     */

    private function isDuplicateEntryException(QueryException $e)
    {
        $sqlState = $e->errorInfo[0];
        $errorCode  = $e->errorInfo[1];
        if ($sqlState === "23000" && $errorCode === 1062) {
            return true;
        }
         return false;
    }

    public function importExcel(Request $request) {
        // type import
        // 1. category_spareparts
        // 2. services
        // 3. models_parts
        try {
            request()->validate([
            'file' => 'required',
            'file.*' => 'mimes:csv,xls,xlsx'
            ]);

            $file = $request->file('file');
            $nama_file = rand().$file->getClientOriginalName();
            $file->storeAs('public/file_import_all/',$nama_file);
            $import = null;
            if ($request->get('type_import') == 'category_spareparts') {
                return $this->uploadExcel($nama_file, 'category_spareparts');
            } else if ($request->get('type_import') == 'services') {
                return $this->uploadExcel($nama_file, 'services');
            } else if ($request->get('type_import') == 'model_parts') {
                return $this->uploadExcel($nama_file, 'model_parts');
            }

        } catch (Exception $e) {
            return $e;
        }
    }

    public function saveDataCategorySpareparts(Request $request) {

        foreach($request->data as $key => $row) {
            $data = CategorySparepart::create([
                'name'        => $row['name'],
                'imported_by' => Auth::user()->id
            ]);
        }
        $import_log = ImportLogCategorySparepart::create([
                'users_id' => Auth::user()->id,
                'amount_imported' => count($request->data)
            ]);

        return $this->successResponse($data,
                'success',
                201
            );
    }



    public function saveDataServices(Request $request) {

        foreach($request->data as $key => $row) {
            $data = Service::create([
                'status'          => $row['status'],
                'ms_companies_id' => Company::where(DB::raw("LOWER(name)"), strtolower($row['company_name']))->value('id'),
                'ms_brands_id'    => Brand::where(DB::raw("LOWER(name)"), strtolower($row['brand_name']))->value('id'),
                'service_code'    => $row['service_code'],
                'description'     => $row['description'],
                'svc_type'        => $row['svc_type'],
                'defect_type'     => $row['defect_type'],
                'cogs_price'      => $row['cogs_price'],
                'selling_price'   => $row['selling_price'],
                'imported_by'     => Auth::user()->id
            ]);
        }

        $import_log = ImportLogService::create([
                'users_id' => Auth::user()->id,
                'amount_imported' => count($request->data)
            ]);

        return $this->successResponse($data,
                'success',
                201
            );
    }

    public function saveDataModelParts(Request $request) {
        foreach($request->data as $key => $row) {
            $data = ModelPart::create([
                'ms_companies_id'  => Company::where(DB::raw("LOWER(name)"), strtolower($row['company_name']))->value('id'),
                'model_code'       => $row['model_code'],
                'ms_brands_id'     => Brand::where(DB::raw("LOWER(name)"), strtolower($row['brand_name']))->value('id'),
                'ms_categories_id' => CategorySparepart::where(DB::raw("LOWER(name)"), strtolower($row['category_name']))->value('id'),
                'model_name'       => $row['model_name'],
                'service_code'     => $row['service_code'],
                'desc'             => $row['desc'],
                'imported_by'      => Auth::user()->id
            ]);
        }

        $import_log = ImportLogModelPart::create([
                'users_id' => Auth::user()->id,
                'amount_imported' => count($request->data)
            ]);

        return $this->successResponse($data,
                'success',
                201
            );
    }


    public function uploadExcel($nama_file, $type_import) {
        // type import
        // 1. batchinventory
        // 2. orders_data_excel
        ini_set('memory_limit', '-1');
        if (isset($nama_file)) {
            $dataExcels = null;
            if ($type_import == 'category_spareparts') {
                $dataExcels = $this->checkImportCategorySpareparts($nama_file);
            } else if($type_import == 'services') {
                $dataExcels = $this->checkImportServices($nama_file);
            } else if($type_import == 'model_parts') {
                $dataExcels = $this->checkImportModelParts($nama_file);
            }
            if (isset($dataExcels['error']) && $dataExcels['error'] == 0) {
                return json_encode(array('error' => 0, 'preview' => $dataExcels['data']));

            } else {
                if (isset($dataExcels['data']) && $dataExcels['data'] == false) {
                    return json_encode(array('error' => 1, 'msg' => 'Excel format is incorrect or excel cannot be empty', 'showSubscribeModal' => 'hide'));
                } else {
                    return json_encode(array('error' => 2, 'data' => $dataExcels['data']));
                }
            }
        } else {
            //error checking goes here
            return json_encode(array('error' => 1, 'msg' => 'no file is selected'));
        }
    }

    public function checkImportCategorySpareparts($nama_file)
    {
        ini_set('memory_limit', '-1');

        // $collection = Excel::selectSheetsByIndex(1)->load($file, function ($reader) {
        //     $reader->formatDates(false);
        // })->get();

        $collection = (new FastExcel)->sheet(1)->import(storage_path('app/public/file_import_all/'.$nama_file));

        if (sizeof($collection) == 0) {
            return ['error' => 1, 'data' => false];
        } else {
            $errorArray = array();
            $row = 2;

            foreach ($collection->toArray() as $key => $getExcel) {


                if (trim($getExcel['name']) == null) {
                    array_push($errorArray, 'Name cannot be empty on cell A' . ($key+$row));
                }

            }
            if (sizeof($errorArray) > 0) {
                unlink(storage_path('app/public/file_import_all/'.$nama_file));
                return ["error" => 1, 'data' => $errorArray];
            } else {
                return ["error" => 0, 'data' => $collection];
            }
        }
    }
    public function checkImportServices($nama_file)
    {
        ini_set('memory_limit', '-1');

        // $collection = Excel::selectSheetsByIndex(1)->load($file, function ($reader) {
        //     $reader->formatDates(false);
        // })->get();

        $collection = (new FastExcel)->sheet(1)->import(storage_path('app/public/file_import_all/'.$nama_file));

        if (sizeof($collection) == 0) {
            return ['error' => 1, 'data' => false];
        } else {
            $errorArray = array();
            $row = 2;

            foreach ($collection->toArray() as $key => $getExcel) {

                if (trim($getExcel['status']) == null) {
                    array_push($errorArray, 'Status cannot be empty on cell A' . ($key+$row));
                }
                if (trim($getExcel['company_name']) == null) {
                    array_push($errorArray, 'Company Name cannot be empty on cell B' . ($key+$row));
                } else {
                    $find = Company::where(DB::raw("LOWER(name)"), strtolower($getExcel['company_name']))->count();
                    if (!$find) {
                        array_push($errorArray, 'Company Name "'.$getExcel['company_name'].'" is not found on cell B' . ($key+$row));
                    }
                }
                if (trim($getExcel['brand_name']) == null) {
                    array_push($errorArray, 'Brand Name cannot be empty on cell C' . ($key+$row));
                } else {
                    $find = Brand::where(DB::raw("LOWER(name)"), strtolower($getExcel['brand_name']))->count();
                    if (!$find) {
                        array_push($errorArray, 'Brand Name "'.$getExcel['brand_name'].'" is not found on cell C' . ($key+$row));
                    }
                }

                if (trim($getExcel['service_code']) == null) {
                    array_push($errorArray, 'Service Code cannot be empty on cell D' . ($key+$row));
                }
                if (trim($getExcel['description']) == null) {
                    array_push($errorArray, 'Description cannot be empty on cell E' . ($key+$row));
                }
                if (trim($getExcel['svc_type']) == null) {
                    array_push($errorArray, 'SCV Type cannot be empty on cell F' . ($key+$row));
                }
                if (trim($getExcel['defect_type']) == null) {
                    array_push($errorArray, 'Defect Type cannot be empty on cell G' . ($key+$row));
                }
                if (trim($getExcel['cogs_price']) == null) {
                    array_push($errorArray, 'Cogs Price Type cannot be empty on cell H' . ($key+$row));
                }
                if (trim($getExcel['selling_price']) == null) {
                    array_push($errorArray, 'Selling Price Type cannot be empty on cell I' . ($key+$row));
                }

            }
            if (sizeof($errorArray) > 0) {
                unlink(storage_path('app/public/file_import_all/'.$nama_file));
                return ["error" => 1, 'data' => $errorArray];
            } else {
                return ["error" => 0, 'data' => $collection];
            }
        }
    }
    public function checkImportModelParts($nama_file)
    {
        ini_set('memory_limit', '-1');

        // $collection = Excel::selectSheetsByIndex(1)->load($file, function ($reader) {
        //     $reader->formatDates(false);
        // })->get();

        $collection = (new FastExcel)->sheet(1)->import(storage_path('app/public/file_import_all/'.$nama_file));

        if (sizeof($collection) == 0) {
            return ['error' => 1, 'data' => false];
        } else {
            $errorArray = array();
            $row = 2;

            foreach ($collection->toArray() as $key => $getExcel) {

                if (trim($getExcel['company_name']) == null) {
                    array_push($errorArray, 'Company Name cannot be empty on cell A' . ($key+$row));
                } else {
                    $find = Company::where(DB::raw("LOWER(name)"), strtolower($getExcel['company_name']))->count();
                    if (!$find) {
                        array_push($errorArray, 'Company Name "'.$getExcel['company_name'].'" is not found on cell A' . ($key+$row));
                    }
                }
                if (trim($getExcel['model_code']) == null) {
                    array_push($errorArray, 'Model Code cannot be empty on cell B' . ($key+$row));
                }
                if (trim($getExcel['brand_name']) == null) {
                    array_push($errorArray, 'Brand Name cannot be empty on cell C' . ($key+$row));
                } else {
                    $find = Brand::where(DB::raw("LOWER(name)"), strtolower($getExcel['brand_name']))->count();
                    if (!$find) {
                        array_push($errorArray, 'Brand Name "'.$getExcel['brand_name'].'" is not found on cell C' . ($key+$row));
                    }
                }
                if (trim($getExcel['category_name']) == null) {
                    array_push($errorArray, 'Category Name cannot be empty on cell D' . ($key+$row));
                } else {
                    $find = CategorySparepart::where(DB::raw("LOWER(name)"), strtolower($getExcel['category_name']))->count();
                    if (!$find) {
                        array_push($errorArray, 'Category Name "'.$getExcel['category_name'].'" is not found on cell D' . ($key+$row));
                    }
                }

                if (trim($getExcel['model_name']) == null) {
                    array_push($errorArray, 'Model Name cannot be empty on cell E' . ($key+$row));
                }
                if (trim($getExcel['service_code']) == null) {
                    array_push($errorArray, 'Service Code cannot be empty on cell F' . ($key+$row));
                }
                if (trim($getExcel['desc']) == null) {
                    array_push($errorArray, 'Description cannot be empty on cell G' . ($key+$row));
                }

            }
            if (sizeof($errorArray) > 0) {
                unlink(storage_path('app/public/file_import_all/'.$nama_file));
                return ["error" => 1, 'data' => $errorArray];
            } else {
                return ["error" => 0, 'data' => $collection];
            }
        }
    }
}
