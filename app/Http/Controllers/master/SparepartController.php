<?php

namespace App\Http\Controllers\master;

use App\Models\Brand;
use App\Models\Company;
use App\Models\Service;
use App\Models\Sparepart;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\CategorySparepart;
use App\Http\Controllers\Controller;


class SparepartController extends Controller
{
    public function index()
    {
        $showSparepart = Sparepart::get();
        return view('master.spareparts.index', [
            'showSparepart' => $showSparepart
        ]);
    }

    public function list()
    {
        return Sparepart::query();
    }

    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return Sparepart::where('description', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function create()
    {
        $showServices = Service::get();
        $showCompany = Company::get();
        $showBrand = Brand::get();
        $showCategory = CategorySparepart::get();
        return view('master.spareparts.create', [
            'showServices' => $showServices,
            'showCompany' => $showCompany,
            'showCategory' => $showCategory,
            'showBrand' => $showBrand,
        ]);
    }

    public function update($id)
    {
        $showServices = Service::get();
        $showCompany = Company::get();
        $showBrand = Brand::get();
        $showCategory = CategorySparepart::get();
        $dataEdit = Sparepart::find($id);
        return view('master.spareparts.update', [
            'showServices' => $showServices,
            'showCompany' => $showCompany,
            'showCategory' => $showCategory,
            'showBrand' => $showBrand,
            'dataEdit' => $dataEdit,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'ms_categories_id' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'sparepart_code' => 'required',
            'division_category' => 'required',
            'uom' => 'required',
            'description' => 'required',
            'imei_1' => 'required',
            'supplier_price_1' => 'required',
            'supplier_price_2' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required'
        ]);

        Sparepart::create([
            'ms_categories_id' => $request->ms_categories_id ,
            'ms_companies_id' => $request->ms_companies_id ,
            'ms_brands_id' => $request->ms_brands_id ,
            'sparepart_code' => $request->sparepart_code ,
            'division_category' => $request->division_category ,
            'uom' => $request->uom ,
            'description' => $request->description ,
            'imei_1' => $request->imei_1 ,
            'imei_2' => $request->imei_2 ,
            'supplier_price_1' => $request->supplier_price_1 ,
            'supplier_price_2' => $request->supplier_price_2 ,
            'cogs_price' => $request->cogs_price ,
            'selling_price' => $request->selling_price ,
            'up_price_percent' => $request->up_price_percent ,
            'customer_price' => $request->customer_price
        ]);

        return redirect()->route('spareparts.home')->with('success', 'This Sparepart Has Been Saved.');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'ms_categories_id' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'sparepart_code' => 'required',
            'division_category' => 'required',
            'uom' => 'required',
            'description' => 'required',
            'imei_1' => 'required',
            'supplier_price_1' => 'required',
            'supplier_price_2' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required'
        ]);

        Sparepart::find($id)->update([
            'ms_categories_id' => $request->ms_categories_id ,
            'ms_companies_id' => $request->ms_companies_id ,
            'ms_brands_id' => $request->ms_brands_id ,
            'sparepart_code' => $request->sparepart_code ,
            'division_category' => $request->division_category ,
            'uom' => $request->uom ,
            'description' => $request->description ,
            'imei_1' => $request->imei_1 ,
            'imei_2' => $request->imei_2 ,
            'supplier_price_1' => $request->supplier_price_1 ,
            'supplier_price_2' => $request->supplier_price_2 ,
            'cogs_price' => $request->cogs_price ,
            'selling_price' => $request->selling_price ,
            'up_price_percent' => $request->up_price_percent ,
            'customer_price' => $request->customer_price
        ]);

        return redirect()->route('spareparts.home')->with('success', 'This Sparepart Has Been updated.');
    }

    public function destroy($id)
    {
        Sparepart::find($id)->delete();
        return redirect()->route('spareparts.home')->with('success', 'This Sparepart Has Been deleted.');
    }
}
