<?php

namespace App\Http\Controllers\master;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;

class PaymentController extends Controller
{
    public function index()
    {
        $no = 1;
        $showData = Payment::get();
        return view('master.payments.index', [
            'showData' => $showData,
            'no' => $no,
        ]);
    }

    public function paymentSearch(Request $request)
    {
        $query = Payment::orderBy('id');


        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function paymentSelect2(Request $request)
    {
        return Payment::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function store(Request $request)
    {
        if(!empty($request->name)){
            Payment::create([
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Payment Has Been Saved');
    }

    public function dataUpdate($id)
    {
        $dataEdit = Payment::find($id);
        return $dataEdit;
    }

    public function edit(Request $request, $id)
    {
        if(!empty($request->name)){
            Payment::where('id', $request->id)->update([
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Payment Has Been Saved');
    }

    public function delete($id)
    {
        Payment::where('id', $id)->delete();
    }
}
