<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Bank;

class CustomerController extends Controller
{
    public function index()
    {
        $getCustomer = Customer::get();
        return view('master.customers.index', [
            'getCustomer' => $getCustomer
        ]);
    }

    public function create()
    {
        $getPayment = Bank::get();
        return view('master.customers.create', [
            'getPayment' => $getPayment
        ]);
    }

    public function update($id)
    {
        $getPayment = Bank::get();
        $custEdit = Customer::find($id);
        return view('master.customers.update', [
            'getPayment' => $getPayment,
            'custEdit' => $custEdit,
        ]);
    }

    public function store(Request $request, $length = 15)
    {
        $this->validate($request, [
            'name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'no_ktp' => 'required',
            'email' => 'required',
            'limit_piutang' => 'required',
            'fax' => 'required',
            'npwp' => 'required',
            'rekening_no' => 'required',
            'rekening_name' => 'required',
            'ms_banks_id' => 'required',
        ]);

        $str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        Customer::create([
            'code' => substr(str_shuffle(str_repeat($str, 5)), 0, $length),
            'name' => $request->name,
            'alias' => $request->alias,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'email' => $request->email,
            'no_ktp' => $request->no_ktp,
            'city' => $request->city,
            'contact_person' => $request->contact_person,
            'telepon_cp' => $request->telepon_cp,
            'limit_piutang' => $request->limit_piutang,
            'npwp' => $request->npwp,
            'rekening_no' => $request->rekening_no,
            'rekening_name' => $request->rekening_name,
            'ms_banks_id' => $request->ms_banks_id,
            'status' => $request->status,
        ]);

        return redirect()->route('customers.home')->with('success', 'This Customer Has Been Saved.');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'no_ktp' => 'required',
            'email' => 'required',
            'limit_piutang' => 'required',
            'fax' => 'required',
            'npwp' => 'required',
            'rekening_no' => 'required',
            'rekening_name' => 'required',
            'ms_banks_id' => 'required',
        ]);

        Customer::find($id)->update([
            'name' => $request->name,
            'alias' => $request->alias,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'email' => $request->email,
            'no_ktp' => $request->no_ktp,
            'city' => $request->city,
            'contact_person' => $request->contact_person,
            'telepon_cp' => $request->telepon_cp,
            'limit_piutang' => $request->limit_piutang,
            'npwp' => $request->npwp,
            'rekening_no' => $request->rekening_no,
            'rekening_name' => $request->rekening_name,
            'ms_banks_id' => $request->ms_banks_id,
            'status' => $request->status,
        ]);

        return redirect()->route('customers.home')->with('success', 'This Customer Has Been Updated.');
    }

    public function destroy($id)
    {
        Customer::find($id)->delete();
        return redirect()->route('customers.home')->with('success', 'This Customer Has Been Deleted.');
    }
}
