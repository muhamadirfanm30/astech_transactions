<?php

namespace App\Http\Controllers\master;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Company;
use App\Models\Brand;

class ServicesController extends Controller
{
    public function index()
    {
        $showServices = Services::with(['getcompany', 'getbrand'])->get();
        $brand = brand::get();
        return view('master.services.index', [
            'showServices' => $showServices,
            'brand' => $brand,
        ]);
    }

    public function datatables()
    {
        $showServices = Services::with(['getcompany', 'getbrand']);
        return DataTables::of($showServices)
            ->addIndexColumn()
            ->toJson();
    }

    public function serviceSearch(Request $request)
    {
        $query = Services::with(['getcompany', 'getbrand']);
        if (!empty($request->search_brand)) {
            $query->where('ms_brands_id', $request->search_brand);
        }

        if (!empty($request->search_service_code)) {
            $query->where('service_code', $request->search_service_code);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function create()
    {
        $showServices = Services::get();
        $showCompany = Company::get();
        $showBrand = Brand::get();
        return view('master.services.create', [
            'showServices' => $showServices,
            'showCompany' => $showCompany,
            'showBrand' => $showBrand,
        ]);
    }

    public function update($id)
    {
        $seriveEdit = Services::find($id);
        $showCompany = Company::get();
        $showBrand = Brand::get();
        return view('master.services.update', [
            'seriveEdit' => $seriveEdit,
            'showCompany' => $showCompany,
            'showBrand' => $showBrand,
        ]);
    }

    public function store(Request $request, $length = 20)
    {
        $this->validate($request, [
            'status' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'service_code' => 'required',
            'svc_type' => 'required',
            'defect_type' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required',
        ]);

        Services::create([
            'status' => $request->status,
            'ms_companies_id' => $request->ms_companies_id,
            'ms_brands_id' => $request->ms_brands_id,
            'service_code' => $request->service_code,
            'svc_type' => $request->svc_type,
            'defect_type' => $request->defect_type,
            'cogs_price' => $request->cogs_price,
            'selling_price' => $request->selling_price,
            'description' => $request->description,
        ]);

        return redirect()->route('services.home')->with('success', 'This Services Has Been Saved.');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'ms_companies_id' => 'required',
            'ms_brands_id' => 'required',
            'service_code' => 'required',
            'svc_type' => 'required',
            'defect_type' => 'required',
            'cogs_price' => 'required',
            'selling_price' => 'required',
        ]);

        Services::find($id)->update([
            'status' => $request->status,
            'ms_companies_id' => $request->ms_companies_id,
            'ms_brands_id' => $request->ms_brands_id,
            'service_code' => $request->service_code,
            'svc_type' => $request->svc_type,
            'defect_type' => $request->defect_type,
            'cogs_price' => $request->cogs_price,
            'selling_price' => $request->selling_price,
            'description' => $request->description,
        ]);

        return redirect()->route('services.home')->with('success', 'This Services Has Been Updated.');
    }

    public function destroy($id)
    {
        Services::find($id)->delete();
        return redirect()->route('services.home')->with('success', 'This Services Has Been Deleted.');
    }
}
