<?php

namespace App\Http\Controllers\master;

use Session;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\Company;
use App\Models\BranchOffice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function index()
    {
        $showData = Company::get();
        $count = Company::count();
        return view('master.companies.index', [
            'showData' => $showData,
            'count' => $count,
        ]);
    }

    public function indexBranch()
    {
        $showBranch = BranchOffice::with(['company', 'prov', 'city'])->get();
        return view('master.companies.indexBranch', [
            'showBranch' => $showBranch,
        ]);
    }

    public function create()
    {
        return view('master.companies.create');
    }

    public function brachEdit($id)
    {
        $getMainOffice = Company::get();
        $editBranch = BranchOffice::with(['company', 'prov', 'city'])->find($id);
        $getBank = Bank::get();
        return view('master.companies.edit_branch', [
            'editBranch' => $editBranch,
            'getMainOffice' => $getMainOffice,
            'getBank' => $getBank,
        ]);
    }

    public function mainEdit($id)
    {
        $mainOffices = Company::find($id);
        return view('master.companies.edit_main', [
            'mainOffices' => $mainOffices
        ]);
    }

    public function brach()
    {
        $getMainOffice = Company::get();
        $getBank = Bank::get();
        return view('master.companies.create_branch', [
            'getMainOffice' => $getMainOffice,
            'getBank' => $getBank,
        ]);
    }

    public function select2Branch(Request $request)
    {
        return BranchOffice::where('name', 'like', '%' . $request->q . '%')
            ->groupBy('name')
            ->limit(20)
            ->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'tax_address' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required',
        ]);

        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'address_3' => $request->address_3,
            'city' => $request->city,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'email' => $request->email,
            'tax_address' => $request->tax_address,
            'bank_name' => $request->bank_name,
            'bank_account' => $request->bank_account,
        ];

        $save = Company::create($data);
        return redirect()->route('companies.home')
                        ->with('success','The main office has been saved');
    }

    public function storeBranch(Request $request)
    {
        $this->validate($request, [
            'provinces_id' => 'required',
            'cities_id' => 'required',
            'ms_branches_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'head_of_branch' => 'required',
            'head_of_departement' => 'required',
            'branch_area' => 'required',
            'type' => 'required',
            'ms_banks_id' => 'required',
            'rekening_no' => 'required',
            'credit_limit' => 'required',
            'item_limit' => 'required',
            'branch_principal' => 'required',
            'service_percent' => 'required',
            'sales_percent' => 'required',
            'down_payment' => 'required',
            'check_unit_price' => 'required',
        ]);

        $data = [
            'provinces_id' => $request->provinces_id,
            'cities_id' => $request->cities_id,
            'ms_branches_id' => $request->ms_branches_id,
            'code' => $request->code,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'head_of_branch' => $request->head_of_branch,
            'head_of_departement' => $request->head_of_departement,
            'branch_area' => $request->branch_area,
            'type' => $request->type,
            'ms_banks_id' => $request->ms_banks_id,
            'rekening_no' => $request->rekening_no,
            'credit_limit' => $request->credit_limit,
            'item_limit' => $request->item_limit,
            'branch_principal' => $request->branch_principal,
            'service_percent' => $request->service_percent,
            'sales_percent' => $request->sales_percent,
            'down_payment' => $request->down_payment,
            'check_unit_price' => $request->check_unit_price,
        ];

        if(!empty($request->ms_branches_id)){
            $save = BranchOffice::create($data);
            return redirect()->route('companies.indexBranch')
                        ->with('success','The branch office has been saved');
        }else{
            return redirect()->back()
                ->with('error','branch office cannot be empty');
        }
    }

    public function updateBranch(Request $request, $id)
    {
        $this->validate($request, [
            'ms_branches_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'head_of_branch' => 'required',
            'head_of_departement' => 'required',
            'branch_area' => 'required',
            'type' => 'required',
            'ms_banks_id' => 'required',
            'rekening_no' => 'required',
            'credit_limit' => 'required',
            'item_limit' => 'required',
            'branch_principal' => 'required',
            'service_percent' => 'required',
            'sales_percent' => 'required',
            'down_payment' => 'required',
            'check_unit_price' => 'required',
        ]);

        $data = BranchOffice::find($id);

        $data = [
            'provinces_id' => !empty($request->provinces_id) ? $request->provinces_id : $data->provinces_id,
            'cities_id' => !empty($request->cities_id) ? $request->cities_id : $data->cities_id,
            'ms_branches_id' => $request->ms_branches_id,
            'code' => $request->code,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'head_of_branch' => $request->head_of_branch,
            'head_of_departement' => $request->head_of_departement,
            'branch_area' => $request->branch_area,
            'type' => $request->type,
            'ms_banks_id' => $request->ms_banks_id,
            'rekening_no' => $request->rekening_no,
            'credit_limit' => $request->credit_limit,
            'item_limit' => $request->item_limit,
            'branch_principal' => $request->branch_principal,
            'service_percent' => $request->service_percent,
            'sales_percent' => $request->sales_percent,
            'down_payment' => $request->down_payment,
            'check_unit_price' => $request->check_unit_price,
        ];

        if(!empty($request->ms_branches_id)){
            $save = BranchOffice::find($id)->update($data);
            return redirect()->route('companies.indexBranch')
                        ->with('success','The branch office has been saved');
        }else{
            return redirect()->back()
                ->with('error','branch office cannot be empty');
        }
    }

    public function updateMain(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'tax_address' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required',
        ]);

        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'address_3' => $request->address_3,
            'city' => $request->city,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'email' => $request->email,
            'tax_address' => $request->tax_address,
            'bank_name' => $request->bank_name,
            'bank_account' => $request->bank_account,
        ];

        $save = Company::find($id)->update($data);
        return redirect()->route('companies.home')
                    ->with('success','This Main office has been updated');
    }

    public function destroy($id)
    {
        Company::find($id)->delete();
        return redirect()->route('companies.home')
                    ->with('success','This Main office has been Deleted');
    }

    public function destroyBranch($id)
    {
        BranchOffice::find($id)->delete();
        return redirect()->route('companies.indexBranch')
                    ->with('success','This Branch office has been Deleted');
    }

}
