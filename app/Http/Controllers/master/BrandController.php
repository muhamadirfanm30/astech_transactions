<?php

namespace App\Http\Controllers\master;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Company;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BrandController extends Controller
{
    public function index()
    {
        $no = 1;
        $brandShow = Brand::with('getcompany')->get();
        $getCompany = Company::get();
        return view('master.brands.index', [
            'brandShow' => $brandShow,
            'getCompany' => $getCompany,
            'no' => $no,
        ]);
    }

    public function list()
    {
        return Brand::query();
    }

    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        return Brand::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function brandSearch(Request $request)
    {
        $query = Brand::with('getcompany')->orderBy('id');


        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }
    public function edit($id)
    {
        $dataEdit = Brand::with('getcompany')->find($id);
        $getCompany = Company::get();
        return view('master.brands.update', [
            'dataEdit' => $dataEdit,
            'getCompany' => $getCompany,
        ]);

    }

    public function store(Request $request, $length = 20)
    {
        $this->validate($request, [
            'ms_companies_id' => 'required',
            'name' => 'required',
            'logo' => 'required',
        ]);

        $str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $path = Brand::getImagePathUpload();
        $filename = null;

        if ($request->logo != null) {
            $image = (new ImageUpload)->upload($request->logo, $path);
            $filename = $image->getFilename();
        }else{
            return redirect()->back()->with('error', 'Image Logo Not null');
        }

        Brand::create([
            'ms_companies_id' => $request->ms_companies_id,
            'name' => $request->name,
            'logo' => $filename,
            'code' => substr(str_shuffle(str_repeat($str, 5)), 0, $length),
        ]);

        return redirect()->route('brands.home')->with('success', 'This Brand Has Been Saved.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ms_companies_id' => 'required',
            'name' => 'required',
        ]);

        $cek = Brand::where('id', $id)->first();

        if(!empty($request->logo)){
            if ($request->hasFile('logo')) {
                $image      = $request->file('logo');
                $file_name   = time(). '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/brand_image/' . $file_name, $img);

                $exists2 = Storage::disk('local')->has('public/brand_image/' . $cek->logo);
                if ($exists2) {
                    Storage::delete('public/brand_image/' . $cek->logo);
                }
            }
        }else{
            $file_name = $cek->logo;
        }

        Brand::find($id)->update([
            'ms_companies_id' => $request->ms_companies_id,
            'name' => $request->name,
            'logo' => $file_name,
        ]);

        return redirect()->route('brands.home')->with('success', 'This Brand Has Been Updated.');
    }

    public function destroy($id)
    {
        $cekImg = Brand::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/brand_image/' . $cekImg->logo);
        if ($exists) {
            Storage::delete('public/brand_image/' . $cekImg->logo);
        }
        Brand::find($id)->delete();
        return redirect()->route('brands.home')->with('success', 'This Brand Has Been Deleted.');
    }
}
