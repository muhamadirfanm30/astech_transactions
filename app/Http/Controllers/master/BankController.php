<?php

namespace App\Http\Controllers\master;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;

class BankController extends Controller
{
    public function index()
    {
        $no = 1;
        $showBank = Bank::get();
        return view('master.banks.index', [
            'showBank' => $showBank,
            'no' => $no,
        ]);
    }

    public function bankSearch(Request $request)
    {
        $query = Bank::orderBy('id');


        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function store(Request $request)
    {
        if(!empty($request->name)){
            Bank::create([
                'code' => date('ymd'),
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Bank Has Been Saved');
    }

    public function dataUpdate($id)
    {
        $dataEdit = Bank::find($id);
        return $dataEdit;
    }

    public function edit(Request $request, $id)
    {
        if(!empty($request->name)){
            Bank::where('id', $request->id)->update([
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Bank Has Been Saved');
    }

    public function delete($id)
    {
        Bank::where('id', $id)->delete();
    }
}
