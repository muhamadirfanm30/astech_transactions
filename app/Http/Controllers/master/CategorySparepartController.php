<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\CategorySparepart;
use App\Http\Controllers\Controller;
use App\Models\ImportLogCategorySparepart;

class CategorySparepartController extends Controller
{
    public function index()
    {
        $no = 1;
        $categori = CategorySparepart::get();
        return view('master.category_spareparts.index', [
            'categori' => $categori,
            'no' => $no,
        ]);
    }


    public function importLogdatatables()
    {
        $query = ImportLogCategorySparepart::with('user');
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function importedByDatatables($users_id)
    {
        $query = CategorySparepart::with('user')->where('imported_by', $users_id)->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function store(Request $request)
    {
        if(!empty($request->name)){
            CategorySparepart::create([
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Payment Has Been Saved');
    }

    public function dataUpdate($id)
    {
        $dataEdit = CategorySparepart::find($id);
        return $dataEdit;
    }

    public function edit(Request $request, $id)
    {
        if(!empty($request->name)){
            CategorySparepart::where('id', $request->id)->update([
                'name' => $request->name,
            ]);
        }else{
            return redirect()->back()->with('error', 'Field Nama Tidak boleh Kosong');
        }
        return redirect()->back()->with('success', 'Payment Has Been Saved');
    }

    public function delete($id)
    {
        CategorySparepart::where('id', $id)->delete();
    }
}
