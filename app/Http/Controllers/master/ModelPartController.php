<?php

namespace App\Http\Controllers\master;
use App\Models\Brand;
use App\Models\Company;
use App\Models\ModelPart;
use Illuminate\Http\Request;
use App\Models\CategorySparepart;
use App\Models\ImportLogModelPart;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ModelPartController extends Controller
{
    public function index()
    {
        $showDataModelPart = ModelPart::with('getcompany')->get();
        $getComp = Company::get();
        $getCategory = CategorySparepart::get();
        $getBrand = brand::get();
        return view('master.model-part.index', [
            'showDataModelPart' => $showDataModelPart,
            'getComp' => $getComp,
            'getCategory' => $getCategory,
            'getBrand' => $getBrand,
        ]);
    }

    public function datatables(Request $request)
    {
        $query = ModelPart::with('getcompany')->orderBy('id');


        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function importLogdatatables()
    {
        $query = ImportLogModelPart::with('user');
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function importedByDatatables($users_id)
    {
        $query = ModelPart::with('user')->where('imported_by', $users_id)->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function update($id)
    {
        $dataEdit = ModelPart::find($id);
        $getComp = Company::get();
        $getCategory = CategorySparepart::get();
        $getBrand = Brand::get();
        return view('master.model-part.update', [
            'getComp' => $getComp,
            'getCategory' => $getCategory,
            'getBrand' => $getBrand,
            'dataEdit' => $dataEdit
        ]);
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'ms_companies_id' => 'required',
        //     'model_code' => 'required',
        //     'ms_brands_id' => 'required',
        //     'ms_categories_id' => 'required',
        //     'model_name' => 'required',
        //     'service_code' => 'required',
        //     'desc' => 'required',
        // ]);

        ModelPart::create([
            'ms_companies_id' => $request->ms_companies_id ,
            'model_code' => $request->model_code ,
            'ms_brands_id' => $request->ms_brands_id ,
            'ms_categories_id' => $request->ms_categories_id ,
            'model_name' => $request->model_name ,
            'service_code' => $request->service_code ,
            'desc' => $request->desc ,
        ]);

        return redirect()->route('model.part.home')->with('success', 'Model Part Has Been Saved');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'ms_companies_id' => 'required',
            'model_code' => 'required',
            'ms_brands_id' => 'required',
            'ms_categories_id' => 'required',
            'model_name' => 'required',
            'service_code' => 'required',
            'desc' => 'required',
        ]);

        ModelPart::find($id)->update([
            'ms_companies_id' => $request->ms_companies_id ,
            'model_code' => $request->model_code ,
            'ms_brands_id' => $request->ms_brands_id ,
            'ms_categories_id' => $request->ms_categories_id ,
            'model_name' => $request->model_name ,
            'service_code' => $request->service_code ,
            'desc' => $request->desc ,
        ]);

        return redirect()->route('model.part.home')->with('success', 'Model Part Has Been Updated');
    }

    public function destroy($id)
    {
        ModelPart::find($id)->delete();

        return redirect()->route('model.part.home')->with('success', 'Model Part Has Been Deleted');
    }
}
