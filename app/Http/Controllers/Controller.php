<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successResponse($data, string $msg = 'success', $status_code = 200)
    {
        return response()->json([
            'data' => $data,
            'msg' => $msg,
        ], $status_code);
    }

    public function errorResponse($data, string $msg = 'terjadi kesalahan', $status_code = 400)
    {
        return response()->json([
            'data' => $data,
            'msg' => $msg,
        ], $status_code);
    }

    public function successStoreMsg(){
        return 'Data successfully saved';
    }
    public function successUpdateMsg(){
        return 'Data successfully updated';
    }
    public function successDeleteMsg(){
        return 'Data successfully deleted';
    }
}
