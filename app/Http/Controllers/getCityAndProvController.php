<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Province;

class getCityAndProvController extends Controller
{
    public function getCity($id)
    {
        return City::where('provincies_id', $id)->get();
    }

    public function getProvince()
    {
        return Province::get();
    }
}
