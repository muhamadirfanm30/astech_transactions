<?php

namespace App\Http\Controllers\utility;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Brand;
Use App\Models\User;
Use App\Models\BranchUser;
use Hash;
use DB;
use App\Models\BranchOffice;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ListUsersController extends Controller
{
    public function index()
    {
        return view('utility.user-list.index');
    }

    public function create()
    {
        $roles = Role::get();
        $company = Company::get();
        $brand = Brand::get();
        $branchOffice = BranchOffice::with('company')->get();
        return view('utility.user-list.create', [
            'brand' => $brand,
            'roles' => $roles,
            'company' => $company,
            'branchOffice' => $branchOffice
        ]);
    }

    public function select2Company()
    {
        $comp = Company::get();
        return response()->json($comp);
    }

    public function select2Branch($id)
    {
        return BranchOffice::where('ms_branches_id', $id)->get();
    }

    public function update($id)
    {
        $roles = Role::get();
        $brachUser = BranchUser::where('user_id', $id)->count();
        $user = User::with('branchuser')->find($id);
        // return $user->branchuser->count();
        $company = Company::get();
        $brand = Brand::get();
        $branchOffice = BranchOffice::get();
        return view('utility.user-list.update', [
            'brand' => $brand,
            'user' => $user,
            'roles' => $roles,
            'company' => $company,
            'branchOffice' => $branchOffice,
            'brachUser' => $brachUser
        ]);
    }

    public function store(Request $request)
    {
        // return $request->branch_id;
        try{
            $password = Hash::make($request->pass);
            $insertUsers = User::create([
                'username' => $request->username,
                'password' => $password, 
                'name' => $request->name,
                'ms_brand_id' => $request->brand_id,
                'ms_companies_id' => $request->company_id,
                'is_head_office' => $request->head_office,
                'nip' => $request->nip,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'status' => $request->status,
                'type' => $request->type,
                'level' => $request->level,
            ]);

            $insertUsers->assignRole($request->level);

            if(!empty($request->branch_id)) {
                foreach($request->branch_id as $key => $id) {
                    $data_branch = [
                        'user_id' => $insertUsers->id,
                        'users_branch_id' => $request->branch_id[$key],
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s')

                    ];
                    $insert_branch = BranchUser::create($data_branch);
                }
            }

            DB::commit();
        }catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
        
    }

    public function edit(Request $request, $id)
    {
        // return $request->all();
        try{
            $user = User::find($id);

            if(!empty($request->pass)){
                $password = Hash::make($request->pass);
            }else{
                $password = $user->password;
            }

            
            $user->update([
                'username' => $request->username,
                'password' => $password, 
                'name' => $request->name,
                'ms_brand_id' => $request->brand_id,
                'ms_companies_id' => $request->company_id,
                'is_head_office' => $request->head_office,
                'nip' => $request->nip,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'status' => $request->status,
                'type' => $request->type,
                'level' => $request->level,
            ]);
            
            DB::table('model_has_roles')->where('model_id',$id)->delete();
        
            $user->assignRole($request->level);
            $delete = BranchUser::whereIn('user_id', [$id])->delete();
            if(!empty($request->branch_id)) {
                foreach($request->branch_id as $key => $id) {
                    $data_branch = [
                        'user_id' => $user->id,
                        'users_branch_id' => $request->branch_id[$key],
                        'created_at' => date('y-m-d H:i:s'),
                        'updated_at' => date('y-m-d H:i:s')

                    ];
                    // $insert_branch = BranchUser::where('user_id', $id)->update($data_branch);
                    $insert = BranchUser::upsert($data_branch, ['user_id'],['users_branch_id','created_at','updated_at']);
                }
            }
            
            DB::commit();
        }catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
        
    }

    public function datatables(Request $request)
    {
        $query = User::with('get_role')->orderBy('id');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function destroy($id)
    {
        if($id != 1){
            $userDelete = User::where('id', $id)->first();
            $exists = Storage::disk('local')->has('public/users-profile/' . $userDelete->attachment);
            if ($exists) {
                Storage::delete('public/users-profile/' . $userDelete->attachment);
            }
            User::find($id)->delete();
        }
    }
}
