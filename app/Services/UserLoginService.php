<?php

namespace App\Services;

use App\Models\User;

class UserLoginService
{
    public $user;

    /**
     * handle login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function handle(array $credentials)
    {
        if (is_numeric($credentials['email'])) {
            return $this->login([
                'phone' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        } else {
            return $this->login([
                'email' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        }
    }

    /**
     * login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function login(array $credentials)
    {
        if (auth()->attempt($credentials)) {

            $user = User::with('get_role')->where('id', auth()->user()->id)->first();
            // set user
            $this->setUser($user);

            return true;
        } else {
            return false;
        }
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
