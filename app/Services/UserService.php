<?php

namespace App\Services;

use App\Exceptions\SendErrorMsgException;
use App\Services\UserLoginService;
use App\Models\User;
use DB;

class UserService{
    public $token_name = 'astech_cashier';
    public $user;
    public $notif    = true;
    public $redirect = '/';
    public $status;

    public function login(array $credentials)
    {
        $loginService = new UserLoginService();

        $login = $loginService->handle($credentials);

        if ($login) {
            // $this->setUser($loginService->getUser())->setBearerToken();
            $this->setUser($loginService->getUser());
        }

        return $login;
    }

    public function setTokenName(string $name)
    {
        $this->token_name = $name;
        return $this;
    }

    public function getTokenName(): string
    {
        return $this->token_name;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
