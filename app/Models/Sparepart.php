<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sparepart extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_spareparts';

    public function quotation_order_service_part_detail()
    {
        return $this->hasMany(QuotationOrderServicePartDetail::class, 'id', 'ms_spareparts_id');
    }


}
