<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class ModelPart extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_model_parts';

    public function getcompany()
    {
        return $this->hasOne(Company::class, 'id', 'ms_companies_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'imported_by');
    }
}
