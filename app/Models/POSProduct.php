<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSProduct extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_products';

    protected $dates = ['manufacturing_date','expiry_date'];

    public function category()
    {
        return $this->belongsTo(POSCategory::class, 'pos_categories_id');
    }

    public function supplier()
    {
        return $this->belongsTo(POSSupplier::class, 'pos_suppliers_id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchOffice::class, 'ms_branches_id');
    }

    public function tax()
    {
        return $this->belongsTo(POSTax::class, 'pos_taxes_id');
    }

    public function barcode_symbology()
    {
        return $this->belongsTo(POSBarcodeSymbology::class, 'pos_barcode_symbologies_id');
    }

    public function unit_type()
    {
        return $this->belongsTo(POSUnitType::class, 'pos_unit_types_id');
    }

    public static function getImagePathUpload()
    {
        return 'public/pos_products';
    }
}
