<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationOrderServicePartDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'quotation_order_servicepart_details';

    public function quotation_order()
    {
        return $this->belongsTo(QuotationOrder::class, 'quotation_orders_id');
    }

    public function sparepart()
    {
        return $this->belongsTo(Sparepart::class, 'ms_spareparts_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'ms_services_id');
    }




}
