<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class Brand extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_brands';

    public function getcompany()
    {
        return $this->belongsTo(Company::class, 'ms_companies_id');
    }

    public static function getImagePathUpload()
    {
        return 'public/brand_image';
    }

    public function brand()
    {
        return $this->hasMany(Brand::class, 'ms_brands_id');
    }
}
