<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationOrderCustomer extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'quotation_order_customers';


    public function quotation_order()
    {
        return $this->belongsTo(QuotationOrder::class, 'id', 'quotation_orders_id');
    }




}
