<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchOffice extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_branches';

    public function company()
    {
        return $this->belongsTo(Company::class, 'ms_branches_id');
    }

    public function prov()
    {
        return $this->belongsTo(Province::class, 'provinces_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id');
    }
}
