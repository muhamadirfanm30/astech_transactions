<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'ms_companies';

    public function branch()
    {
        return $this->hasMany(Branch::class, 'id', 'ms_companies_id');
    }
}
