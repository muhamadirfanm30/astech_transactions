<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationOrder extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'quotation_orders';
    protected $dates = ['date'];


    public function brand()
    {
        return $this->belongsTo(Brand::class, 'ms_brands_id');
    }

    public function status_payment()
    {
        return $this->belongsTo(StatusPayment::class, 'status_payments_id');
    }

    public function quotation_order_customer()
    {
        return $this->hasOne(QuotationOrderCustomer::class, 'quotation_orders_id');
    }

    public function payment_detail()
    {
        return $this->hasMany(PaymentDetail::class, 'quotation_orders_id');
    }

    public function quotation_order_servicepart_detail()
    {
        return $this->hasMany(QuotationOrderServicePartDetail::class, 'quotation_orders_id');
    }

    public function sparepart_detail()
    {
        return $this->quotation_order_servicepart_detail()->where('status', 'PART');
    }

    public function service_detail()
    {
        return $this->quotation_order_servicepart_detail()->where('status', 'LABOR');
    }




}
