<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSBarcodeSymbology extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_barcode_symbologies';
}
