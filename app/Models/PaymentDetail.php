<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'payment_details';


    public function payment()
    {
        return $this->belongsTo(Payment::class, 'ms_payments_id');
    }

    public function quotation_order()
    {
        return $this->belongsTo(QuotationOrder::class, 'quotation_orders_id');
    }






}
