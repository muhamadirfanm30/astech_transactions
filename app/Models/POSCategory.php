<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSCategory extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_categories';

    public static function getImagePathUpload()
    {
        return 'public/pos_categories';
    }
}
