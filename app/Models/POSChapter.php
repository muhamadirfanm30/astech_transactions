<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSChapter extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_chapters';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
