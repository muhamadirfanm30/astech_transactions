<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSUnitType extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_unit_types';
}
