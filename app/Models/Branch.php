<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_branches';

    public function company()
    {
        return $this->belongsTo(Company::class, 'ms_companies_id');
    }
}
