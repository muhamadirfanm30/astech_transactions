<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSRefund extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_refunds';

    public function sale()
    {
        return $this->belongsTo(POSSale::class, 'pos_sales_id');
    }

    public function sale_detail()
    {
        return $this->belongsTo(POSSaleDetail::class, 'pos_sale_details_id');
    }
}
