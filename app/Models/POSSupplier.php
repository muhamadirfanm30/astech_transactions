<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSSupplier extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_suppliers';
}
