<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSPurchase extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_purchases';

    public function supplier()
    {
        return $this->belongsTo(POSSupplier::class, 'pos_suppliers_id');
    }

    public function tax()
    {
        return $this->belongsTo(POSTax::class, 'pos_taxes_id');
    }

}
