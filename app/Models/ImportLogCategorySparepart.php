<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportLogCategorySparepart extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'import_log_category_spareparts';


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }


}
