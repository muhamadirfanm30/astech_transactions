<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorySparepart extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_category_spareparts';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'imported_by');
    }
}
