<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'ms_services';

    public function getcompany()
    {
        return $this->hasOne(Company::class, 'id', 'ms_companies_id');
    }

    public function getbrand()
    {
        return $this->hasOne(Company::class, 'id', 'ms_companies_id');
    }

    public function quotation_order_service_part_detail()
    {
        return $this->hasMany(QuotationOrderServicePartDetail::class, 'id', 'ms_services_id');
    }
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'imported_by');
    }
}
