<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSReport extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_reports';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
