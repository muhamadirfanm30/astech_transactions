<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSSale extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_sales';

    public function chapter()
    {
        return $this->belongsTo(POSChapter::class, 'pos_chapters_id');
    }

    public function sale_detail()
    {
        return $this->hasMany(POSSaleDetail::class, 'pos_sales_id');
    }
}
