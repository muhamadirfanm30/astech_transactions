<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POSSaleDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pos_sale_details';

    public function sale()
    {
        return $this->belongsTo(POSSale::class, 'pos_sales_id');
    }
}
