<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_model_parts', function (Blueprint $table) {
            $table->id();
            $table->integer('ms_companies_id');
            $table->string('model_code');
            $table->integer('ms_brands_id');
            $table->integer('ms_categories_id');
            $table->string('model_name');
            $table->string('service_code');
            $table->bigInteger('imported_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_model_parts');
    }
}
