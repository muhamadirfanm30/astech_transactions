<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationOrderServicepartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_order_servicepart_details', function (Blueprint $table) {
            $table->id();
            $table->integer('quotation_orders_id');
            $table->enum('status', ['LABOR','PART']);
            $table->integer('ms_spareparts_id');
            $table->integer('ms_services_id');
            $table->string('sparepart_code');
            $table->string('service_code');
            $table->string('sparepart_name');
            $table->string('service_name');
            $table->integer('qty');
            $table->double('price');
            $table->double('discount');
            $table->double('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_order_servicepart_details');
    }
}
