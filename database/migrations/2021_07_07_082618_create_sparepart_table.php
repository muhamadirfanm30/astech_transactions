<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_sparepart', function (Blueprint $table) {
            $table->id();
            $table->integer('ms_category_spareparts_id');
            $table->integer('ms_companies_id');
            $table->integer('ms_brands_id');
            $table->string('part_code');
            $table->string('division_category');
            $table->text('description');
            $table->string('uom');
            $table->string('imei_1');
            $table->string('imei_2');
            $table->double('cogs_price');
            $table->double('supplier_price_1');
            $table->double('supplier_price_2');
            $table->double('selling_price');
            $table->float('up_price_percent');
            $table->double('customer_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_sparepart');
    }
}
