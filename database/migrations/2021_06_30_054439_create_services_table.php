<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_services', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['By.Cek', 'By.Jasa']);
            $table->integer('ms_companies_id');
            $table->integer('ms_brands_id');
            $table->string('service_code');
            $table->text('description');
            $table->enum('svc_type', ['CI','IH','PS','RH','OTH']);
            $table->enum('defect_type', ['MJ','MN','SP','AE','FL','OTH']);
            $table->double('cogs_price');
            $table->double('selling_price');
            $table->bigInteger('imported_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_services');
    }
}
