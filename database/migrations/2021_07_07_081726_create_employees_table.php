<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_employees', function (Blueprint $table) {
            $table->id();
            $table->integer('ms_companies_id');
            $table->integer('ms_branches_id');
            $table->string('nip');
            $table->string('name');
            $table->string('address_1');
            $table->string('address_2');
            $table->integer('city_id');
            $table->string('telephone');
            $table->string('handphone');
            $table->integer('ms_positions_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_employees');
    }
}
