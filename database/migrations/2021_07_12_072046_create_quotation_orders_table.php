<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_orders', function (Blueprint $table) {
            $table->id();
            $table->integer('ms_customers_id');
            $table->date('date');
            $table->enum('type_transaction', ['Service','Sales']);
            $table->integer('ms_brands_id');
            $table->string('type_product');
            $table->string('bill_number');
            $table->string('so_number');
            $table->double('total_price_sparepart');
            $table->double('total_price_service');
            $table->string('imei_number');
            $table->enum('bill_to', ['PRINCIPAL','END USER']);
            $table->text('description');
            $table->integer('status_payments_id');
            $table->date('dp_date');
            $table->double('dp');
            $table->double('dp_paid');
            $table->date('inv_date');
            $table->double('inv_paid');
            $table->double('sub_total');
            $table->float('discount_percent');
            $table->double('discount');
            $table->float('ppn_percent');
            $table->double('ppn');
            $table->double('total');
            $table->double('payment');
            $table->integer('remaining');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_orders');
    }
}
