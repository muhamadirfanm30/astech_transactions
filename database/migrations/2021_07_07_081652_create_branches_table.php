<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_branches', function (Blueprint $table) {
            $table->id();
            $table->integer('provinces_id');
            $table->integer('cities_id');
            $table->string('code');
            $table->string('name');
            $table->text('address');
            $table->string('phone');
            $table->string('fax');
            $table->string('head_of_branch');
            $table->string('head_of_departement');
            $table->enum('branch_area', ['AREA-01','AREA-02']);
            $table->enum('type', ['BRANCH','PARTNER']);
            $table->integer('ms_banks_id');
            $table->string('rekening_no');
            $table->double('credit_limit');
            $table->string('item_limit');
            $table->string('branch_principal');
            $table->string('service_percent');
            $table->string('sales_percent');
            $table->string('down_payment');
            $table->double('check_unit_price');
            $table->integer('ms_branches_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_branches');
    }
}
