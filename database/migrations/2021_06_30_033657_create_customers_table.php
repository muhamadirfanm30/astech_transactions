<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_customers', function (Blueprint $table) {
            $table->id();
            $table->string('code', 200);
            $table->string('name', 200);
            $table->string('alias', 200);
            $table->text('address_1');
            $table->text('address_2');
            $table->string('phone', 200);
            $table->string('fax', 200);
            $table->string('email', 200);
            $table->string('no_ktp', 200);
            $table->string('city', 200);
            $table->string('contact_person', 200);
            $table->string('telepon_cp', 200);
            $table->double('limit_piutang');
            $table->string('npwp', 200);
            $table->string('rekening_no', 200);
            $table->string('rekening_name', 200);
            $table->integer('ms_banks_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_customers');
    }
}
